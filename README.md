# FYS-4096 Computational physics

Alpi Tolvanen, 2019

All code is under MIT-licence. There exists also parts of course examples made by Ilkka Kylänpää.


## General information
Computational physic course at _(brand new)_ University of Tampere. Weekly exercises are located in directories ```exerciseN```.

## Exercise 1
Solved: 1,2,3,4,5
For exercise 1, check file 'exercise1/readme.md'

## Exercise 2
Solved: 1,2,3
Check file 'exercise2/readme.md' and 'exercise2/ex2_solved_problems.pdf'

## Exercise 3
Solved: 1,2,3,4
Check file 'exercise3/readme.md' and 'exercise3/ex3_solved_problems.pdf'

## Exercise 4
Solved: 1,2,3,4

## Exercise 5
Solved: 1,2,3,4

## Exercise 6
Solved: 1,2,3,4

## Project_Work_1
Solved: warmup and problem 5

## Exercise7
Solved: 1,2,3,4 (Note all solutions are in file hartree_1d.py)

## Exercise 8
Solved: 1,2,3,4

## Exercise 9
Solved: 1,2,3

## Exercise 10
Solved: 1,2

## Exercise 11
Solved: 1,2,3,4

## Exercise 12
Solved: 1,2,3,4

## Project_Work_2
Solved: warmup and problem 4
