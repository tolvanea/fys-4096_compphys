"""
This file solves warmup problem in project work 1.

Alpi Tolvanen
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps

def integrand(r):
    return (r.sum()) * np.exp(-0.5*np.linalg.norm(r))

def calculate_integral(a1, a2):
    """ Integral is calculated by "change of variables" from lattice
    cooordinates to box coordinates (r = A @ α). Therefore integrand is
    multiplied by determinant of linear transformation A."""
    N = 31
    # 0-1 grid
    α_xgrid = np.linspace(0, 1, N, endpoint=True)
    α_ygrid = np.linspace(0, 1, N, endpoint=True)
    dx = 1.0 / (N-1)
    A = np.vstack((a1, a2)).T

    transformed_integrand = np.vectorize(
        lambda x, y: integrand(A @ np.array((x,y))) * np.linalg.det(A))

    int_over_x = np.empty_like(α_xgrid)
    for i in range(N):
        dens = transformed_integrand(α_xgrid, α_ygrid[i])
        int_over_x[i] = simps(y=dens, dx=dx)

    int = simps(y=int_over_x, dx=dx)

    print("Integral: ", int)
    # >>> Integral: 0.824887...

def plot_figure(a1, a2):
    N = 31
    mx = max(a1[0] + a2[0], a1[1] + a2[1])
    xgrid = np.linspace(-0.1, mx+0.1, N, endpoint=True)
    ygrid = np.linspace(-0.1, mx+0.1, N, endpoint=True)
    XX, YY = np.meshgrid(xgrid, ygrid)

    vectorized_integrand = np.vectorize(lambda x, y: integrand(np.array((x,y))))
    ZZ = vectorized_integrand(XX, YY)

    fig, ax = plt.subplots(1, 1, figsize=(5,5))

    CS = ax.contourf(XX, YY, ZZ, 100)
    origo = [0.0, 0.0]
    for triple in [[origo, a1, a1+a2], [origo, a2, a1+a2]]:
        for i in range(2):
            line_x = [triple[i][0], triple[i+1][0]]
            line_y = [triple[i][1], triple[i+1][1]]
            ax.plot(line_x, line_y, c="r")

    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_title("$(x+y)e^\\sqrt{x^2, y^2}$")
    ax.text(0.1, 0.07, "$\\Omega$", color="r", fontsize=16)
    fig.savefig("warm_up_plot.png")
    plt.show()

def main():
    # Lattice vectors
    a1 = np.array((1.1, 0.0))
    a2 = np.array((0.5, 1.0))
    calculate_integral(a1, a2)

    plot_figure(a1, a2)



if __name__ == "__main__":
    main()
