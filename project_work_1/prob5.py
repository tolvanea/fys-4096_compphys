"""
Density matrix of non-interacting particles.
This file solves second part of problem 5 in project work 1.

Alpi Tolvanen
"""

import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
#from scipy.integrate import simps
import typing as tp
import LU_decomp_module as lu

def ρ_free_particle(r: np.ndarray, rp: np.ndarray,
                    constants: tp.Tuple[float, int, float]) -> float:
    """
    Free particle density matrix.
    :param r:           start position
                            np.ndarray[float], size: (dim)
    :param rp:          end position
                            np.ndarray[float], size: (dim)
    :param constants:   constants: λ, dim, β
                            float, int, float
    :return:
    """
    λ, d, β = constants
    return (4 * np.pi * λ * β) ** (-d/2) * np.exp(-(r-rp)@(r-rp) / (4*λ * β))

def ρ_noninteracting_particles(R: np.ndarray, Rp: np.ndarray,
                               constans: tp.Tuple[float, int, float]
                               ) -> float:
    """
    Density matrix for noninteracting particles.

    :param R:           start position of all particles
                            np.ndarray[float], size: (num_parts*dim)
    :param Rp:          end position of all particles
                            np.ndarray[float], size: (num_parts*dim)
    :param constans:    constants: λ, dim, β
                            float, int, float
    :return:
    """
    assert(len(R) == len(Rp))
    mat = np.empty((len(R), len(R)))
    for i in range(len(R)):
        for j in range(len(R)):
            mat[i,j] = ρ_free_particle(R[i], Rp[j],constans)

    LE, P, succ = lu.LUPDecompose(mat, 1e-10)
    if succ:
        det = lu.LUPDeterminant(LE, P)
    else:
        det = 0.0

    return det

def draw_circle(x, y, radius):
    """
    Return x,y points of a circle. (Matplotlib does not have good ways to plot
    circles of knows size to axes-object.)
    :return:
    """
    theta = np.linspace(-np.pi, np.pi, 200, endpoint=True)
    return (radius*np.sin(theta)+x, radius*np.cos(theta)+y)

def main():
    """Calculate density matrix for each pixel and plot it."""
    λ = 1
    d = 2

    # Initial positions of fermions
    r1 = np.array((1.0, 0.0))
    r2 = np.array((-1.0, 1.0))
    r3 = np.array((0.0, 0.0))
    R = np.vstack((r1,r2,r3))

    # Grid of density matrix
    x_range = np.linspace(-5,5,50)
    y_range = np.linspace(-5,5,50)
    XX, YY = np.meshgrid(x_range, y_range)
    imgs = np.empty((3, len(x_range), len(y_range)))

    # Calculate values of density matrix for each pixel
    for pic, β in enumerate((10.0, 1.0, 0.1)):
        constants = (λ, d, β)
        for i, x in enumerate(x_range):
            for j, y in enumerate(y_range):
                r3p = np.array((x, y))
                Rp = np.vstack((r1,r2,r3p))
                imgs[pic,i,j] = ρ_noninteracting_particles(R, Rp, constants)


    # Plot images
    fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(15,5))

    # By the way, matplotlib colorsbars are terrible to use:
    cax1 = fig.add_axes([0.09 + 0.00*0.92, 0.1, 0.21, 0.03])
    cax2 = fig.add_axes([0.09 + 0.33*0.92, 0.1, 0.21, 0.03])
    cax3 = fig.add_axes([0.09 + 0.66*0.92, 0.1, 0.21, 0.03])

    for pic, (ax, cax, β) in enumerate(zip((ax1, ax2, ax3),
                                        (cax1, cax2, cax3),
                                        (10.0, 1.0, 0.1))):
        # Background heatmap
        im = ax.imshow(imgs[pic], interpolation='bilinear', origin='lower',
                       extent=(*x_range[[0,-1]], *y_range[[0,-1]]))
        # Contour line
        cnt = ax.contour(XX, YY, imgs[pic], levels=[0.0], colors="white")
        plt.clabel(cnt, [0.0], inline=1, fmt='%1.1f', fontsize=14)

        # Colorbar
        CB = plt.colorbar(im, cax=cax, extend='both',
                          orientation='horizontal')

        # Draw starting positions
        for r in R:
            face = 'none' if r@r < 0.001 else None
            ax.plot([r[0]], [r[1]], c="r", marker="o", markerfacecolor=face)

        # De Broglie wavelength
        deBroglie_length= np.sqrt(2*λ*β)
        ax.plot(*draw_circle(0.0, 0.0, deBroglie_length), ls=":", c="white")
        ax.text(-0.3, -deBroglie_length, "$\\sqrt{2 \\lambda \\beta}$",
                color="white", fontsize=10, horizontalalignment="right",
                verticalalignment="center")

        # Labels did not fit into picture
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        ax.set_title("$\\beta={}$".format(β))
        fig.suptitle("Density matrix of three non-interacting fermions")
        fig.tight_layout(pad=3, rect=[0, 0.1, 1, 1])  # Colorbars are hard.


    fig.savefig("prob5__densitymatrix.png")
    plt.show()



if __name__ == "__main__":
    main()
