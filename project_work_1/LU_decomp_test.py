import numpy as np
import unittest
import LU_decomp_module as lu

"""
Test LU-decomposition.
This file solves first part of problem 5 in project work 1.
- 1d FEM is used to test matrix equation solution. 
- Also random matrices are used to test inverse and determinant. 
"""





class TestLUDecompositionWithFEM(unittest.TestCase):
    """
    Test to solve 1d FEM poisson case with LU-module and scipy, as were asked in
    question.
    """

    def A_and_b_by_given_matrix_elements(self, xvec: np.ndarray):
        """
        Calculate matrix element's of A and vector b by formula given in problem 2
        in exercises 6. (This is taken directly from my solution.)
        """
        h = (xvec[-1]-xvec[0])/len(xvec)
        diag0 = np.full(len(xvec), fill_value=2.0 / h)
        diag1 = np.full(len(xvec) - 1, fill_value=-1.0 / h)
        A = np.diag(diag0) + np.diag(diag1, k=+1) + np.diag(diag1, k=-1)
        A[[0,-1],:] = 0.0
        A[:,[0,-1]] = 0.0
        A[0,0] = 1.0
        A[-1,-1] = 1.0

        b = np.zeros_like(xvec)
        for i in range(1, len(xvec)-1):
            b[i] = np.pi/h * (xvec[i-1] + xvec[i+1] - 2*xvec[i]) \
                   * np.cos(np.pi * xvec[i]) \
                   + 1/h * (2 * np.sin(np.pi * xvec[i]) - np.sin(np.pi * xvec[i-1])
                            - np.sin(np.pi * xvec[i+1]))
        return A, b

    def test_to_solve_1d_FEM_poisson_case(self):
        N = 50
        h = 1 / N
        xvec = np.linspace(0-h,1+h,N+2, endpoint=True)
        A, b = self.A_and_b_by_given_matrix_elements(xvec)

        LE, P, succ = lu.LUPDecompose(A[1:-1,1:-1], 1e-6)
        assert(succ)
        a_LU = lu.LUPSolve(LE, P, b[1:-1])

        a_scipy = np.linalg.solve(A[1:-1,1:-1], b[1:-1])

        self.assertTrue(np.isclose(a_scipy, a_LU).all())






class TestLUDecompositionWithRandomMatrix(unittest.TestCase):
    """ Test LU decomposition with randomly generated matrix. This was not asked
    in question, but it shows validity of inverse and determinant also.
    """

    def setUp(self):

        N = 10
        self.tol = 1e-6

        self.test_matrices = []
        for i in range(10):  # Generate ten test matrices

            succ = False
            while not succ:
                A = np.random.rand(N, N)
                LE, P, succ = lu.LUPDecompose(A, self.tol)
                if not succ:
                    print("Singular random matrix, trying another one. ", i)

            self.test_matrices.append(A)


    def test_LU_solve(self):
        """ Solve Ax = b """
        for A in self.test_matrices:
            b = np.random.rand(A.shape[0])
            LE, P, succ = lu.LUPDecompose(A, self.tol)
            x_1 = lu.LUPSolve(LE, P, b)

            x_2 = np.linalg.solve(A, b)
            self.assertTrue(np.isclose(x_1, x_2).all())


    def test_LU_inv(self):
        """ Inverse """
        for A in self.test_matrices:
            LE, P, succ = lu.LUPDecompose(A, self.tol)
            inv_1 = lu.LUPInvert(LE, P)

            inv_2 = np.linalg.inv(A)
            self.assertTrue(np.isclose(inv_1, inv_2).all())


    def test_LU_det(self):
        """ Determinant """
        for A in self.test_matrices:
            LE, P, succ = lu.LUPDecompose(A, self.tol)
            det_1 = lu.LUPDeterminant(LE, P)

            det_2 = np.linalg.det(A)
            self.assertTrue(np.isclose(det_1, det_2))

def main():
    unittest.main()

if __name__ == "__main__":
    main()
