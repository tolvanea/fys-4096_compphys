"""
LU-decomposition, matrix equation solve, inverse, and determinant.
This file solves first part of problem 5 in project work 1.

All code in this file is python port of LU decomposition code in
https://en.wikipedia.org/wiki/LU_decomposition
"""

import numpy as np
import typing as tp


def LUPDecompose(A: np.ndarray, tol: float) \
        -> tp.Tuple[tp.Union[np.ndarray, None],
                    tp.Union[np.ndarray, None],
                    bool]:
    """
    L-U-P decomposition.

    :param A:  np.ndarray[float], size: (N*N)
                    A square matrix having dimension N.
    :param tol: float, size: (N,)
                    Small tolerance number to detect failure when the matrix is
                    near degenerate
    :return:
                LE - np.ndarray[float], size: (N*N)
                     Contains both matrices L-E and U as A=(L-E)+U
                     such that P*A=L*U.
                P  - np.ndarray[int], size: (N+1)
                     The permutation matrix is not stored as a matrix, but in an
                     integer vector P of size N+1 containing column indexes where
                     the permutation matrix has "1". The last element P[N]=S+N,
                     where S is the number of row exchanges needed for
                     determinant computation, det(P)=(-1)^S.
                success - bool
                    Succes/fail. If fail, then P and LE are None.
    """
    assert A.shape[0] == A.shape[1]
    N: int = A.shape[0]

    LE = A.copy()
    P = np.empty(N+1, dtype=np.int_)
    for i in range(N+1):
        P[i] = i  # Unit permutation matrix, P[N] initialized with N

    for i in range(N):
        maxA: float = 0.0
        imax: int = i

        for k in range(i, N):
            absA = np.abs(LE[k,i])
            if (absA > maxA):
                maxA = absA
                imax = k

        if maxA < tol:
            return None, None, False  # failure, matrix is degenerate

        if imax != i:
            # pivoting P
            j = P[i]
            P[i] = P[imax]
            P[imax] = j

            # pivoting rows of A
            row = LE[i,:].copy()  # These two copy()-functions took me a lot of
            LE[i,:] = LE[imax,:].copy()  # time to figure out.
            LE[imax,:] = row

            # counting pivots starting from N (for determinant)
            P[N] += 1

        for j in range(i+1, N):
            LE[j,i] /= LE[i,i]

            for k in range(i+1, N):
                LE[j,k] -= LE[j,i] * LE[i,k]

    return LE, P, True  #decomposition done


def LUPSolve(LE: np.ndarray, P: np.ndarray, b: np.ndarray) -> np.ndarray:
    """
    Solve x in equation A @ x = b

    :param LE:  np.ndarray[float], size: (N*N)
                    Returned by 'LUPDecompose()'
    :param P:   np.ndarray[int], size: (N+1,)
                    Returned by 'LUPDecompose()'
    :param b:   np.ndarray[float], size: (N,)
                    rhs vector
    :return:
                x - np.ndarray[float] (N,)
                    solution vector of equation A @ x = b
    """
    assert LE.shape[0] == LE.shape[1] == P.shape[0]-1 == b.shape[0]
    N: int = LE.shape[0]
    x = np.empty(N)

    for i in range(0, N):
        x[i] = b[P[i]]

        for k in range(0, i):
            x[i] -= LE[i,k] * x[k]

    for i in range(N-1, 0-1, -1):
        for k in range(i+1, N):
            x[i] -= LE[i,k] * x[k]

        x[i] = x[i] / LE[i,i]

    return x


def LUPInvert(LE: np.ndarray, P: np.ndarray) -> np.ndarray:
    """
    Invert original matrix by results of LU decomposition

    :param LE:   np.ndarray[float], size: (N*N)
                    Returned by 'LUPDecompose()'
    :param P:   np.ndarray[int], size: (N,)
                    Returned by 'LUPDecompose()'
    :return:
                IA - np.ndarray[float], size: (N,N)
                    Inverse of the initial matrix A

    """

    assert LE.shape[0] == LE.shape[1] == P.shape[0]-1
    N: int = LE.shape[0]
    IA = np.empty_like(LE)

    for j in range(0, N):
        for i in range(0, N):
            if P[i] == j:
                IA[i,j] = 1.0
            else:
                IA[i,j] = 0.0

            for k in range(0, i):
                IA[i,j] -= LE[i,k] * IA[k,j]

        for i in range(N-1, 0-1, -1):
            for k in range(i+1, N):
                IA[i,j] -= LE[i,k] * IA[k,j]
            IA[i,j] = IA[i,j] / LE[i,i]

    return IA


def LUPDeterminant(A: np.ndarray, P: np.ndarray) -> float:
    """
    Calculate determinant for LU decomposition.

    :param A:   np.ndarray[float] (N*N)
                    Filled with 'LUPDecompose()'
    :param P:   np.ndarray[int] (N,)
                    Filled with 'LUPDecompose()'
    :return:
    """
    assert A.shape[0] == A.shape[1] == P.shape[0]-1
    N: int = A.shape[0]

    det = A[0,0]

    for i in range(1, N):
        det *= A[i,i]

    if (P[N] - N) % 2 == 0:
        return det
    else:
        return -det