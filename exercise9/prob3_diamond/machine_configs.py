#! /usr/bin/env python
from nexus import job

def general_configs(machine):
    if machine=='taito':
        jobs = get_taito_configs()
    else:
        print 'Using taito as defaul machine'
        jobs = get_taito_configs()
    return jobs

def get_taito_configs():
    scf_presub = '''
    module purge
    module load gcc
    module load openmpi
    module load openblas
    module load hdf5-serial
    '''

    qe0='pw.x'
    qe1='pw2qmcpack.x'
    qe2='qmcpack_taito_cpu_comp_SoA'
    # 4 processes
    scf   = job(cores=4,minutes=30,user_env=False,presub=scf_presub,app=qe0)
    conv  = job(cores=4,minutes=30,user_env=False,presub=scf_presub,app=qe1)
    qmc   = job(cores=4,minutes=30,user_env=False,presub=scf_presub,app=qe2)
    # 24 processes (1 node = 24 processors at taito)
    #scf  = job(nodes=1,hours=1,user_env=False,presub=scf_presub,app=qe)
    
    jobs = {
        'scf' : scf,
        'conv' : conv,
        'qmc' : qmc,
        }

    return jobs
