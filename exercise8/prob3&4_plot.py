
import numpy as np
import matplotlib.pyplot as plt

def prob3(ax):

    seps = np.array([
        0.907,
        1.032,
        1.157,
        1.2074,
        1.282,
        1.407,
        1.507])
    # Values copy pasted from generated outputfiles
    E_in_Ry = np.array([
        -63.07539384,
        -63.50123836,
        -63.57464198,
        -63.58314599,
        -63.57073313,
        -63.54553552,
        -63.76107030])
    # Values are read manyally from from generated outputfiles
    converged = np.array([
        False,
        False,
        True,
        True,
        True,
        False,
        False])

    ax.plot(seps, E_in_Ry, c="C0")
    ax.plot(seps[converged], E_in_Ry[converged], linestyle="", marker="o",
            markerfacecolor="C0", c="C0", label="Converged")
    ax.plot(seps[~converged], E_in_Ry[~converged], linestyle="", marker="o",
            markerfacecolor="None", c="C0", label="Not converged")
    ax.legend()
    ax.set_xlabel("separation (Å)")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 3: O$_2$ potential energy curve")


def prob4a(ax):
    cuts = np.array([50, 100, 150, 200, 250, 300])

    # Values copy pasted from generated outputfiles
    E_in_Ry = np.array([
        -22.40864005,
        -22.51487991,
        -22.52203164,
        -22.52257768,
        -22.52262441,
        -22.52262803])

    converged = np.array([
        True,
        True,
        True,
        True,
        True,
        True])


    ax.plot(cuts, E_in_Ry, c="C0")
    ax.plot(cuts[converged], E_in_Ry[converged], linestyle="", marker="o",
            markerfacecolor="C0", c="C0", label="Converged")
    ax.plot(cuts[~converged], E_in_Ry[~converged], linestyle="", marker="o",
            markerfacecolor="None", c="C0", label="Not converged")
    ax.legend()
    ax.set_xlabel("Cut-off")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 4a: Diamond, energy cut-off convergence")

def prob4b(ax):
    cuts = np.array([1, 2, 3])

    # Values copy pasted from generated outputfiles
    E_in_Ry = np.array([
        -20.54485326,
        -22.52257768,
        -22.71847567])

    converged = np.array([
        True,
        True,
        True])


    ax.plot(cuts, E_in_Ry, c="C0")
    ax.plot(cuts[converged], E_in_Ry[converged], linestyle="", marker="o",
            markerfacecolor="C0", c="C0", label="Converged")
    ax.plot(cuts[~converged], E_in_Ry[~converged], linestyle="", marker="o",
            markerfacecolor="None", c="C0", label="Not converged")
    ax.legend()
    ax.set_xlabel("k-grid (k,k,k)")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 4b: Diamond, k-grid convergence")

def main():
    fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(13.5,4))
    prob3(ax1)
    prob4a(ax2)
    prob4b(ax3)
    fig.tight_layout()
    
main()
    
plt.show()


