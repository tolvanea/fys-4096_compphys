#! /usr/bin/env python2
import os
from nexus import settings,job,run_project,obj
from nexus import generate_physical_system
from nexus import generate_pwscf
from machine_configs import get_taito_configs
import numpy as np

settings(
    pseudo_dir    = './pseudopotentials',
    results       = '',
    status_only   = 0,
    generate_only = 0, 
    sleep         = 3,
    machine       = 'taito',
)

jobs = get_taito_configs()

cubic_box_size=[10.0]
x=1.0*cubic_box_size[0]
d_equil=1.2074 # nuclei separation in Angstrom
dx = 0.1
p = np.linspace(d_equil - 3*dx, d_equil + 3*dx, 6)
potential_grid = np.hstack((p[:3],[d_equil],p[3:]))
print("potential_grid", potential_grid)

simulations = []

for sep in potential_grid:

    O2 = generate_physical_system(
        units     = 'A',
        axes      = [[ x,   0.0 ,  0.0   ],
                     [ 0.,   x  ,  0.0   ],
                     [ 0.,   0. ,   x    ]],
        elem      = ['O','O'],
        pos       = [[ x/2-sep/2    ,  x/2    ,  x/2    ],
                     [ x/2+sep/2    ,  x/2    ,  x/2    ]],
        net_spin  = 0,
        tiling    = (1,1,1),
        kgrid     = (1,1,1), # scf kgrid given below to enable symmetries
        kshift    = (0,0,0),
        O         = 6
    )

    scf = generate_pwscf(
        identifier   = 'scf',
        path         = 'scf_{:.3f}'.format(sep),
        job          = jobs['scf'],
        input_type   = 'generic',
        calculation  = 'scf',
        input_dft    = 'lda', 
        ecutwfc      = 200,   
        conv_thr     = 1e-8, 
        nosym        = True,
        wf_collect   = True,
        system       = O2,
        kgrid        = (1,1,1),
        pseudos      = ['O.BFD.upf'],
        )
    
    simulations.append(scf)

run_project(simulations)

