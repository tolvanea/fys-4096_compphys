"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
    - Done
- Comment the parts with "# ADD" and make any additional comments you
  think could be useful for yourself later.
    - Done
- Follow the assignment from ex11.pdf.
    - Default settings:
        - Temprerature 3.0 K
        - grid 10×10
        - dimension 2
        - boundary: perioidic
        - exchange is probanly J=4.0
    - See image "prob1&2_plot.png"
    - Transition temperature is roughly 3 temperature units.


Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
    - Done
- Follow the assignment from ex11.pdf.
    - Done
    - See image "prob3_plot.png"

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.
    - Yes phase transitions are seen as sudden change of observables in
      different temperatures.

"""

from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import pickle
#import numba



# @numba.jitclass([
#     ("spin", numba.float64),
#     ("nearest_neighbors", numba.int64[:]),
#     ("sys_dim", numba.int64),
#     ("coords", numba.int64[:]),
# ])
class Walker:
    """
    Single gridpoint of system.
    """
    def __init__(self, spin, nn, dim, coords):
        self.spin = spin
        self.nearest_neighbors = nn
        self.sys_dim = dim
        self.coords = coords

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())


#@numba.njit
def Energy(Walkers) -> float:
    """
    Total energy of system
    """
    E = 0.0
    J = 4.0  # given in units of k_B
    # ADD calculation of energy
    for walker in Walkers:
        E += site_Energy(Walkers, walker)

    return E / 2

#@numba.njit
def site_Energy(Walkers, walker):
    """
    Energy of single gridpoint with respecto of nearest neighbours.
    """
    E = 0.0
    J = 4.0  # given in units of k_B
    for k in range(len(walker.nearest_neighbors)):
        j = walker.nearest_neighbors[k]
        E += -J * walker.spin * Walkers[j].spin
    return E

def calc_observable(Walkers, obs_callable):
    """
    Calculate mean value for generic observable.
    :param Walkers:
    :param obs_callable:  function with input parameters (Walkers, walker)
    :return:
    """
    O = 0.0
    for walker in Walkers:
        O += obs_callable(Walkers, walker)

    return O / len(Walkers)


def observables(Walkers, T):
    """
    Applies given observable and divides by probability.
    Calculates \sum_s O(s)P(s),
    Obserbales (per spin):
        energy
        heat capacity,
        magnetization and
        magnetic susceptibility
    """
    # energy
    Eb = Energy(Walkers) / len(Walkers)

    # heat capacity
    E_squared_fun = lambda Walkers, walker: (site_Energy(Walkers, walker)/2)**2
    E2 = calc_observable(Walkers, E_squared_fun)
    Cv = (E2 - Eb**2) / T**2

    # magnetization
    M_fun = lambda Walkers, walker: walker.spin
    magnetization = calc_observable(Walkers, M_fun)

    # magnetic susceptibility
    M_squared = lambda Walkers, walker: M_fun(Walkers, walker) ** 2
    magnetization_squared = calc_observable(Walkers, M_squared)
    Xhii = (magnetization_squared - magnetization**2) / T

    return Eb, Cv, magnetization, Xhii

#@numba.njit
def ising(Nblocks, Niters, Walkers, beta):
    """
    Metropolis MC algorithm for solving ising model of magnetic spins.
    """
    M = len(Walkers)
    obs_interval = 5
    obs_count = Nblocks * Niters * obs_interval
    obs = {
        "Energy Eb" : zeros((Nblocks,)),
        "Heat capacity Cv" : zeros((Nblocks,)),
        "Magnetization M" : zeros((Nblocks,)),
        "Susceptibility X" : zeros((Nblocks,)),
    }
    # obs = {
    #     "Energy Eb" : 0.0,
    #     "Heat capacity Cv" : 0.0,
    #     "Magnetization M" : 0.0,
    #     "Susceptibility X" : 0.0,
    # }
    Accept = zeros((Nblocks,))
    AccCount = zeros((Nblocks,))

    for i in range(Nblocks):
        obs_count = 0
        for j in range(Niters):
            site = int(random.rand() * M)

            s_old = 1.0 * Walkers[site].spin

            E_old = site_Energy(Walkers, Walkers[site])

            # ADD selection of new spin to variable s_new
            # NOTE: This is uniform distribution. --> S/S' = 1
            s_new = s_old * (-1)**random.randint(2)
            Walkers[site].spin = s_new

            E_new = site_Energy(Walkers, Walkers[site])

            # ADD Metropolis Monte Carlo
            E_diff = E_new - E_old
            q = min(1.0, exp(-beta * E_diff))
            if q < random.rand():
                Walkers[site].spin = s_old
            else:
                Accept[i] += 1
            AccCount[i] += 1

            if j % obs_interval == 0:
                o = observables(Walkers, 1/beta)
                for key_idx, key in enumerate(obs):
                    obs[key][i] += o[key_idx]
                obs_count += 1

        for key in obs:
            obs[key][i] /= obs_count
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i + 1, Nblocks))
        print('    E   = {0:.5f}'.format(obs[key][i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))

    return Walkers, obs, Accept


def plot_fancy_map(Walkers, grid_side, ax2):
    """
    Plot 2d image of system
    """

    img = zeros((grid_side,grid_side))
    for walker in Walkers:
        img[walker.coords[0],walker.coords[1]] = walker.spin

    ax2.imshow(img)
    ax2.set_title("States of random walkers at the end")


def calculate_ising_system(grid_side, T, Nblocks):
    Walkers = []

    dim = 2
    grid_size = grid_side ** dim

    # Ising model nearest neighbors only
    mapping = zeros((grid_side, grid_side), dtype=int)  # mapping
    inv_map = []  # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i, j] = ii
            inv_map.append([i, j])
            ii += 1

    # ADD comment
    # Initialize individual walker to each grid point.
    for i in range(grid_side):
        for j in range(grid_side):
            j1 = mapping[i, (j - 1) % grid_side]
            j2 = mapping[i, (j + 1) % grid_side]
            i1 = mapping[(i - 1) % grid_side, j]
            i2 = mapping[(i + 1) % grid_side, j]
            Walkers.append(Walker(spin=0.5,
                                  nn=np.array([j1, j2, i1, i2]),
                                  dim=dim,
                                  coords=np.array([i, j])))

    Niters = 1000
    beta = 1.0 / T
    """
    Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """
    Walkers, obs, Acc = ising(Nblocks, Niters, Walkers, beta)

    return Walkers, obs, Acc




def problems_1_and_2():
    grid_side = 10
    Nblocks = 200
    T = 3.0
    eq = 20  # equilibration "time"

    Walkers, obs, Acc = calculate_ising_system(grid_side, T, Nblocks)

    fig, (ax1, ax2) = subplots(1,2,figsize=(10,5))
    ax1.set_xlabel("Block")

    for key in obs:
        ax1.plot(obs[key], label=key)
        ob = obs[key][eq:]
        print("Obserwable '{}'".format(key))
        print('    Mean: {:.5f} +/- {:0.5f}'.format(mean(ob),
                                                std(ob) / sqrt(len(ob))))
        print('    Variance ratio: {:.5f}'.format(abs(std(ob) / mean(ob))))

    ax1.legend()
    ax1.set_title("Observables by block, T = {}".format(T))
    plot_fancy_map(Walkers, grid_side, ax2)

    with open('observables.pickle', 'wb') as f:
        pickle.dump(obs, f)

def problem_3():
    T_grid = 10
    grid_side = 10
    T_range = linspace(0.6,6.0,T_grid)
    Nblocks = 200
    eq = 20  # equilibration "time"
    obs_by_T = [[] for i in range(4)]
    errs_by_T = [[] for i in range(4)]
    for T in T_range:
        Walkers, obs, Acc = calculate_ising_system(grid_side, T, Nblocks)
        for key_idx, key in enumerate(obs):
            obs_by_T[key_idx].append(mean(obs[key][eq:]))
            errs_by_T[key_idx].append(std(obs[key][eq:]) / sqrt(len(obs[key][eq:])))


    with open('observables_by_T.pickle', 'wb') as f:
        pickle.dump(obs_by_T, f)

    fig, ((ax1, ax2), (ax3, ax4)) = subplots(2,2,figsize=(8,8))
    axes = ax1, ax2, ax3, ax4
    for idx, (ax, key) in enumerate(zip(axes, obs)):
        ax.plot(T_range, obs_by_T[idx])
        ax.set_title(key)
        ax.set_xlabel("$T$")

    fig.tight_layout()





if __name__ == "__main__":
    problems_1_and_2()
    problem_3()
    show()
