Problems solved

Exercise1
    1,2,3,4,5
    
Exercise2
    1,2,3
    (see 'exercise2/ex2_solved_problems.pdf' for more detail)

Exercise3
    1,2,3,4
    
Exercise4
    1,2,3,4

Exercise5
    1,2,3,4
    
Exercise6
    1,2,3,4

Project_Work_1
    warmup and problem 5

Exercise7
    1,2,3,4 (Note all solutions are in file hartree_1d.py)

Exercise8
    1,2,3,4
    
Exercise9
    1,2,3

Exercise10
    1,2
    
Exercise11
    1,2,3,4

Exercise12
    1,2,3,4
    
Project_Work_2
    warmup and problem 4
