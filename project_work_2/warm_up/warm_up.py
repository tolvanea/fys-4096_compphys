"""
Warm up problem.
This file does:
    - Calculate 1d interpoaltion within 2d data with 'spline_class.py'
    - Plot 1d and 2d pictures
    - Store 1d interpolation to hdf5 format.

Alpi Tolvanen
"""


import numpy as np
import matplotlib.pyplot as plt
import h5py
import spline_class as sp

def read_file(filename, datanames):
    """
    Read hdf5 file.
    :param filename:
    :param datanames:   list of datanames  (e.g ["x_grid", "x_grid"])
    :return:
    """
    assert filename[-3:] == ".h5"
    h5f = h5py.File(filename, 'r')
    datas = []
    for dataname in datanames:
        datas.append(h5f[dataname][...])
    h5f.close()
    return tuple(datas)

def write_file(filename, datanames, datas, info=None):
    """
    Write hdf5 file
    :param filename:
    :param datanames:   list of datanames  (e.g ["x_grid", "x_grid"])
    :param datas:       list of datas (e.g [[0.3, 0.5], [0.0, 1.0]])
    :param info:        (optional) list of infos (e.g ["x data", "y data"])
    :return:
    """
    assert filename[-3:] == ".h5"
    h5f = h5py.File(filename, 'a')
    for i,(dataname, data) in enumerate(zip(datanames, datas)):
        # Create or overwrite
        if not dataname in h5f.keys():
            dset = h5f.create_dataset(dataname, data=data)
        else:
            dset = h5f[dataname]
            dset[...] = data
        if info is not None:
            dset.attrs["info"] = info[i]
    h5f.close()

def plot(data, x_grid, y_grid,
         interp_points, interp_vals, spl2d):
    """
    Plot 1d and 2d plots of interpolated data (e.g. values along the line and
    the 2d-countour).
    """

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(9, 5))

    #1d
    iterp_dist = np.sqrt(np.sum((interp_points - interp_points[:,0][:,np.newaxis])**2, axis=0))
    ax1.plot(iterp_dist, interp_vals)
    ax1.set_xlabel("distance on line")
    ax1.set_ylabel("value")

    # 2d

    XX, YY = np.meshgrid(x_grid, y_grid)
    im = ax2.imshow(data, interpolation='bilinear', origin='lower',
                       extent=(*x_grid[[0,-1]], *y_grid[[0,-1]]))
    # Contour line
    # By the way, matplotlib colorsbars are terrible to use:
    cax = fig.add_axes([0.55, 0.1, 0.42, 0.03])
    cnt = ax2.contour(XX.T, YY.T, data, levels=[-1.0, -0.5, 0.0, 0.5, 1.0], colors="white")
    plt.clabel(cnt, [-1.0, -0.5, 0.0, 0.5, 1.0], inline=1, fmt='%1.1f', fontsize=12)

    ax2.plot(interp_points[0,:], interp_points[1,:], color="r")
    ax2.set_xlabel("$x$")
    ax2.set_ylabel("$y$")
    fig.tight_layout(pad=3, rect=[0, 0.1, 1, 1])  # Colorbars are hard.

    # Colorbar
    plt.colorbar(im, cax=cax, extend='both', orientation='horizontal')

    # draw dot
    dot_r = np.array([0.1, 0.1])
    dot_dist = np.sqrt(np.sum((dot_r - interp_points[:,0])**2))
    val = spl2d.eval2d(*dot_r)[0][0]

    ax1.plot(dot_dist, val, ls="", marker="o", color="k")
    ax1.text(dot_dist+0.2, val, "$\\approx$ {:.4f}".format(val))
    ax2.plot(dot_r[0], dot_r[1],  ls="", marker="o", color="k")
    ax2.text(dot_r[0]+0.2, dot_r[1], "$\\approx$ {:.4f}".format(val))


def main():
    data, x_grid, y_grid = read_file("warm_up_data.h5",
                                     ("data", "x_grid", "y_grid"))

    # Before changing 'interp_gridsize', please remove 'warm_up_interpolated.h5'
    interp_gridsize = 100
    # interpolated line end points
    interp_start = np.array([-1.5,-1.5])
    interp_end = np.array([1.5,1.5])

    spl2d = sp.spline(x=x_grid, y=y_grid, f=data, dims=2)
    interp_vals = np.zeros(interp_gridsize)
    interp_points = interp_start[:, np.newaxis] \
                    + np.outer((interp_end-interp_start),
                               np.linspace(0, 1, interp_gridsize, endpoint=True)
                               )
    for i in range(interp_gridsize):
        r = interp_points[:,i]
        interp_vals[i] = spl2d.eval2d(*r)

    plot(data, x_grid, y_grid, interp_points, interp_vals, spl2d)

    # Write hdf5
    write_file("warm_up_interpolated.h5",
               ["interp_vals", "start", "end"],
               [interp_vals, interp_start, interp_end],
               ["Interpolated data values on a line",
                "Start point of line",
                "End point of line (inlcluded in data)"])

    plt.show()



if __name__ == "__main__":
    main()