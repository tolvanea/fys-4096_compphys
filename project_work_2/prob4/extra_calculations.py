"""
THIS IS EXTRA FILE

This file contains similar plots than in problem 4 in exerises 11.
It does following simulations
    H2 atomic       (Morse potential)
    H2 electronic   (Coulomb potentials)

Both of them produce quite stupidly high kinetic energy values. (I dont know why)

The main point here is to show how my refactorization of 'pimc_simple' made it really flexible
to change simulation systems on the fly.
"""

import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

import pimc_python
ord = pimc_python.ord
import walker_slider
import init_systems

def calculate_with_different_trotters(params, state, trotters=None):
    if trotters is None:
        trotters = 2**np.arange(0, 6)  # [1,2,4,8...]
    taus = 1 / (params.T * trotters)

    calcs_by_trotter = [None for i in range(len(taus))]

    # Calculate with many tau:s
    for i, (tau, trotter) in enumerate(zip(taus, trotters)):

        params_copy = deepcopy(params)
        params_copy.trotter = trotter
        params_copy.tau = tau

        state_copy = deepcopy(state)
        walker = state_copy.walkers[0,:]
        state_copy.walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)

        calculation = pimc_python.start(params_copy, state_copy)
        calcs_by_trotter[i] = calculation

    return calcs_by_trotter

def plot_block_convergence(ax1, ax2, calcs_by_trotter):
    """
    Plot energy by block count for Largest M value.
    """
    pick_idx = -1
    obs = calcs_by_trotter[pick_idx].obs
    params = calcs_by_trotter[pick_idx].params
    state = calcs_by_trotter[pick_idx].state

    plot = (
        ("Energy", "$E_{tot}$", ax1, obs.E_tot, 0),
        ("Energy", "$E_{kin}$", ax1, obs.E_kin, 1),
        ("Energy", "$E_{pot}$", ax1, obs.E_pot, 2),
        ("Bond length", "$r_{H-H}$", ax2, obs.R[ord(0,1)], 0)
            )
    # Make sure that "Bond length" measures hydrogen bond distance with 'obs.R[(0,1)]'
    assert(state.species[0].symbol == state.species[1].symbol and
            state.species[0].symbol in ("H","p"))

    print("Trotter: {}".format(params.trotter))

    for title, label, ax, data, id in plot:
        b = int(params.burnout_ratio * params.num_blocks)  # burnout
        ax.plot(np.arange(b+1), data[:b+1], ls=":", c="C{}".format(id))
        dat = data[b:]
        ax.plot(np.arange(b, params.num_blocks), dat, c="C{}".format(id), label=label)
        ax.set_xlabel("block")
        ax.set_ylabel("{}".format(title))
        #if label != "E_{kin}" or label != "E_{pot}":
        txt = "Mean: {} : {:.3f} ± {:0.3f}"\
            .format(label, np.mean(dat), np.std(dat) / np.sqrt(len(dat)))
        print("       ", txt)
        ax.text(0.05, 0.1 + id * 0.1, txt, transform=ax.transAxes)
        print("       ", 'Variance to {} ratio: {:.5f}'.format(label, abs(np.std(dat) / np.mean(dat))))
        print("")
        ax.set_title("{} by block, $M = {}$".format(title, params.trotter))
        #ax.set_ylim((np.min(dat), np.max(dat)))
        ax.legend()


def plot_timesteps(ax1, ax2, calcs_by_tau):
    """
    Plot energy by time step (tau).
    """

    get_E = lambda obs: obs.E_tot
    get_R = lambda obs: obs.R[ord(0,1)]

    plot = (("Energy", "$E_{tot}$", ax1, get_E),
              ("Bond length", "$r_{H-H}$", ax2, get_R))

    for title, label, ax, getter in plot:

        Ms = [None for i in range(len(calcs_by_tau))]
        # Energies (and errorbars) by tau (/ Trotter)
        means = [None for i in range(len(calcs_by_tau))]
        errors = [None for i in range(len(calcs_by_tau))]

        for calc_id in range(len(calcs_by_tau)):
            obs = calcs_by_tau[calc_id].obs
            params = calcs_by_tau[calc_id].params
            b = int(params.burnout_ratio * params.num_blocks)  # burnout

            data = getter(obs)
            Ms[calc_id] = params.trotter
            means[calc_id] = data[b:].mean()
            errors[calc_id] = data[b:].std() / np.sqrt(len(data[b:]))

        ax.errorbar(Ms, means, errors)

        ax.text(0.05, 0.25 - 1 * 0.1,
                 "At $M = 1$ : {}$ = {:.3f} ({:.3f})\\;$Ha"
                 .format(label, means[0], errors[0]), transform=ax2.transAxes)

        ax.set_title("{} by Trotter number $M$".format(title))
        ax.set_xlim(1, Ms[-1])
        ax.set_ylabel("${}$".format(title))
        ax.set_xlabel("$M$")
        ax.set_xscale("log")
        #ax2.xaxis.set_major_forWmatter(ScalarFormatter())


def main():

    # H2
    params_atomic, state_atomic = init_systems.create_atomic_H2(num_iters=1000)
    params_electronic, state_electronic = init_systems.create_electronic_H2(num_iters=1000)

    for params, state, label in ((params_atomic, state_atomic, "atomic"),
                               (params_electronic, state_electronic, "electronic")):
        print("\n" + label)
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 10), num=label + " plots")
        calcs_by_trotter = calculate_with_different_trotters(params, state)
        plot_block_convergence(ax1, ax2, calcs_by_trotter, )
        plot_timesteps(ax3, ax4, calcs_by_trotter)

        walker_slider.walkers_interactive_trotters(calcs_by_trotter, num=label + " slider")

    plt.show()

if __name__=="__main__":
    main()

