"""
Pimc simulation

This code is based on exercise 11 problem 4. However the code has been rewritten / refactorized
to increase flexibility.

Main modifications to pimc_simple:
    - removal of quantum dot external potential
    - atomic simulation --> mass of hydrogen atom

Main modifications to my implementation of problem 4 in exercise 11
    - Refactorization and class structure
    - Bug fixed: energy calulation was wrong, lambda1 0.5 --> 1/(2*1800)
    - Morse potential
    - Bisection move function from problem 1 in exercise 10
    - Bug fixed: Temperature is not in kelvins, but in atomic units

"""

import numpy as np
import typing as tp
from scipy.special import erf
from copy import deepcopy

from data_structures import Parameters, State, Observables, Calculation
from calculation_misc import distance, keep_in_box, mid_point, potential_action, kinetic_action


def start(params: Parameters, init_state: State):
    """
    Simple path integral monte carlo calculation.
    :param params:      Parameters
    :param init_state:  Initial particle configuration
    :return:
    """
    state = deepcopy(init_state)
    
    print("Starting calulation T: {}, Trotter: {}"
          .format(params.get_T_in_kelvin(), params.trotter))

    obs = Observables(params, state.particles)

    for block_id in range(params.num_blocks):
        for iter_id in range(params.num_iters):
            time_slice1 = int(np.random.rand() * params.trotter)
            prt_index = int(np.random.rand() * len(state.particles))
            r1_old = state.walkers[time_slice1, prt_index].copy()

            A_RtoRp = bisection_move(time_slice1, prt_index, state, params)

            # If move degreases energy, allow move, else throw dice
            if (A_RtoRp > np.random.rand()):
                obs.acceptance[block_id] += 1.0
            else:
                state.walkers[time_slice1, prt_index, :] = r1_old

            if iter_id % params.obs_interval == 0:
                obs.sum_new_measurement(block_id, state, params)

        # Normalize obervables
        obs.normalize_to_mean(block_id, params)

    calc = Calculation(params, state, obs)
    return calc


def bisection_move(time_slice1, prt_index, state, params):
    M = params.trotter  # Trotter number
    sys_dim = params.sys_dim  # dimension
    tau = params.tau  # time step
    m = state.species[state.particles[prt_index]].mass
    lambda1 = 1 / (2*m)  # h^2 / (2m)
    sigma2 = lambda1 * tau  # diffusion variance
    sigma = np.sqrt(sigma2)

    # Three consecutive time slices
    time_slice0 = (time_slice1 + M - 1) % M
    time_slice2 = (time_slice1 + 1) % M

    r0 = state.walkers[time_slice0,prt_index].copy()
    r1 = deepcopy(state.walkers[time_slice1,prt_index])
    r2 = state.walkers[time_slice2,prt_index].copy()


    potential_action_old = potential_action(state, time_slice0, time_slice1, params) \
                         + potential_action(state, time_slice1, time_slice2, params)

    # Kinetic action is discarded with bisection sampling
    r02_ave = mid_point(r0, r2, params.periodic_box)
    if params.bisection:
        Rp = keep_in_box(r02_ave + np.random.randn(sys_dim) * sigma, params.periodic_box)
    else:
        Rp = keep_in_box(r02_ave + np.random.randn(sys_dim) * sigma * params.step_size,
                         params.periodic_box)

    state.walkers[time_slice1, prt_index] = Rp.copy()
    potential_action_new = potential_action(state, time_slice0, time_slice1, params) \
                         + potential_action(state, time_slice1, time_slice2, params)

    # Action difference
    deltaU = potential_action_new - potential_action_old

    if params.bisection:
        q_R_Rp = np.exp(-deltaU)
    else:
        KineticActionOld = kinetic_action(r0,r1,params,lambda1) \
                           + kinetic_action(r1,r2,params,lambda1)
        log_S_Rp_R = -distance(r1, r02_ave, params.periodic_box)**2 / (2*sigma2*params.step_size**2)
        log_S_R_Rp = -distance(Rp, r02_ave, params.periodic_box)**2 / (2*sigma2*params.step_size**2)
        KineticActionNew = kinetic_action(r0,Rp,params,lambda1) \
                           + kinetic_action(Rp,r2,params,lambda1)
        deltaK = KineticActionNew-KineticActionOld
        q_R_Rp = np.exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU)
        # print('delta K', deltaK)
        # print('delta logS', log_S_R_Rp-log_S_Rp_R)
        # print('exp(dS-dK)', np.exp(log_S_Rp_R - log_S_R_Rp - deltaK))
        # print('deltaU', deltaU)
        # print("--")
    #print("new, old, delta, q_R_Rp", potential_action_new, potential_action_old, deltaU, q_R_Rp)
    #print("r0, r2", r0, r2)
    #print("r02_ave, Rp", r02_ave, Rp)
    #print("--")
    # q sρ/(s'ρ')
    A_R_Rp = min(1.0, q_R_Rp)

    return A_R_Rp


