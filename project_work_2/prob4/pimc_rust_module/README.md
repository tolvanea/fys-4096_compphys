# PIMC, Python module written in Rust
Alpi Tolvanen, 2019

Licence: GPLv3+ (or MIT if you remove and fix GPL-dependecy 'rgsl' somehow)

This template is based on 'word-count' example found in 'pyo3'-library.

## Brief
This module implements the same functionality as in python code. This is intended to be a efficient module that can be called from python side.

Basic idea is that Parameters and State objects are generated on the python side, and then passed to function rust start_calculation_set(). This function does all the conversions from python types to corresponding rust types, and then starts multithreaded calculation. GIL is released for that while. Results are converted back to python objects, which are passed back to the python side, which draws the figures.

(Minor note: it should leak a bit memory, as it does not tell python carbage collector about the newly generated objects. I was too lazy to implement fix for that.)

## Installation
Install the module as pip3 package by running

```
cd pimc_rust_module
pip3 install .
```

## Run
Run program with python

```
cd prob4
python3 main.py
```
Alternatively few integration tests also exist:
```
cargo test
```

## Uninstall
To uninstall, run:

```
pip3 uninstall pimc-rust
```

