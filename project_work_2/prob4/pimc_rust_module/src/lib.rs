// Source adopted from
// https://github.com/tildeio/helix-website/blob/master/crates/word_count/src/lib.rs
// This code is based on Ilkka Kylänpääs path integral Monte Carlo -code sample
// presented in computational physics -cource (Tampere University 2019).

use pyo3::prelude::*; // I promise this glob import is used only for good purposes
use pyo3::{wrap_pyfunction, exceptions::ValueError, PyObject};
use pyo3::types::{PyList, PyTuple};
use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
#[allow(unused_imports)] // Most common structs and macros
use nd::{s, Axis, Array, ArrayBase, Data, ArrayView,
         Ix1, Ix2, Ix3, Ix4, Ix5,
         Array1, Array2, Array3, Array4, Array5};
use numpy::{PyArray3};
//#[allow(unused_imports)]
//use failure::Error;
use rayon::prelude::{ParallelIterator, IntoParallelIterator};

#[allow(non_snake_case)]
pub mod pimc;
#[allow(non_snake_case)]
pub mod init_systems;

use pimc::data_structures::{Parameters, State, Calculation};


/// Add two floats together.
///
/// This is a hello world example of this module. Not used anywhere
#[pyfunction]
fn addf(a: f64, b: f64) -> PyResult<f64> {
    if a.is_nan() || b.is_nan() {
        return Err(ValueError::py_err("Input is NaN".to_string()));
    }
    return Ok(a + b);
}


#[pyfunction]
fn start_calculation_set(py: Python<'_>, list_obj: PyObject) -> PyResult<PyObject> {
    // Initial settings contains parameters and initial state
    let initsett_list = PyList::try_from(list_obj.as_ref(py))
        .expect("Extracting list failed. (ErrCode 1)");

    let mut initsetts = Vec::<(Parameters, State)>::with_capacity(initsett_list.len());
    // New temporary vector is created because PyList does not implement ExactSizeIterator :(
    let mut initsett_vec = Vec::<(PyObject, PyObject)>::with_capacity(initsett_list.len());

    for initsett_pair_obj in initsett_list.iter() {
        let initsett_pair_pytuple: &PyTuple = PyTuple::try_from(initsett_pair_obj)
            .expect("Extracting tuple failed. (ErrCode 2)");

        let params_obj = initsett_pair_pytuple.get_item(0);
        let state_obj = initsett_pair_pytuple.get_item(1);
        initsett_vec.push((params_obj.to_object(py), state_obj.to_object(py)));

        let params = params_obj.extract::<Parameters>().unwrap();
        let state = state_obj.extract::<State>().unwrap();
        initsetts.push((params, state));
    }

    let results = py.allow_threads(move ||
        initsetts.into_par_iter().map(
            |(params, state)| {
                println!(
                    "Starting calculation in thread with T: {}, trotter: {}",
                    params.get_temperature_in_kelvin(), params.trotter
                );
                pimc::start(params, state)
            }
        ).collect::<Vec<Calculation>>()
    );

    // Instead of implementing all methods for rust_obj -> py_obj, just steal and
    // use already existing python objects. (Lazy, yes, but it should work as well.)
    let results_itr = results.iter().zip(initsett_vec)
        .map(|(calc, (params_obj, state_obj))| {
            // mutate walkers to the latest position, so returned value is up to date
            let walkers_obj = state_obj.getattr(py, "walkers")
                .expect("\nGetting 'walkers' attribute failed. (ErrCode 2.01)");
            let walkers_arr = walkers_obj.extract::<&PyArray3<f64>>(py)
                .expect("\nExtracting 'walkers: &PyArray3<f64>' failed. (ErrCode 7)");
            for (idx, &v) in calc.state.walkers.indexed_iter() {
                *walkers_arr.get_mut(idx).unwrap() = v;
            }

            calc.to_pyobject(py, params_obj.clone_ref(py), state_obj.clone_ref(py))
        });
    let results_obj = PyList::new(py, results_itr).to_object(py);

//    for calc in results.iter() {
//        println!("Trotter: {}, E_tot: {}({}) ",
//              calc.params.trotter, calc.obs.E_tot_mean, calc.obs.E_tot_err);
//    }
    return Ok(results_obj);
}

#[pymodule]
fn pimc_rust(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(addf))?;
    m.add_wrapped(wrap_pyfunction!(start_calculation_set))?;

    Ok(())
}