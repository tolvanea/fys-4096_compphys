/// This file contains miscellaneous functions needed in main program.
///
/// I trained with generics functions, so code is sometimes unnecessarily
/// generic. Also this genericity is left in a half way: if everythin would be
/// generic, the floating point accuracy could be easily changed.

// Lazy ass fix:

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
#[allow(unused_imports)] // Most common structs and macros
use nd::{s, Axis, Array, ArrayBase, Data, ArrayView,
         Ix1, Ix2, Ix3, Ix4, Ix5,
         Array1, Array2, Array3, Array4, Array5};
use ndarray_linalg as linalg;
use linalg::{Norm};
use rgsl;
use num_traits;

use crate::pimc::data_structures::{State, Parameters};
use crate::pimc::data_structures::PairPotential;

// My custom float trait because num::float is inferior
pub trait Float:
    num_traits::Float
    + ndarray::ScalarOperand
    + num_traits::NumAssign
    + num_traits::NumAssignOps<Self>
    + std::ops::Div<f64, Output=Self>
{}
impl Float for f64 {}


/// Modulo operator for numeric types and ndarray types
/// (This differs from remainder operator '%' with negative inputs.)
pub mod modulo_ops {
    use super::*;
    use num_traits::{Num, NumRef, RefNum};

    /// Generic modulo trait
    /// (It can be applied e.g. for int, float, ndarray...)
    pub trait Modulo<Rhs = Self> {
        type Output;
        fn modulo(self, rhs: Rhs) -> Self::Output;
    }

    /// Modulo operator implementation for primitive types, e.g. int, float...
    /// By value: "x + y"
    impl<T> Modulo<T> for T
    where
        T: Num + Copy
    {
        type Output = T;
        fn modulo(self, rhs: T) -> Self::Output {
            (self % rhs + rhs) % rhs
        }
    }

    /// Modulo operator implementation for primitive types, e.g. int, float...
    /// By value and reference: "x + &y"
    impl<'a, T> Modulo<&'a T> for T
    where
        T: Num + NumRef + Copy
    {
        type Output = T;
        fn modulo(self, rhs: &'a T) -> Self::Output {
            (self % rhs + rhs) % rhs
        }
    }

    /// Modulo operator implementation for primitive types, e.g. int, float...
    /// By reference and value: "&x + y"
    impl<'a, T> Modulo<T> for &'a T
    where
        T: Num + NumRef + RefNum<T> + Copy
    {
        type Output = T;
        fn modulo(self, rhs: T) -> Self::Output {
            (*self % rhs + rhs) % rhs
        }
    }

//    /// Modulo operator implementation for primitive types, e.g. int, float...
//    /// By reference and reference: "&x + &y"
//    impl<'a, 'b, T> Modulo<&'b T> for &'a T
//    where
//        T: Num + NumRef + RefNum<T> + Copy
//    {
//        type Output = T;
//        fn modulo(self, rhs: &'b T) {
//            (self % rhs + rhs) % rhs
//        }
//    }

    /// <rant> Modulo trait that is implemented to ndarrays only. F*ckin bummer,
    /// rust is goddamn restricted, that you must have separate trait for that.
    /// Like seriously, rust sucks ass. <rant>
    pub trait VecModulo<Rhs = Self> {
        type Output;
        fn modulo(self, rhs: Rhs) -> Self::Output;
    }

    /// Modulo operator for two arrays "mod(a, &b)"
    impl<'a, 'b, S1, S2, T, D> VecModulo<&'b ArrayBase<S2, D>> for &'a ArrayBase<S1, D>
    where
        S1: Data<Elem=T>,
        S2: Data<Elem=T>,
        T: Copy + Num + NumRef + RefNum<T>,
        D: ndarray::Dimension,
    {
        type Output = Array<T, D>;
        fn modulo(self, rhs: &'b ArrayBase<S2, D>) -> Self::Output {
            apply_fn(self, rhs, |a, b| a.modulo(*b))
        }
    }

    // Following function is another approach in implementing modulus for arrays (and also numbers)
    // It is a bit less generic, as the inputs must be the same type (e.g. two owned arrays)

    /// Modulo operator that works correctly with negative inputs, i.e. -2 mod 3 = 1
    /// Why on earth nobody has ever implemented modulo opertator in history of rust?
    /// It is not included in ndarray, rust nor in num-crate. Like seriously, c'mon.
    /// (This function is unit tested.)
    pub fn modulo_fn<'a, T>(x: &'a T, y: &'a T) -> T
    where
        &'a T: std::ops::Rem<&'a T, Output=T> + std::ops::Add<&'a T, Output=T>,
        T: std::ops::Add<&'a T, Output=T> + std::ops::Rem<&'a T, Output=T>
    {
        (x % y + y) % y
    }

}


/// Return 2-tuple so that smaller number is first.
/// Used in calling pair dict, as particle-particle key is defined so that
/// smaller index is first.
pub fn ord(a: usize, b: usize) -> (usize, usize) {
    if a < b {
        return (a, b);
    } else {
        return (b, a);
    }
}


/// Test if two floats are close each other.
/// This is missing too from standard libraries. Damnit rust is primitive.
pub fn is_close(a: f64, b: f64) -> bool {
    (a - b).abs() < (a + b + 1e-290).abs() * 1e-9
}

/// Construct new array by applying closure to two zipped arrays.
/// This function is unit tested.
pub fn apply_fn<S1, S2, T1, T2, T3, D, F>(
    first: &ArrayBase<S1, D>,
    second: &ArrayBase<S2, D>,
    f: F,
) -> Array<T3, D>
where
    S1: Data<Elem=T1>, // 1. array of type T1
    S2: Data<Elem=T2>, // 2. array of type T2
    T3: Copy,
    D: ndarray::Dimension,      // Dimension of both arrays
    F: Fn(&T1, &T2) -> T3,      // Function, T3 is out type
{
    assert!(first.shape() == second.shape());
    unsafe {
        let mut out = Array::<T3, D>::uninitialized(first.dim());
        nd::Zip::from(&mut out).and(first).and(second)
            .apply(|a, b, c| *a = f(b, c));
        return out;
    }
}

/// Periodic difference of two points
/// One peridoidix box side spans from -x/2 to x/2, where x is side length.
///
/// (This is alternative way to implement ndarray generics, if you compare it to
/// rest of code around)
///
/// ## Arguments
/// * 'r1', 'r2':       1d position vectors
/// * 'periodic_box':   Perioidic boundary box. Box span is [-L/2, L/2], where
///                     L is periodic_box side length.
pub fn diff_periodic<S1, S2, S3>(
    r1: &ArrayBase<S1, Ix1>,
    r2: &ArrayBase<S2, Ix1>,
    periodic_box: &ArrayBase<S3, Ix1>) -> Array1<f64>
where
    S1: Data<Elem=f64>,
    S2: Data<Elem=f64>,
    S3: Data<Elem=f64>,
{
    let diff = r2 - r1;
    let half_periodic_box = periodic_box / 2.0;//f64;
    let dists = &diff / &half_periodic_box;
    // 'multipliers' tells how many times we need to subtract periodic box from
    // original point to get it to fit within our box range [-L/2, L/2].
    let multipliers = dists.map(|x| {let o = *x as i32; o + (&o).signum()}) / 2;
    let offset = multipliers.map(|x| *x as f64) * periodic_box;
    let diff = diff - offset;

    // If we assumed new point is not very far away out side the box, naive
    // implementation would be:
//    debug_assert_eq!(
//        diff.iter().zip(periodic_box.iter())
//            .all(|(a, b)| *a < *b*1.5),
//        true,
//        "Assumed no particle has moved more than simulation box side length away."
//    );
//    for dim in 0..diff.len() {
//        if diff[dim] < -half_periodic_box[dim] {
//            diff[dim] += periodic_box[dim];
//        } else if diff[dim] >= half_periodic_box[dim] {
//            diff[dim] -= periodic_box[dim];
//        }
//    }
    return diff;
}


/// Calculate distance of two points with possibility of periodic boundaries.
///
/// ## Arguments
/// * 'r1', 'r2':       1d position vectors
/// * 'periodic_box':   Perioidic boundary box. Box span is [-L/2, L/2], where
///                     L is periodic_box side length.
pub fn distance<S1, S2, S3>(
    r1: &ArrayBase<S1, Ix1>,
    r2: &ArrayBase<S2, Ix1>,
    periodic_box: &Option<ArrayBase<S3, Ix1>>) -> f64
where
    S1: Data<Elem=f64>,
    S2: Data<Elem=f64>,
    S3: Data<Elem=f64>,
{
    match periodic_box {
        Some(arr) => diff_periodic(r1, r2, &arr).norm(),
        None => (r1 - r2).norm(),
    }
}


/// If system is periodic, then move point in it.
///
/// Assuming point is not more than periodic_box length away.
/// One peridoidix box side spans from -x/2 to x/2, where x is box side length.
/// (This function is unit tested.)
///
/// ## Arguments
/// * 'r1':             1d position vector
/// * 'periodic_box':   Perioidic boundary box. Box span is [-L/2, L/2], where
///                     L is periodic_box side length.

pub fn keep_in_periodic_box(
    r1: Array1<f64>,
    periodic_box: &Option<Array1<f64>>
) -> Array1<f64> {
    use modulo_ops::VecModulo;
    match periodic_box {
        Some(arr) => {
            let half_periodic_box = arr / 2.0;
            return (r1 + &half_periodic_box).modulo(arr) - half_periodic_box;
        }
        None => r1,
    }
}


/// Middle point of two points, with the possibility of periodic boundaries
/// One peridoidix box side is from -x/2 to x/2, where x is box side length.
/// (This function is unit tested.)
///
/// ## Arguments
/// * 'r1', 'r2':       1d position vectors
/// * 'periodic_box':   Perioidic boundary box. Box span is [-L/2, L/2], where
///                     L is periodic_box side length.
pub fn mid_point<S1, S2>(
    r1: &ArrayBase<S1, Ix1>,
    r2: &ArrayBase<S2, Ix1>,
    periodic_box: &Option<Array1<f64>>
) -> Array1<f64>
where
    S1: Data<Elem=f64>,
    S2: Data<Elem=f64>,
{
    match periodic_box {
        Some(arr) => {
            let d = diff_periodic(r1, r2, arr);
            return keep_in_periodic_box(r1 + &(d / 2.0), periodic_box);
        }
        None => { return (r2 + r1) / 2.0; }
    }
}


/// Potential action for move of one bead.
pub fn potential_action(
    state: &State,
    time_slice_id1: usize,
    time_slice_id2: usize,
    params: &Parameters
) -> f64 {
    return 0.5 * params.tau * (potential(state, time_slice_id1, params)
            + potential(state, time_slice_id2, params));
}

/// Calculates the kinetic action between two beads
pub fn kinetic_action<S1: Data<Elem=f64>, S2: Data<Elem=f64>>(
    r1: &ArrayBase<S1, Ix1>,
    r2: &ArrayBase<S2, Ix1>,
    params: &Parameters,
    lambda1: f64) -> f64
{
    return distance(r1, r2, &params.periodic_box).powi(2) / (lambda1 * params.tau * 4.0);
}


/// Morse potential from ex11 slides, eq 14
pub fn morse_potential(r: f64, de: f64, re: f64, a: f64) -> f64 {
    return de * (1.0 - (-a * (r - re)).exp()).powi(2) - de;
}


/// Thermal energy. Equation 2.36 in thesis of Kylänpää
pub fn energy(state: &State, params: &Parameters) -> (f64, f64) {
    let M = params.trotter;
    let d = params.sys_dim;
    let tau = params.tau;
    let mut U = 0.0;
    let mut K = 0.0;
    for i in 0..M {
        U += potential(state, i, &params);
        for j in 0..state.particles.len() {
            let m = state.species[state.particles[j]].mass;
            let lambda1 = 1.0 / (2.0 * m); // h ^ 2 / (2m);
            K += d as f64 / (2.0 * tau) -
                    distance(
                        &state.walkers.slice(s![i, j, ..]),
                        &state.walkers.slice(s![(i + 1) % M, j, ..]),
                        &params.periodic_box
                    ).powi(2) / (4.0 * lambda1 * tau.powi(2));
        }
    }
    return (K / M as f64, U / M  as f64);
}


/// Diagonal potential energy of particles.
pub fn potential(state: &State, time_slice_id: usize, params: &Parameters)-> f64 {
    use rgsl::error::erf;
    let mut V = 0.0;
    let walker = state.walkers.slice(s![time_slice_id, .., ..]);
    for i in 0..(walker.shape()[0] - 1){
        for j in i+1..walker.shape()[0]{
            let sp1_id = state.particles[i];  // species id, tells type of particle e.g. "H"
            let sp2_id = state.particles[j];
            let pot = &state.pair_potentials[&ord(sp1_id, sp2_id)];
            let r = distance(
                &walker.slice(s![i, ..]),
                &walker.slice(s![j, ..]),
                &params.periodic_box
            );
            match pot {
                PairPotential::Morse{de, re, a} => {
                    V += morse_potential(r, *de, *re, *a);
                },
                PairPotential::Coulomb => {
                    let charge1 = state.species[sp1_id].charge;
                    let charge2 = state.species[sp2_id].charge;
                    // sigma = σ
                    let sigma = 0.1 as f64;  // some random choice for smoothing parameter
                    V += charge1 * charge2 * erf(r / (2.0 * sigma).sqrt()) / r;
                }
                PairPotential::None => {
                    let symbol1 = &state.species[sp1_id].symbol;
                    let symbol2 = &state.species[sp2_id].symbol;
                    assert!(false,
                        "You did not specify a pair potential for pair {} {}",
                        symbol1, symbol2
                    );
                }
            }
        }
    }
    return V;
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_midpoint_without_perioidic_box() {
        // Case 1: Mid point does not change due to the periodic box
        let pbox: Array1<f64> = nd::arr1(&[4.0, 4.0]);
        let r1: Array1<f64> = nd::arr1(&[0.8, 1.3]);
        let r2: Array1<f64> = nd::arr1(&[1.2, 0.9]);
        let answer: Array1<f64> = nd::arr1(&[1.0, 1.1]);

        let mid1: Array1<f64> = mid_point(&r1, &r2, &None);
        let mid2: Array1<f64> = mid_point(&r1, &r2, &Some(pbox));

        assert!(mid1.all_close(&answer, 1e-6), "-- 1");
        assert!(mid2.all_close(&answer, 1e-6), "-- 2");
    }
    #[test]
    fn test_midpoint_with_perioidic_box() {
        // Case 2: Mid point changes due to the periodic box.
        //
        // Two points a and b, and three midpoints 1,2,3 periodic and not.
        //   2  –––––––––––––
        //     |c     |  a   |
        //     |      |      |
        //   0 |––––––+––1–– |
        //     |      |      |
        //     |      |  b   |
        //      –––––––––2––3
        //  -2        0        2
        let pbox = nd::arr1(&[4.0, 4.0]);
        let a = nd::arr1(&[0.8, 1.9]);
        let b = nd::arr1(&[1.2, -1.3]);
        let c = nd::arr1(&[-1.8, 1.9]);

        // non-periodic a-b
        let answer = nd::arr1(&[1.0, 0.3]);
        // periodic     a-b, cross other side
        let periodic_answer = nd::arr1(&[1.0, -1.7]);
        // periodic     b-c, cross both sides
        let periodic_answer_2 = nd::arr1(&[1.7, -1.7]);

        let mid1: Array1<f64> = mid_point(&a, &b, &None);
        let mid2: Array1<f64> = mid_point(&a, &b, &Some(pbox.clone()));
        let mid3: Array1<f64> = mid_point(&b, &c, &Some(pbox));

        assert!(mid1.all_close(&answer, 1e-6), "\n-- 3");
        assert!(mid2.all_close(&periodic_answer, 1e-6),
                "\n-- 4 \nleft {:?},  \nright {:?}", mid2, periodic_answer);
        assert!(mid3.all_close(&periodic_answer_2, 1e-6),
                "\n-- 5 \nleft {:?},  \nright {:?}", mid3, periodic_answer_2);
    }
    
    #[test]
    fn test_keep_in_box() {
        let pbox = nd::arr1(&[4.0, 4.0]);
        let r1: Array1<f64> = nd::arr1(&[-0.8, 1.3]);
        let r2: Array1<f64> = nd::arr1(&[1.2, 2.9]);
        let r3: Array1<f64> = nd::arr1(&[-3.2, 2.9]);
        let r2_periodic_answer = nd::arr1(&[1.2, -1.1]);
        let r3_periodic_answer = nd::arr1(&[0.8, -1.1]);

        // Should not move
        let in1 = keep_in_periodic_box(r1.clone(), &Some(pbox.clone()));
        assert!(r1.all_close(&in1, 1e-6),
                "\n-- 1\nleft {:?},  \nright {:?}", r1, in1);

        // Should not wrap as no box given
        let in2 = keep_in_periodic_box(r2.clone(), &None);
        assert!(r2.all_close(&in2, 1e-6),
                "\n-- 2\nleft {:?},  \nright {:?}", r2, in2);

        // Should wrap in positive y 2.9 -> -1.1
        let in3 = keep_in_periodic_box(r2.clone(), &Some(pbox.clone()));
        assert!(r2_periodic_answer.all_close(&in3, 1e-6),
                "\n-- 3\nleft {:?},  \nright {:?}", r2_periodic_answer, in3);

        // Should wrap in negative x and positive y
        // (-3.2, 2.9) -> (0.8, -1.1)
        let in4 = keep_in_periodic_box(r3.clone(), &Some(pbox.clone()));
        assert!(r3_periodic_answer.all_close(&in4, 1e-6),
                "\n-- 4\nleft {:?},  \nright {:?}", r3_periodic_answer, in4);
    }

    /// Test modulo functions
    ///
    /// * modulo_fn(&a, &b)
    /// * a.modulo(b)
    #[test]
    fn test_modulo() {
        use modulo_ops::Modulo;

        assert!(modulo_ops::modulo_fn(& 8, &3) ==  2);
        assert!(modulo_ops::modulo_fn(& 8, &-3) == -1);
        assert!(modulo_ops::modulo_fn(&-8, &3) ==  1);
        assert!(modulo_ops::modulo_fn(&-8, &-3) == -2);

        assert!(( 8).modulo( 3) ==  2);
        assert!(( 8).modulo(-3) == -1);
        assert!((-8).modulo( 3) ==  1);
        assert!((-8).modulo(-3) == -2);

        assert!(is_close(modulo_ops::modulo_fn(& 8.0, &3.0), 2.0));
        assert!(is_close(modulo_ops::modulo_fn(& 8.0, &-3.0), -1.0));
        assert!(is_close(modulo_ops::modulo_fn(&-8.0, &3.0), 1.0));
        assert!(is_close(modulo_ops::modulo_fn(&-8.0, &-3.0), -2.0));

        assert!(is_close(( 8.0).modulo( 3.0),  2.0));
        assert!(is_close(( 8.0).modulo(-3.0), -1.0));
        assert!(is_close((-8.0).modulo( 3.0),  1.0));
        assert!(is_close((-8.0).modulo(-3.0), -2.0));

    }

    #[test]
    fn test_apply_fn() {
        let a = nd::arr1(&[1.0, 1.4, -1.4, 2.0, 3.6]);
        let b = nd::arr1(&[1, 1, 2, 2, 2]);

        // Cumbersome ndarray way
        let mut c1 = a.clone();
        let mut d1 = a.clone();
        let mut e1 = a.clone();
        c1.zip_mut_with(&b, |a_, b_| *a_ %= *b_ as f64);
        d1.zip_mut_with(&b, |_a_, _b_| ());
        e1.zip_mut_with(&b, |_a_, b_| *_a_ = *b_ as f64);

        // To be honest I dont undestand why this is not the same as above.
//        let mut c1 = a.clone();
//        let mut d1 = a.clone();
//        let mut e1 = a.clone();
//        c1.zip_mut_with(&b, |&mut mut a_, &b_| a_ %= b_ as f64);
//        d1.zip_mut_with(&b, |&mut mut _a_, &_b_| ());
//        e1.zip_mut_with(&b, |&mut mut _a_, &b_| _a_ = b_ as f64);

        // My better way
        let c2 = apply_fn(&a, &b, |&a_, &b_| a_ % (b_ as f64));
        let d2 = apply_fn(&a, &b, |&a_, &_b_| a_);
        let e2 = apply_fn(&a, &b, |&_a_, &b_| b_ as f64);

        assert!(c2.all_close(&c1, 1e-9));
        assert!(d2.all_close(&d1, 1e-9));
        assert!(e2.all_close(&e1, 1e-9));

    }
}