/// This file contains data structures of the pimc-program.
///
/// The ErrCode in error messages are because if program crashes, it tells
/// precisely where the error originates from. (I couldn't enable backtrace)

use pyo3::prelude::*;
use pyo3::{PyTryFrom, PyResult, FromPyObject, IntoPyObject, PyNativeType};
use pyo3::types::{PyList, PyDict, PyTuple, PyAny};
use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
#[allow(unused_imports)] // Most common structs and macros
use nd::{s, Axis, Array, ArrayBase, Data, ArrayView,
         Ix1, Ix2, Ix3, Ix4, Ix5,
         Array1, Array2, Array3, Array4, Array5};
//use failure::Error;
use std::collections::HashMap;
#[allow(unused_imports)] // Most common structs and macros
use numpy::{PyArray, PyArray1, PyArray3, PyArray4, IntoPyArray, PyArrayDyn, TypeNum};

use crate::pimc::calculation_misc::{ord, energy, distance, is_close};

// https://en.wikipedia.org/wiki/Atomic_units
const AU_TEMPERATURE_IN_KELVIN: f64 = 3.157746e5;

/// Spin of particle
#[derive(Clone, Debug)]
pub enum Spin {
    Up,     // Corresponds spin=1
    Down,   // Corresponds spin=0
}

/// Type of particle (Eg. electron, proton, atomic H with pair potential...)
#[derive(Clone)]
pub struct ParticleSpecies {
    pub symbol: String,
    pub mass: f64,
    pub spin: Spin,
    pub charge: f64,
}

#[derive(Clone, Debug)]
pub enum PairPotential {
    Coulomb,
    Morse {de: f64, re: f64, a: f64},
    None,
}

impl<'a> FromPyObject<'a> for ParticleSpecies {
    fn extract(any: &'a PyAny) -> PyResult<Self> {
        let py = any.py();
        let obj = any.to_object(py);
        let spin = obj.getattr(py, "spin")
            .expect("\nGetting 'spin' attribute failed. (ErrCode 2.1)")
            .extract::<i64>(py)
            .expect("\nExtracting 'spin: i64' failed. (ErrCode 2.2)");
        assert!(spin <= 1, "Spin can be only up (1) or down (0).");
        let spin = if spin == 0 {Spin::Down} else {Spin::Up};
        return Ok(ParticleSpecies{
            symbol: obj.getattr(py, "symbol")
                .expect("\nGetting 'symbol' attribute failed. (ErrCode 2.3)")
                .extract::<String>(py)
                .expect("\nExtracting 'symbol: String' failed. (ErrCode 2.4)"),
            mass: obj.getattr(py, "mass")
                .expect("\nGetting 'mass' attribute failed. (ErrCode 2.5)")
                .extract::<f64>(py)
                .expect("\nExtracting 'mass: f64' failed. (ErrCode 2.6)"),
            spin: spin,
            charge: obj.getattr(py, "charge")
                .expect("\nGetting 'charge' attribute failed. (ErrCode 2.7)")
                .extract::<f64>(py)
                .expect("\nExtracting 'charge: f64' failed. (ErrCode 2.8)"),
        })
    }
}

///Parameters of a system. (Particle configuration is defined by 'State'.)
#[derive(Clone, PartialEq, Debug)]
pub struct Parameters {
    /// Time step
    pub tau: f64,
    /// Temperature
    pub temperature: f64,
    /// system dimension
    pub sys_dim: usize,
    /// Trotter number
    pub trotter: usize,
    /// Number of blocks, i.e. averaged observable count
    pub num_blocks: usize,
    /// Number of random walker moves within block
    pub num_iters: usize,
    /// How big portion of simulation is discarded (between 0 - 1)
    pub burnout_blocks: usize,
    /// How many random moves will be taken between measurements
    pub obs_interval: usize,
    /// Pair correlation vector resolution
    pub pair_correlation_gridsize: usize,
    /// Pair correlation range (min is 0.0)
    pub pair_correlation_max_range: f64,
    /// Perioidic box. If None, then no box.
    pub periodic_box: Option<Array1<f64>>,
    /// Use bisection which speeds up process.
    ///
    /// Sometimes it may be so that acceptance with bisection is very low. Then it may be better
    /// to calculate kinetic action with custom step that is smaller than fixed bisection step.
    /// Then it may bee good to turn bisection to false.
    pub bisection: bool,
    /// Gaussian step scale. This is used only if 'bisection' is set to False'
    /// If step_size is 1.0, then it has no difference to 'bisection=true'
    pub step_size: f64
}

impl Parameters {
    pub fn new (
        tau: f64,
        temperature: f64,
        trotter: usize,
        sys_dim: usize,
        num_blocks: usize,
        num_iters: usize,
    ) -> Parameters {
        let burnout_ratio = 0.3;
        let burnout_blocks = (burnout_ratio * (num_blocks as f64)) as usize;
        return Parameters {
            tau,
            temperature,
            sys_dim,
            trotter,
            num_blocks,
            num_iters,
            burnout_blocks,
            obs_interval: 5,
            pair_correlation_gridsize: 128,
            pair_correlation_max_range: 9.0,
            periodic_box: None,
            bisection: true,
            step_size: 1.0,
        };
    }

    pub fn convert_temperature_to_au(temperature: f64) -> f64 {
        return temperature / AU_TEMPERATURE_IN_KELVIN
    }

    pub fn get_temperature_in_kelvin(&self) -> f64 {
        return self.temperature * AU_TEMPERATURE_IN_KELVIN
    }
}



impl<'a> FromPyObject<'a> for Parameters {
    fn extract(any: &'a PyAny) -> PyResult<Self> {
        let py = any.py();
        let obj = any.to_object(py);
        let burnout_ratio = obj.getattr(py, "burnout_ratio")
            .expect("\nGetting 'burnout_ratio' attribute failed. (ErrCode 3)")
            .extract::<f64>(py)
            .expect("\nExtracting 'burnout_ratio: f64' failed. (ErrCode 4)");
        let num_blocks = obj.getattr(py, "num_blocks")
            .expect("\nGetting 'num_blocks' attribute failed. (ErrCode 5)")
            .extract::<usize>(py)
            .expect("\nExtracting 'num_blocks: usize' failed. (ErrCode 6)");
        let burnout_blocks = (burnout_ratio * (num_blocks as f64)) as usize;

        let python_periodic_box = obj.getattr(py, "periodic_box")
            .expect("\nGetting 'periodic_box' attribute failed. (ErrCode 6)");
        let periodic_box;
        if python_periodic_box.is_none() {
            periodic_box = None;
        } else {
            let py_array = python_periodic_box.extract::<&PyArray1<f64>>(py)
                .expect("\nExtracting '&PyArray1<f64>' failed. (ErrCode 7)");
            periodic_box = Some(py_array.as_array().to_owned());
        }

        return Ok(Parameters {
            tau: obj.getattr(py, "tau")
                .expect("\nGetting 'tau' attribute failed. (ErrCode 8)")
                .extract::<f64>(py)
                .expect("\nExtracting 'tau: f64' failed. (ErrCode 9)"),
            temperature: obj.getattr(py, "temperature")
                .expect("\nGetting 'temperature' attribute failed. (ErrCode 10)")
                .extract::<f64>(py)
                .expect("\nExtracting 'temperature: f64' failed. (ErrCode 11)"),
            sys_dim: obj.getattr(py, "sys_dim")
                .expect("\nGetting 'sys_dim' attribute failed. (ErrCode 12)")
                .extract::<usize>(py)
                .expect("\nExtracting 'sys_dim: usize' failed. (ErrCode 13)"),
            trotter: obj.getattr(py, "trotter")
                .expect("\nGetting 'trotter' attribute failed. (ErrCode 14)")
                .extract::<usize>(py)
                .expect("\nExtracting 'trotter: usize' failed. (ErrCode 15)"),
            num_blocks,
            num_iters: obj.getattr(py, "num_iters")
                .expect("\nGetting 'num_iters' attribute failed. (ErrCode 16)")
                .extract::<usize>(py)
                .expect("\nExtracting 'num_iters: usize' failed. (ErrCode 17)"),
            burnout_blocks,
            obs_interval: obj.getattr(py, "obs_interval")
                .expect("\nGetting 'obs_interval' attribute failed. (ErrCode 18)")
                .extract::<usize>(py)
                .expect("\nExtracting 'obs_interval: usize' failed. (ErrCode 19)"),
            pair_correlation_gridsize: obj.getattr(py, "pair_correlation_gridsize")
                .expect("\nGetting 'pair_correlation_gridsize' attribute failed. (ErrCode 20)")
                .extract::<usize>(py)
                .expect("\nExtracting 'pair_correlation_gridsize: usize' failed. (ErrCode 21)"),
            pair_correlation_max_range: obj.getattr(py, "pair_correlation_max_range")
                .expect("\nGetting 'pair_correlation_max_range' attribute failed. (ErrCode 22)")
                .extract::<f64>(py)
                .expect("\nExtracting 'pair_correlation_max_range: f64' failed. (ErrCode 23)"),
            periodic_box: periodic_box,
            bisection: obj.getattr(py, "bisection")
                .expect("\nGetting 'bisection' attribute failed. (ErrCode 24)")
                .extract::<bool>(py)
                .expect("\nExtracting 'bisection: bool' failed. (ErrCode 25)"),
            step_size: obj.getattr(py, "step_size")
                .expect("\nGetting 'step_size' attribute failed. (ErrCode 26)")
                .extract::<f64>(py)
                .expect("\nExtracting 'step_size: f64' failed. (ErrCode 27)"),
        })
        // A better way to handle errors would have been using '?' instead
        // of 'expect()'. Well, this is just verbose syntax..
    }
}


/// Particle configuration: Locations, and species types of each particle.
#[derive(Clone)]
pub struct State {
    /// 'walkers': position of system at some step of random walk
    /// same row - same time slice,
    /// same column - same particle
    /// same depth - same x/y/z coordinate
    pub walkers: Array3<f64>,

    /// 'species': all different species in system
    /// e.g. [H_spin0, H_spin1, He_spin0]
    pub species: Array1<ParticleSpecies>,

    /// 'particles': 'species'-indices of all the particles
    /// e.g. [1, 0, 2, 1]. In this case above particle
    /// 'walkers[:,1,:]' would correspond species "H_spin0".
    pub particles: Array1<usize>,

    /// 'pair_potentials': Potential-types corresponding each species pair
    /// key: (a,b) where a and b integers and a<=b.
    pub pair_potentials: HashMap<(usize, usize), PairPotential>
}


impl State {
    pub fn invariant(&self) {
        let s = self.species.len();

        assert_eq!(*self.particles.iter().max().unwrap(), s-1,
                   "Invalid indices. 'particles' must have indices for all species");
        for i1 in 0..self.particles.len() {
            for i2 in i1..self.particles.len() {
                let s1 = &self.particles[i1];
                let s2 = &self.particles[i2];
                let p1 = &self.species[*s1];
                let p2 = &self.species[*s2];
                assert!(
                    self.pair_potentials.contains_key(&(*s1, *s2)),
                    "Pairpotential not found for pair {}-{} e.g. {}[{:?}]-{}[{:?}]",
                    i1, i2,
                    p1.symbol, p1.spin,
                    p2.symbol, p2.spin,
                );
            }
        }
        let p = self.particles.len();
        let w = self.walkers.shape()[1];
        assert_eq!(
            w, p,
            "Wrong number of particles compared to walkers array."
        );
    }
}

impl<'a> FromPyObject<'a> for State {
    fn extract(any: &'a PyAny) -> PyResult<Self> {
        let py = any.py();
        let obj = any.to_object(py);

        let walkers_obj = obj.getattr(py, "walkers")
            .expect("\nGetting 'walkers' attribute failed. (ErrCode 28)");
        let walkers_pyarray = walkers_obj.extract::<&PyArray3<f64>>(py)
            .expect("\nExtracting 'walkers: &PyArray3<f64>' failed. (ErrCode 29)");
        let walkers = walkers_pyarray.as_array().to_owned();

        let species_obj = obj.getattr(py, "species")
            .expect("\nGetting 'species' attribute failed. (ErrCode 30)");
        let species_list = PyList::try_from(species_obj.as_ref(py))
            .expect("\nExtracting 'species: PyList' failed. (ErrCode 31)");
        let species = Array1::<ParticleSpecies>::from_shape_fn(
            species_list.len(),
            |idx| {
                //let ix = Ix1::from(idx);
                let obj = species_list.get_parked_item(idx as isize);
                obj.extract::<ParticleSpecies>(py).unwrap()
            }
        );

        let tmp = obj.getattr(py, "particles")
            .expect("\nGetting 'particles' attribute failed. (ErrCode 32)");
        let particles_pyarray = tmp.extract::<&PyArray1<i64>>(py)
            .expect("\nExtracting 'particles: &PyArray1<i64>' failed. (ErrCode 33)");
        let particles = particles_pyarray.as_array().to_owned().mapv(|v| v as usize);

        let pps_obj = obj.getattr(py, "pair_potentials")
            .expect("\nGetting 'pair_potentials' attribute failed. (ErrCode 34)");
        let pps_dict = pps_obj.extract::<&pyo3::types::PyDict>(py)
            .expect("\nExtracting 'PyDict' failed. (ErrCode 35)");
        let mut pair_potentials = HashMap::new();
        for (key, value) in pps_dict.into_iter() {
            let pair = key.extract::<(usize, usize)>()
                .expect("\nExtracting 'dict-key: (usize, usize)' failed. (ErrCode 36)");
            let tuple = PyTuple::try_from(value)
                .expect("\nExtracting 'dict-key: PyTuple' failed. (ErrCode 37)");
            let pp = match tuple.len() {
                0 => panic!("Empty pair potential tuple!"),
                1 => {
                    let name = value.extract::<String>()
                        .expect("\nExtracting 'dict-value: (String,)' failed. (ErrCode 38)");
                    match name.as_ref() {
                        "Coulombic" => PairPotential::Coulomb,
                        "None" => PairPotential::None,
                        _ => panic!("Unknown pair potential name or wrong number \
                        of pair potential parameters (=0)."),
                    }
                },
                4 => {
                    let (name, p1, p2, p3) = value.extract::<(String, f64, f64, f64)>()
                        .expect("\nExtracting 'dict-value: (String, f64, f64, f64)' failed. \
                        (ErrCode 37)");
                    match name.as_ref() {
                        "Morse" => PairPotential::Morse { de: p1, re: p2, a: p3 },
                        _ => panic!("Unknown pair potential name or wrong number \
                        of pair potential parameters (=3)."),
                    }
                },
                x => panic!("No matching pair potential for tupple that has this \
                many parametes {}.", x - 1),
            };
            pair_potentials.insert(pair, pp);
        }
        assert!(!pair_potentials.is_empty(), " Creating pair potentials failed somewhy. ");

        return Ok(State {
            walkers,
            species,
            particles,
            pair_potentials: pair_potentials,
        });
    }
}


/// Observables, which will be calculated during simulation.
#[allow(non_snake_case)]
#[derive(Clone)]
pub struct Observables {
        // Following observables are calculated for each block
        pub E_tot: Array1<f64>,
        pub E_kin: Array1<f64>,
        pub E_pot: Array1<f64>,
        pub acceptance: Array1<f64>,
        // Distances and correlation
        // Mean distance per species pair, Dict of 1d-arrays
        pub R: HashMap<(usize, usize), Array1<f64>>,  // mean distance R
        pub R_squared: HashMap<(usize, usize), Array1<f64>>,  // mean of R^2
        pub R_inverse: HashMap<(usize, usize), Array1<f64>>,  // mean of 1/R
        // Pair correlation per species. Dict of 2d-arrays (Not used nor tested!)
        pub pair_correlation: HashMap<(usize, usize), Array2<f64>>,
        // Walker positions by block 4d array (= 1d × 3d)
        pub walkers_history: Array4<f64>,

        // These values are calculated means of all blocks (burn-out blocks excluded)
        pub E_tot_mean: f64,
        pub E_kin_mean: f64,
        pub E_pot_mean: f64,
        pub E_tot_err: f64,
        pub E_kin_err: f64,
        pub E_pot_err: f64,

}
impl Observables {
    pub fn new(params: &Parameters, particles: &Array1<usize>) -> Observables {
        let (R, pair_correlation) = Observables::create_empty_pair_distance_structures(
            particles,
            params.pair_correlation_gridsize,
            params.num_blocks
        );
        return Observables {
            E_tot: Array1::zeros(params.num_blocks),
            E_kin: Array1::zeros(params.num_blocks),
            E_pot: Array1::zeros(params.num_blocks),
            acceptance: Array1::zeros(params.num_blocks),
            R: R.clone(),
            R_squared: R.clone(),
            R_inverse: R,
            pair_correlation: pair_correlation,
            // Walker positions by block 4d array (= 1d × 3d)
            walkers_history: Array4::zeros((params.num_blocks,
                                            params.trotter,
                                            particles.len(),
                                            params.sys_dim)),
            E_tot_mean: 0.0,
            E_kin_mean: 0.0,
            E_pot_mean: 0.0,
            E_tot_err: 0.0,
            E_kin_err: 0.0,
            E_pot_err: 0.0,
        };
    }

    /// Creates empty 'R' and 'pair_correlation' data structures.
    ///
    /// They are dicts with particle species pair as a key.
    /// R contains arrays of size [blocks]. pair_correlation contains arrays
    /// of size [blocks ×  paircorrelation_gridsize].
    ///
    /// # Return
    ///
    ///  * Empty mean distance datastructure: Dict[(species1,species2), 1d-array],
    ///  * Empty distance distribution datastructure: Dict[(species1,species2), 2d-array]
    fn create_empty_pair_distance_structures(
        particles: &Array1<usize>,
        pair_correlation_gridsize: usize,
        num_blocks: usize,
    ) -> (HashMap<(usize, usize), Array1<f64>>, HashMap<(usize, usize), Array2<f64>>){

        let mut pair_correlation = HashMap::new();
        let mut R = HashMap::new();
        for i in 0..(particles.len()-1){
            for j in (i + 1)..particles.len(){
                let prt1_id = particles[i];
                let prt2_id = particles[j];

                R.insert(ord(prt1_id, prt2_id), Array1::zeros(num_blocks));

                pair_correlation.insert(
                    ord(prt1_id, prt2_id),
                    Array2::zeros((num_blocks, pair_correlation_gridsize)),
                );
            }
        }

        return (R, pair_correlation);
    }

    /// Calculates a new measurement within a block. Results must be normalized with
    /// 'normalize_to_mean()' after the block is completed.
    ///
    /// # Arguments
    ///
    /// * 'block_id'    - Current block
    /// * 'state'       - See 'State'
    /// * 'params'      - See 'Parameters'
    pub fn sum_new_measurement(&mut self, block_id: usize, state: &State, params: &Parameters) {
        debug_assert!(
            if block_id+1 < params.trotter {
                is_close(self.E_tot[block_id+1], 0.0)
            } else {
                true
            }, "Uh oh, this function must be called for fresh and empty blocks. \n\
            It seems you call it to already calculated blocks.\n\
            block: {:?}, E_tot[block+1]: {:?}", block_id, self.E_tot[block_id+1],
        );

        let (E_kin, E_pot) = energy(state, params);
        self.E_kin[block_id] += E_kin;
        self.E_pot[block_id] += E_pot;
        self.E_tot[block_id] += E_kin + E_pot;
        self.add_pair_distance_counts(block_id, state, params);
        let mut history_instant = self.walkers_history.slice_mut(s![block_id, .., .., ..]);
        history_instant.assign(&state.walkers);
    }

    /// Part of 'sum_new_measurement()'. Adds particle distance information to observables
    /// R R^2 1/R and pair_correlation.
    ///
    /// # Arguments
    /// * 'block_id'        - Current block
    /// * 'state'           - See 'State'
    /// * 'params           - See 'Parameters'
    fn add_pair_distance_counts(
        &mut self,
        block_id: usize,
        state: &State,
        params: &Parameters)
    {
        // Iterate over all timesteps
        for timestep in 0..state.walkers.shape()[0] {
            for prt1_id in 0..(state.walkers.shape()[1] - 1) {
                for prt2_id in (prt1_id + 1)..state.walkers.shape()[1] {
                    let prt1_pos = state.walkers.slice(s![timestep, prt1_id, ..]);
                    let prt2_pos = state.walkers.slice(s![timestep, prt2_id, ..]);
                    // species_id
                    let prt1_sp = state.particles[prt1_id];
                    let prt2_sp = state.particles[prt2_id];

                    let r = distance(&prt1_pos, &prt2_pos, &params.periodic_box);

                    self.R.get_mut(&ord(prt1_sp, prt2_sp)).unwrap()[block_id] += r;
                    self.R_squared.get_mut(&ord(prt1_sp, prt2_sp)).unwrap()[block_id] += r.powi(2);
                    self.R_inverse.get_mut(&ord(prt1_sp, prt2_sp)).unwrap()[block_id] += 1.0 / r;

                    // Paircorrelation
                    let c = params.pair_correlation_max_range - r;
                    if 0.0 <= c && c < 1.0 {  // Particle in paircorrelation range
                        let r_id = (c * params.pair_correlation_gridsize as f64) as usize;
                        self.pair_correlation.get_mut(&ord(prt1_sp, prt2_sp))
                            .unwrap()[[block_id, r_id]] += 1.0;
                    }
                }
            }
        }
    }
    /// Calculates the means of sums of observables.
    ///
    /// Also divides with the number of measurements.
    ///
    /// # Arguments
    ///
    /// * 'block'       Current block idx
    /// * 'params'      See 'Parameters'
    pub fn normalize_to_mean(&mut self, block: usize, params: &Parameters) {
        let obs_count = params.num_iters / params.obs_interval;
        debug_assert!(
            if block+1 < params.trotter {
                is_close(self.E_tot[block+1], 0.0)
            } else {
                true
            }, "Uh oh, this function must be called for fresh and empty blocks. \n\
            It seems you call it to already calculated blocks.\n\
            block: {:?}, E_tot[block+1]: {:?}", block, self.E_tot[block+1],
        );

        self.E_tot[block] /= obs_count as f64;
        self.E_kin[block] /= obs_count as f64;
        self.E_pot[block] /= obs_count as f64;
        for (pair, pc) in self.pair_correlation.iter_mut(){
            self.R.get_mut(&pair).unwrap()[block] /= (obs_count * params.trotter) as f64;
            self.R_squared.get_mut(&pair).unwrap()[block] /= (obs_count * params.trotter) as f64;
            self.R_inverse.get_mut(&pair).unwrap()[block] /= (obs_count * params.trotter) as f64;
            let mut pc_of_block = pc.slice_mut(s![block, ..]);
            pc_of_block /= (obs_count * params.trotter) as f64;
        }
        self.acceptance[block] /= params.num_iters as f64;
        if is_close(((block+1) as f64).log2() % 1.0, 0.0) {
            println!("Block {}/{}", block + 1, params.num_blocks);
            println!("    E = {:.5}", self.E_tot[block]);
            println!("    Acc = {:.5}", self.acceptance[block]);
            println!("---");
//            dbg!(self.walkers_history.slice(s![block, 0, .., ..]));
//            println!("---");
//            dbg!(self.walkers_history.slice(s![block, 3, .., ..]));
//            println!("---");
        }
    }

    pub fn calculate_mean_energies(&mut self, params: &Parameters){
        use rgsl::statistics::sd;
        let burn = params.burnout_blocks;
        let len = params.num_blocks - burn;
        self.E_tot_mean = self.E_tot.slice(s![burn..,]).sum() / (len as f64);
        self.E_kin_mean = self.E_tot.slice(s![burn..,]).sum() / (len as f64);
        self.E_pot_mean = self.E_tot.slice(s![burn..,]).sum() / (len as f64);
        // std = sd(data: &[f64], stride: usize, n: usize)
        self.E_tot_err = sd(self.E_tot.slice(s![burn..,]).as_slice().unwrap(), 1, len)
            / (len as f64).sqrt();
        self.E_kin_err = sd(self.E_kin.slice(s![burn..,]).as_slice().unwrap(), 1, len)
            / (len as f64).sqrt();
        self.E_pot_err = sd(self.E_pot.slice(s![burn..,]).as_slice().unwrap(), 1, len)
            / (len as f64).sqrt();
    }
}

/// Observables object that can be passed to python side.
/// Almost identical with Observables, but everything is PyObject.
// TODO implement PyGCProtocol for proper memory release, i.e. THIS LEAKS MEMORY
#[pyclass(name=Observables, dict)]
struct PyObservables {
    // Following observables are calculated for each block
    pub E_tot: Py<PyArray1<f64>>,
    pub E_kin: Py<PyArray1<f64>>,
    pub E_pot: Py<PyArray1<f64>>,
    pub acceptance: Py<PyArray1<f64>>,
    // Distances and correlation
    // Mean distance per species pair, Dict of 1d-arrays
    pub R: Py<PyDict>,  // HashMap<(usize, usize), Array1<f64>>,
    pub R_squared: Py<PyDict>,  // HashMap<(usize, usize), Array1<f64>>,
    pub R_inverse: Py<PyDict>,  // HashMap<(usize, usize), Array1<f64>>,
    // Pair correlation per species. Dict of 2d-arrays (Not used nor tested!)
    pub pair_correlation: Py<PyDict>,  // HashMap<(usize, usize), Array2<f64>>,
    // Walker positions by block 4d array (= 1d × 3d)
    pub walkers_history: Py<PyArray4<f64>>,
    // These values are calculated means of all blocks (burn-out blocks excluded)
    #[pyo3(get)]
    pub E_tot_mean: f64,
    #[pyo3(get)]
    pub E_kin_mean: f64,
    #[pyo3(get)]
    pub E_pot_mean: f64,
    #[pyo3(get)]
    pub E_tot_err: f64,
    #[pyo3(get)]
    pub E_kin_err: f64,
    #[pyo3(get)]
    pub E_pot_err: f64,
}

impl PyObservables {
    fn new_pyobj(py: Python<'_>, obs: &Observables) -> Py<PyObservables> {
        let ob = PyObservables {
            E_tot: PyArray::from_owned_array(py, obs.E_tot.to_owned()).to_owned(),
            E_kin: PyArray::from_owned_array(py, obs.E_kin.to_owned()).to_owned(),
            E_pot: PyArray::from_owned_array(py, obs.E_pot.to_owned()).to_owned(),
            acceptance: PyArray::from_owned_array(py, obs.acceptance.to_owned()).to_owned(),
            R: Self::hashmap_to_pydict(py, &obs.R),
            R_squared: Self::hashmap_to_pydict(py, &obs.R_squared),
            R_inverse: Self::hashmap_to_pydict(py, &obs.R_inverse),
            pair_correlation: Self::hashmap_to_pydict(py, &obs.pair_correlation),
            walkers_history:
            PyArray::from_owned_array(py, obs.walkers_history.to_owned()).to_owned(),
            E_tot_mean: obs.E_tot_mean,
            E_kin_mean: obs.E_kin_mean,
            E_pot_mean: obs.E_pot_mean,
            E_tot_err: obs.E_tot_err,
            E_kin_err: obs.E_kin_err,
            E_pot_err: obs.E_pot_err,
        };
        return Py::new(py, ob).unwrap();
    }

    fn hashmap_to_pydict<D: Dimension>(
        py: Python<'_>,
        pp_map: &HashMap<(usize, usize), Array<f64, D>>
    ) -> Py<PyDict> {
        let pp_dict = PyDict::new(py);
        for (key, value) in pp_map.iter() {
            let pp = PyArray::from_owned_array(py, value.to_owned()).to_object(py);
            pp_dict.set_item(key.clone(), pp).unwrap();
        }
        return pp_dict.to_object(py).extract::<Py<PyDict>>(py).unwrap();
    }
}

/// Obserables class that will be passed to python side.
// TODO implement PyGCProtocol for proper memory release, i.e. THIS LEAKS MEMORY
#[pymethods]
impl PyObservables {
    #[getter]
    fn get_E_tot(&self) -> PyResult<&Py<PyArray1<f64>>> {
        Ok(&self.E_tot)
    }
    #[getter]
    fn get_E_kin(&self) -> PyResult<&Py<PyArray1<f64>>> {
        Ok(&self.E_kin)
    }
    #[getter]
    fn get_E_pot(&self) -> PyResult<&Py<PyArray1<f64>>> {
        Ok(&self.E_pot)
    }
    #[getter]
    fn get_acceptance(&self) -> PyResult<&Py<PyArray1<f64>>> {
        Ok(&self.acceptance)
    }
    #[getter]
    fn get_R(&self) -> PyResult<&Py<PyDict>> {
        Ok(&self.R)
    }
    #[getter]
    fn get_R_squared(&self) -> PyResult<&Py<PyDict>> {
        Ok(&self.R_squared)
    }
    #[getter]
    fn get_R_inverse(&self) -> PyResult<&Py<PyDict>> {
        Ok(&self.R_inverse)
    }
    #[getter]
    fn get_pair_correlation(&self) -> PyResult<&Py<PyDict>> {
        Ok(&self.pair_correlation)
    }
    #[getter]
    fn get_walkers_history(&self) -> PyResult<&Py<PyArray4<f64>>> {
        Ok(&self.walkers_history)
    }
}


/// Specifies an ended simulation of system: Parameters, particles and measured observables.
#[derive(Clone)]
pub struct Calculation {
    /// See Parameters
    pub params: Parameters,
    /// See 'State' (type, location, spin) ...
    pub state: State,
    /// See 'Observables'
    pub obs: Observables,
}

impl Calculation {
    pub fn to_pyobject(
        &self,
        py: Python<'_>,
        params_obj: PyObject,
        state_obj: PyObject
    ) -> Py<PyCalculation> {
        let obs_obj = PyObservables::new_pyobj(py, &self.obs);
        let calc = PyCalculation{
            params: params_obj,
            state: state_obj,
            obs: obs_obj,
        };
        return Py::new(py, calc).unwrap()
    }
}
/// Calculation class that will be passed to python side.
// TODO implement PyGCProtocol for proper memory release, i.e. THIS LEAKS MEMORY
#[pyclass(name=Calculation, dict)]
pub struct PyCalculation {
    //#[pyo3(get)]
    params: PyObject,
    //#[pyo3(get)]
    state: PyObject,
    //#[pyo3(get)]
    obs: Py<PyObservables>,
}

#[pymethods]
impl PyCalculation {
    #[getter]
    fn get_params(&self) -> PyResult<&PyObject> {
        Ok(&self.params)
    }

    #[getter]
    fn get_state(&self) -> PyResult<&PyObject> {
        Ok(&self.state)
    }

    #[getter]
    fn get_obs(&self) -> PyResult<&Py<PyObservables>> {
        Ok(&self.obs)
    }
}
