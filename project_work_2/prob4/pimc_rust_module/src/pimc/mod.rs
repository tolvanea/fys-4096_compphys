#[allow(non_snake_case)]
#[allow(dead_code)]
pub mod data_structures;
#[allow(non_snake_case)]
#[allow(dead_code)]
pub mod calculation_misc;

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
#[allow(unused_imports)] // Most common structs and macros
use nd::{s, Axis, Array, ArrayBase,
         Array1, Array2, Array3, Array4, Array5,
         ArrayView1 as Av1, ArrayView2 as Av2, ArrayView3 as Av3, ArrayView4 as Av4};

use rand::Rng;
use rand::distributions::{Normal};
use ndarray_rand::RandomExt;

use data_structures::{Parameters, State, Observables, Calculation};
use calculation_misc::{kinetic_action, keep_in_periodic_box, potential_action, mid_point,
                           distance};

/// Simple path integral monte carlo calculation.
pub fn start(params: Parameters, init_state: State) -> Calculation{
    let mut state = init_state.clone();

    state.invariant();  // Test for errors

    let mut obs = Observables::new(&params, &state.particles);

    for block_id in 0..params.num_blocks {
        for iter_id in 0..params.num_iters {
            let mut rng = rand::thread_rng();;

            let time_slice1: usize = rng.gen_range(0, params.trotter);
            let prt_index: usize = rng.gen_range(0, state.particles.len());
            let r1_old = state.walkers.slice(s![time_slice1, prt_index, ..]).to_owned();

            let A_RtoRp = bisection_move(time_slice1, prt_index, &mut state, &params);

            // If move degreases energy, allow move, else throw dice
            if A_RtoRp > rng.gen_range(0.0f64, 1.0f64) {
                obs.acceptance[block_id] += 1.0;
            } else {
                state.walkers.slice_mut(s![time_slice1, prt_index, ..]).assign(&r1_old);
            }

            if iter_id % params.obs_interval == 0 {
                obs.sum_new_measurement(block_id, &state, &params);
            }
        }
        // Normalize obervables
        obs.normalize_to_mean(block_id, &params);
    }

    obs.calculate_mean_energies(&params);

    return Calculation{params, state, obs};
}


pub fn bisection_move(
    time_slice1: usize,
    prt_index: usize,
    state: &mut State,
    params: &Parameters
) -> f64 {
    let M = params.trotter;  // Trotter number
    let sys_dim = params.sys_dim;  // dimension
    let tau = params.tau;  // time step
    let m = state.species[state.particles[prt_index]].mass;
    let lambda1 = 1.0 / (2.0 * m);  // h^2 / (2m)
    let sigma2 = lambda1 * tau;  // diffusion variance
    let sigma = sigma2.sqrt();

    // Three consecutive time slices
    let time_slice0 = (time_slice1 + M - 1) % M;
    let time_slice2 = (time_slice1 + 1) % M;

    // TODO
    //      Currently MC move is made first and then reversed if it is not optimal.
    //      Other way around there would no be need to copy so many times due
    //      to the mutability of 'states'.
    let r0: Array1<f64> = state.walkers.slice(s![time_slice0,prt_index, ..]).to_owned();
    let r1: Array1<f64> = state.walkers.slice(s![time_slice1,prt_index, ..]).to_owned();
    let r2: Array1<f64> = state.walkers.slice(s![time_slice2,prt_index, ..]).to_owned();


    let potential_action_old = potential_action(state, time_slice0, time_slice1, params)
        + potential_action(state, time_slice1, time_slice2, params);

    // Kinetic action is discarded with bisection sampling
    let r02_ave: Array1<f64> = mid_point(&r0, &r2, &params.periodic_box);
    let Rp;
    let rnd = Array1::random((sys_dim,), Normal::new(0.0, 1. as f64));

    let new_r = &r02_ave + &(rnd * sigma);
    if params.bisection {
        Rp = keep_in_periodic_box(new_r, &params.periodic_box);
    } else {
        Rp = keep_in_periodic_box(new_r * params.step_size, &params.periodic_box);
    }

    state.walkers.slice_mut(s![time_slice1, prt_index, ..]).assign(&Rp);
    let potential_action_new = potential_action(state, time_slice0, time_slice1, params)
        + potential_action(state, time_slice1, time_slice2, params);

    // Action difference
    let deltaU = potential_action_new - potential_action_old;

    let q_R_Rp;
    if params.bisection {
        q_R_Rp = (-deltaU).exp();
    } else {
        let kinetic_action_old = kinetic_action(&r0, &r1, params, lambda1)
            + kinetic_action(&r1, &r2, params, lambda1);
        let log_S_Rp_R = -distance(&r1, &r02_ave, &params.periodic_box).powi(2)
            / (2.0 * sigma2 * params.step_size.powi(2));
        let log_S_R_Rp = -distance(&Rp, &r02_ave, &params.periodic_box).powi(2)
            / (2.0 * sigma2 * params.step_size.powi(2));
        let kinetic_action_new = kinetic_action(&r0, &Rp, params, lambda1)
            + kinetic_action(&Rp, &r2, params, lambda1);
        let delta_K = kinetic_action_new - kinetic_action_old;
        q_R_Rp = (log_S_Rp_R - log_S_R_Rp - delta_K - deltaU).exp()
        // println!("delta K", delta_K);
        // println!("delta logS", log_S_R_Rp-log_S_Rp_R);
        // println!("exp(dS-dK)", exp(log_S_Rp_R - log_S_R_Rp - delta_K).exp());
        // println!("deltaU", deltaU);
        // println!("--");
        }
    // println!("new, old, delta, q_R_Rp", potential_action_new, potential_action_old, deltaU, q_R_Rp);
    // println!("r0, r2", r0, r2);
    // println!("r02_ave, Rp", r02_ave, Rp);
    // println!("--");
    //  q sρ/(s'ρ')
    let A_R_Rp = f64::min(1.0, q_R_Rp);

    return A_R_Rp;

}

/// This is place where I try different aspects of rust. Not related to the
/// program itself.
#[cfg(test)]
mod playground {
    use super::*;
    use calculation_misc::{modulo_ops::modulo_fn, apply_fn};
    #[test]
    fn try_modulus() {
        // Does modulus a % b work also with ndarray vectors?
        // - Yes.
        let a = nd::arr1(&[1.0, 2.0, 3.0, 4.0, 5.6]);
        let b = nd::arr1(&[2.0, 2.0, 3.0, 3.0, 3.6]);
        let aneg = - &a;

        // Real answers
        let a_mod_3 = nd::arr1(&[1.0, 2.0, 0.0, 1.0, 2.6]);
        let a_mod_b = nd::arr1(&[1.0, 0.0, 0.0, 1.0, 2.0]);

        let aneg_mod_b = nd::arr1(&[1.0, 0.0, 0.0, 2.0, 1.6]);
        let aneg_rem_b = apply_fn(&aneg, &b, |x, y| *x % *y);


        assert!((&a % 3.0).all_close(&a_mod_3, 1e-6),
                "\n1 -- \nleft: {:}, \nright: {:}\n", &a, &a_mod_3);

        assert!((&a % &b).all_close(&a_mod_b, 1e-6),
                "\n2-- \nleft: {:}, \nright: {:}\n", &b, &a_mod_b);

        assert!((&aneg % &b).all_close(&aneg_rem_b, 1e-6),
                "\n3-- \nleft: {:}, \nright: {:}\n", &aneg, &aneg_mod_b);

        assert!(((&aneg % &b + &b) % &b).all_close(&aneg_mod_b, 1e-6),
                "\n4-- \nleft: {:}, \nright: {:}\n", &aneg, &aneg_mod_b);

        //Also test my (generic) 'modulo' function
        let direct = modulo_fn(&aneg, &b);
        let elementwise = apply_fn(&aneg, &b, |x, y| modulo_fn(x, y));
        assert!(direct.all_close(&aneg_mod_b, 1e-6));
        assert!(elementwise.all_close(&aneg_mod_b, 1e-6));
    }
    #[test]
    fn try_struct_shorthand_syntax() {
        // Does struct fields swap places in struct initialization if fields are
        // have the same name as variables?
        // - Yes, nice syntax sugar.
        #[derive(Debug)]
        struct A {
            a: i32,
            b: i32,
        }

        let a = 1;
        let b = 2;

        let s = A{ b, a };
        println!("{:?}", s)
    }
}
