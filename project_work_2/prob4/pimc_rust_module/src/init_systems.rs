/// Initialize and create systems

use ndarray as nd;
#[allow(unused_imports)] // Traits
use nd::prelude::{Dimension, NdFloat, ShapeBuilder, AsArray, };
#[allow(unused_imports)] // Most common structs and macros
use nd::{s, Axis, Array, ArrayBase,
         Array1, Array2, Array3, Array4, Array5,
         ArrayView1 as Av1, ArrayView2 as Av2, ArrayView3 as Av3, ArrayView4 as Av4};

use crate::pimc::data_structures::{
    Parameters, State, Spin, ParticleSpecies, PairPotential};
use std::collections::HashMap;

use ndarray_rand::RandomExt;
use rand::distributions::{Uniform};


/// Atomic H2 system (H + H)
pub fn atomic_H2(T: f64, trotter: usize, num_iters: usize) -> (Parameters, State) {
    let au_in_kelvin = 3.157746e5;
    let T_au = T / au_in_kelvin;
    let tau = 1.0 / (T_au * trotter as f64);
    let dim = 3;
    let num_blocks = 100;

    let H = ParticleSpecies{
        symbol: "H".to_string(),  // proton
        mass: 1843.0,
        spin: Spin::Down,
        charge: 0.0,
    };

    let species = nd::arr1(&[H]);
    let particles = nd::arr1(&[0, 0]);

    let params = Parameters::new(
        tau,
        T_au,
        trotter,
        dim,
        num_blocks,
        num_iters,
    );

    let mut pair_potentials = HashMap::<(usize, usize), PairPotential>::new();
    pair_potentials.insert((0,0), PairPotential::Morse{de: 0.1745, re: 1.40, a: 1.0282});

    let walker = Array2::random((particles.len(), dim), Uniform::new(-1.0, 1.0 as f64));

    // Start all time slices from same position:
    let mut walkers = nd::Array3::zeros((trotter, particles.len(), dim));
    walkers.axis_iter_mut(Axis(0)).map(|mut slice| slice.assign(&walker)).for_each(drop);
    assert!(walkers.iter().all(|x| *x != 0.0));

    let state = State{
        walkers: walkers,
        species: species,
        particles: particles,
        pair_potentials: pair_potentials
    };

    state.invariant();

    return (params, state)
}


/// Electronic H2 system (p + p + e + e)
pub fn electronic_H2(T: f64, trotter: usize, num_iters: usize) -> (Parameters, State) {
    let au_in_kelvin = 3.157746e5;
    let T_au = T / au_in_kelvin;
    let tau = 1.0 / (T_au * trotter as f64);
    let dim = 3;
    let num_blocks = 100;

    let p_s0 = ParticleSpecies{
        symbol: "p".to_string(),  // proton
        mass: 1842.0,
        spin: Spin::Down,
        charge: 1.0,
    };
    let e_s0 = ParticleSpecies{
        symbol: "e".to_string(),  // electron
        mass: 1.0,
        spin: Spin::Down,
        charge: -1.0,
    };

    let mut p_s1 = p_s0.clone();
    p_s1.spin = Spin::Up;
    let mut e_s1 = e_s0.clone();
    e_s1.spin = Spin::Up;
    let species = nd::arr1(&[p_s0, p_s1, e_s0, e_s1]);
    let particles = nd::arr1(&[0, 1, 2, 3]);

    let params = Parameters {
            tau,
            temperature: T_au,
            sys_dim: dim,
            trotter,
            num_blocks,
            num_iters,
            burnout_blocks: num_blocks/3,
            obs_interval: 10,
            pair_correlation_gridsize: 128,
            pair_correlation_max_range: 9.0,
            periodic_box: None,
            bisection: false,
            step_size: 0.1,
        };

    let mut pair_potentials = HashMap::<(usize, usize), PairPotential>::new();
    // Pair (N-N) is not needed to specify as there is no two same particle
    //pair_potentials.insert((0,0), PairPotential::None);
    pair_potentials.insert((0,1), PairPotential::Coulomb);
    pair_potentials.insert((0,2), PairPotential::Coulomb);
    pair_potentials.insert((0,3), PairPotential::Coulomb);
    //pair_potentials.insert((1,1), PairPotential::None);
    pair_potentials.insert((1,2), PairPotential::Coulomb);
    pair_potentials.insert((1,3), PairPotential::Coulomb);
    //pair_potentials.insert((2,2), PairPotential::None);
    pair_potentials.insert((2,3), PairPotential::Coulomb);
    //pair_potentials.insert((3,3), PairPotential::None);

    let walker = Array2::random((particles.len(), dim), Uniform::new(-1.0, 1.0 as f64));

    // Start all time slices from same position:
    let mut walkers = nd::Array3::zeros((trotter, particles.len(), dim));
    walkers.axis_iter_mut(Axis(0)).map(|mut slice| slice.assign(&walker)).for_each(drop);

    assert!(walkers.iter().all(|x| *x != 0.0));

    let state = State{
        walkers: walkers,
        species: species,
        particles: particles,
        pair_potentials: pair_potentials
    };

    state.invariant();

    return (params, state);
}

/// Atomic H2 system (H + H)
pub fn atomic_H8(T: f64, trotter: usize, num_iters: usize) -> (Parameters, State) {
    let au_in_kelvin = 3.157746e5;
    let T_au = T / au_in_kelvin;
    let tau = 1.0 / (T_au * trotter as f64);
    let dim = 3;
    let num_blocks = 100;

    let H_down = ParticleSpecies{
        symbol: "H".to_string(),  // proton
        mass: 1843.0,
        spin: Spin::Down,
        charge: 0.0,
    };

    let mut H_up = H_down.clone();
    H_up.spin = Spin::Up;

    let species = nd::arr1(&[H_down, H_up]);
    let particles = nd::arr1(&[0, 0, 0, 0, 1, 1, 1, 1]);

    let params = Parameters::new(
        tau,
        T_au,
        trotter,
        dim,
        num_blocks,
        num_iters,
    );

    let mut pair_potentials = HashMap::<(usize, usize), PairPotential>::new();
    pair_potentials.insert((0,0), PairPotential::Morse{de: 0.1745, re: 1.40, a: 1.0282});
    pair_potentials.insert((0,1), PairPotential::Morse{de: 0.1745, re: 1.40, a: 1.0282});
    pair_potentials.insert((1,1), PairPotential::Morse{de: 0.1745, re: 1.40, a: 1.0282});

    let walker = Array2::random((particles.len(), dim), Uniform::new(-1.0, 1.0 as f64));

    // Start all time slices from same position:
    let mut walkers = nd::Array3::zeros((trotter, particles.len(), dim));
    walkers.axis_iter_mut(Axis(0)).map(|mut slice| slice.assign(&walker)).for_each(drop);
    assert!(walkers.iter().all(|x| *x != 0.0));

    let state = State{
        walkers: walkers,
        species: species,
        particles: particles,
        pair_potentials: pair_potentials
    };

    state.invariant();

    return (params, state)
}