use criterion::{Criterion, black_box, criterion_group, criterion_main, Benchmark};

use pimc_rust::init_systems;
use pimc_rust::pimc::start;

#[allow(non_snake_case)]
fn atomic_H8(){
    #[allow(non_snake_case)]
    let T = 300.0;
    let trotter = 1;
    let num_iters = 100;

    #[allow(non_snake_case)]
    let (params3, H8_atomic_state) = init_systems::atomic_H8(black_box(T), trotter, num_iters);

    println!("\nRunning atomic H8");
    let calc_3 = start(params3, H8_atomic_state);
    println!("---\n");

    println!("Energy atomic H8: {}({})", calc_3.obs.E_tot_mean, calc_3.obs.E_tot_err);

    //calc_3.obs.E_tot_mean;
}


#[allow(non_snake_case)]
fn bench_atomic_H8(c: &mut Criterion) {
    c.bench(
        "atomic H8 custom",
        Benchmark::new(
            "atomic H8",
            |b| b.iter(|| atomic_H8())
        ).sample_size(3));  // .nresamples(1)
}

criterion_group!(benches, bench_atomic_H8);
criterion_main!(benches);