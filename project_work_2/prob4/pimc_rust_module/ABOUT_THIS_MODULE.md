# Meta information of using this Python module written in Rust

Alpi Tolvanen, 2019

Licence: MIT (As long as you remove GPL-dependecies like 'rgsl')

## Brief
This file does not contains information about pimc-code itself, but more about the aspects of rust python-module.
The code is based on 'word-count' example template found in 'pyo3'-library.

## Installation
Install the module as pip3 package by running

```
cd pimc_rust_module
pip3 install .
# Compile in debug mode: 
# pip3 install -e .
```

## Uninstall
To uninstall, run:

```
pip3 uninstall pimc-rust
```

## Calling from python side
After installation this module should be installed to your python packages. (If you are using anaconda, remember that pip-packages are not installed there.)
```python
import pimc_rust

# Assuming addf is implemented
print("1 + 1 = ", pimc_rust.addf(1.0, 1.0))
```

## Using this module as a template and modifying it to new custom projects
Setting up rust module is not the most straight-forward task, so this module can be used as a template for new projects.

To modify this to your own module, copy paste "pimc_rust_module", and remove all unneeded source files (e.g. other than lib.rs). Replace everywhere "pimc_rust" with your own module name. 

Find and replace all "pimc_rust" occurences in files. Also rename "pimc_rust" directory. You can do this with following commands:

```
cd pimc_rust_module
# replace file occurences
find ./ -type f -exec sed -i -e 's/pimc_rust/my_new_module_name/g' {} \;
# rename directory
mv pimc_rust my_new_module_name
```
(Note: Using those commands, also this help-file's "pimc_rust" occurences will be replaced.) (Also, obsiously rememer to change import statement in your python file that calls this module.)

        
If you want to do the "pimc_rust" -> "my_module" replacement by hand, all occurences can be also searched with:

```
grep -rnw -B 1 -A 1 "pimc_rust"
```

These should be the occurences:

* pyrs_module/__init_.py  (1 occurence)
* src/lib.rs              (1 occurence)
* Cargo.toml              (2 occurences)
* setup.py                (4 occurences)

