
use pimc_rust::init_systems;
use pimc_rust::pimc::start;

#[allow(non_snake_case)]
//#[test]
fn test_atomic_and_electronic_H2() {
    let T = 300.0;
    let trotter = 8;
    let num_iters = 100;

    let (params, H2_atomic_state) = init_systems::atomic_H2(T, trotter, num_iters);
    let (params2, H2_electronic_state) = init_systems::electronic_H2(T, trotter, num_iters);



    println!("\nRunning atomic H2");
    let calc_1 = start(params, H2_atomic_state);
    println!("---\n");

    println!("\nRunning electronic H2");
    let calc_2 = start(params2, H2_electronic_state);
    println!("---\n");



    println!("Energy atomic H2: {}({})", calc_1.obs.E_tot_mean, calc_1.obs.E_tot_err);
    println!("Energy electronic H2: {}({})", calc_2.obs.E_tot_mean, calc_2.obs.E_tot_err);

    // Useless assertion to hopefully force this code to be run.
    assert!(calc_2.obs.E_tot_mean != 0.0);
}

