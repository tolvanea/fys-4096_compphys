"""
This file implements interactive walker history plot with sliders. However matplotlib is quite
buggy and therefore the sliders may stop working quite quickly, (if they work at all).

The main functions are walkers_interactive_trotters() and walkers_interactive_temps()
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider


def plot_walker_positions(ax, calc, block):
    walkers = calc.obs.walkers_history[block]
    rings = np.empty((walkers.shape[0]+1, *walkers.shape[1:]))
    rings[:-1,...] = walkers
    rings[-1,...] = walkers[0,...]

    plots_by_particle = []
    for particle_id in range(rings.shape[1]):
        species = calc.state.species[calc.state.particles[particle_id]]
        p, = ax.plot(rings[:,particle_id,0], rings[:,particle_id,1],
                    color="C{}".format(particle_id % 9), marker="o",
                    label="{} spin {}".format(species.symbol, species.spin))
        plots_by_particle.append(p)

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.legend()

    return plots_by_particle

def update_particle_positions(plots_by_particle, calc, block):
    walkers = calc.obs.walkers_history[block]
    rings = np.empty((walkers.shape[0]+1, *walkers.shape[1:]))
    rings[:-1,...] = walkers
    rings[-1,...] = walkers[0,...]

    assert(len(plots_by_particle) == rings.shape[1])

    for particle_id in range(rings.shape[1]):
        p = plots_by_particle[particle_id]
        p.set_xdata(rings[:,particle_id,0])
        p.set_ydata(rings[:,particle_id,1])


def walkers_interactive_trotters(calcs_by_trotter, num):
    """
    Interactive slider of walkers
    https://matplotlib.org/users/screenshots.html#slider-demo
    """
    fig, ax = plt.subplots(num=num)
    plt.subplots_adjust(left=0.25, bottom=0.25)

    trotters = [c.params.trotter for c in calcs_by_trotter]

    axcolor = 'lightgoldenrodyellow'
    M_min = trotters[0]
    M_max = trotters[-1]
    block_max = calcs_by_trotter[-1].params.num_blocks

    plots = plot_walker_positions(ax, calcs_by_trotter[-1], block_max-1)
    ax.set_title("Walker configuration, trotter = {}".format(calcs_by_trotter[-1].params.trotter)
                 + "\n(Note: first choose time frame with sliders,\n"
                 + "and then zoom, because sliders get stuck easily)")
    ax.axis([-2, 2, -2, 2])

    trotter_ax = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
    block_ax = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)


    trotter_slider = Slider(trotter_ax, 'Trotter', M_min, M_max, valinit=M_max)
    block_slider = Slider(block_ax, 'Block', 0, block_max, valinit=block_max)


    def update(val):
        closest_trotter = min(trotters, key=lambda x:abs(x-int(trotter_slider.val)))
        trotter_id = trotters.index(closest_trotter)
        block = int(block_slider.val)
        calc = calcs_by_trotter[trotter_id]
        print("trotter:", trotter_id, ", block:", block)
        update_particle_positions(plots, calc, block-1)
        ax.set_title("Walker configuration, trotter = {}".format(closest_trotter)
                     + "\n(Note: first choose time frame with sliders,\n"
                     + "and then zoom, because sliders get stuck easily)")
        fig.canvas.draw_idle()
    trotter_slider.on_changed(update)
    block_slider.on_changed(update)


def walkers_interactive_temps(calcs_by_temp, num):
    """
    Interactive slider of walkers
    https://matplotlib.org/users/screenshots.html#slider-demo
    """
    fig, ax = plt.subplots(num=num)
    plt.subplots_adjust(left=0.25, bottom=0.25)

    temps = [c.params.get_T_in_kelvin() for c in calcs_by_temp]

    axcolor = 'lightgoldenrodyellow'
    T_min = temps[0]
    T_max = temps[-1]
    block_max = calcs_by_temp[-1].params.num_blocks

    plots = plot_walker_positions(ax, calcs_by_temp[-1], block_max - 1)
    ax.axis([-4, 4, -4, 4])
    ax.set_title("Walker configuration, $T = {}\\;K$".format(calcs_by_temp[-1].params.get_T_in_kelvin())
                 + "\n(Note: first choose time frame with sliders,\n"
                 + "and then zoom, because sliders get stuck easily)")

    temp_ax = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
    block_ax = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)


    temp_slider = Slider(temp_ax, 'Temperature', T_min, T_max, valinit=(T_max+T_min)/2)
    block_slider = Slider(block_ax, 'Block', 0, block_max, valinit=block_max)


    def update(val):
        closest_temp = min(temps, key=lambda x:abs(x-int(temp_slider.val)))
        temp_id = temps.index(closest_temp)
        block = int(block_slider.val)
        calc = calcs_by_temp[temp_id]
        print("Temperature:", temp_id, ", block:", block)
        update_particle_positions(plots, calc, block-1)
        ax.set_title("Walker configuration, $T = {}\\;K$".format(closest_temp)
                     + "\n(Note: first choose time frame with sliders,\n"
                     + "and then zoom, because sliders get stuck easily)")
        fig.canvas.draw_idle()
    temp_slider.on_changed(update)
    block_slider.on_changed(update)

