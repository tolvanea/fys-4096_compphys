"""
This file plots system of 8 hydrogen atoms using different temperatures and trotter numbers.
"""


import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

import pimc_python
try:
    import pimc_rust
except ImportError:
    print(
        "Rust module not installed, install it by instructions found in 'pimc_rust_module/README.md'."
        + "\nUse python implementation to run without installations and compilations."
    )
import walker_slider
import init_systems


def calculate_variating_trotter_and_temperature_using_python(params, state):
    trotters = np.array([1, 8])
    temps = np.array([300, 600, 1000, 2000, 4000]) / params.au_in_kelvin

    calcs_trotter_temp = np.full((len(trotters),len(temps)), fill_value=None, dtype=np.object_)

    # Calculate with many tau:s
    for i, trotter in enumerate(trotters):
        for j, T in enumerate(temps):
            tau = 1 / (T * trotter)
            params_copy = deepcopy(params)
            params_copy.trotter = trotter
            params_copy.tau = tau
            params_copy.temperature = T

            state_copy = deepcopy(state)
            walker = state_copy.walkers[0,:]
            state_copy.walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)

            calculation = pimc_python.start(params_copy, state_copy)
            calcs_trotter_temp[i,j] = calculation

    return calcs_trotter_temp

def calculate_variating_trotter_and_temperature_using_rust(params, state):
    trotters = np.array([1, 8])
    temps = np.array([300, 600, 1000, 2000, 4000]) / params.au_in_kelvin

    calcs_trotter_temp = np.full((len(trotters),len(temps)), fill_value=None, dtype=np.object_)

    for i, trotter in enumerate(trotters):
        # Initial settings for calculations: (parameters and initial state)
        initsetts = []
        for j, T in enumerate(temps):
            tau = 1 / (T * trotter)
            params_copy = deepcopy(params)
            params_copy.trotter = trotter
            params_copy.tau = tau
            params_copy.temperature = T

            state_copy = deepcopy(state)
            walker = state_copy.walkers[0,:]
            state_copy.walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)

            # Initial setting for calculation
            initsetts.append((params_copy, state_copy))

        calcs = pimc_rust.start_calculation_set(initsetts)
        calcs_trotter_temp[i,:] = calcs

    return calcs_trotter_temp

def plot_energetics_by_block(calcs_trotter_temp):
    """
    Plot energy by block count
    """
    fig, axes = plt.subplots(2, 3, figsize=(15, 10), num="Energetics by block")
    # ((ax1, ax2, ax3), (ax4, ax5, ax6))

    for trotter_id in range(2):
        for ax_id, temp_id in enumerate([0, 2, 4]):

            obs = calcs_trotter_temp[trotter_id, temp_id].obs
            params = calcs_trotter_temp[trotter_id, temp_id].params
            print("Trotter: {}, temperature {}".format(params.trotter, params.get_T_in_kelvin()))

            ax = axes[trotter_id][ax_id]
            plot = (
                ("Energy", "$E_{tot}$", obs.E_tot, 0),
                ("Energy", "$E_{kin}$", obs.E_kin, 1),
                ("Energy", "$E_{pot}$", obs.E_pot, 2),
                )

            for title, label, data, id in plot:
                b = int(params.burnout_ratio * params.num_blocks)  # burnout
                ax.plot(np.arange(b+1), data[:b+1], ls=":", c="C{}".format(id))
                dat = data[b:]
                print()
                ax.plot(np.arange(b, params.num_blocks), dat, c="C{}".format(id), label=label)
                ax.set_xlabel("block")
                ax.set_ylabel("${}$".format(title))

                txt = "Mean: {} : {:.3f} ± {:0.3f}"\
                    .format(label, np.mean(dat), np.std(dat) / np.sqrt(len(dat)))
                print("       ", txt)
                ax.text(0.05, 0.5 - id * 0.07, txt, transform=ax.transAxes)
                ax.set_title("{} by block, $M = {}, \\ T = {}\\;K$"
                             .format(title, params.trotter, int(params.get_T_in_kelvin())))
                ax.legend()


def plot_energetics_by_temperature(calcs_trotter_temp):

    fig, axes = plt.subplots(1, 2, figsize=(10, 5), num="Energetics by temperature")

    for trotter_id in range(2):
        ax = axes[trotter_id]
        temps = [c.params.get_T_in_kelvin() for c in calcs_trotter_temp[trotter_id]]
        means = [None for i in range(len(temps))]
        errors = [None for i in range(len(temps))]

        energy_components = (
            ("$E_{tot}$", lambda obs: obs.E_tot),
            ("$E_{kin}$", lambda obs: obs.E_kin),
            ("$E_{pot}$", lambda obs: obs.E_pot),
        )
        for id, (label, get_component) in enumerate(energy_components):
            for temp_id in range(len(temps)):
                obs = calcs_trotter_temp[trotter_id, temp_id].obs
                data = get_component(obs)
                params = calcs_trotter_temp[trotter_id, temp_id].params

                b = int(params.burnout_ratio * params.num_blocks)  # burnout
                means[temp_id] = data[b:].mean()
                errors[temp_id] = data[b:].std() / np.sqrt(len(data[b:]))

            ax.errorbar(temps, means, errors, c="C{}".format(id), label=label, capsize=5)
            ax.set_xlabel("Temperature")
            ax.set_ylabel("Energy")
            ax.set_title("Energy by $T$, $M = {}$"
                         .format(calcs_trotter_temp[trotter_id,0].params.trotter))
            #ax.set_xscale("log")
            #ax.set_yscale("log")
            ax.legend()


def main():
    print("Simulation takes more than half an hour")
    params, state = init_systems.create_atomic_8H(num_iters=100)

    # Choose Rust or Python backend for pimc
    if False:
        # Use rust
        calcs_trotter_temp = calculate_variating_trotter_and_temperature_using_rust(params, state)
    else:
        # Use python (Does not need separate compilation, but it is (50 times?) slower)
        calcs_trotter_temp = calculate_variating_trotter_and_temperature_using_python(params, state)

    plot_energetics_by_block(calcs_trotter_temp)
    plot_energetics_by_temperature(calcs_trotter_temp)

    walker_slider.walkers_interactive_temps(calcs_trotter_temp[-1], num="Walkers slider")

    plt.show()


if __name__=="__main__":
    main()
