# Simple path integram Monte Carlo
Alpi Tolvanen, 2019


Licence: 

* python code: MIT 
* rust code: GPLv3+ (or MIT without the GPL-dependecy 'rgsl')

These PIMC codes are based on one made by Ilkka Kylänpää.

## Brief
There are two implementations of PIMC, Python version, and Rust a module that is called from python.
Functionality in both is pretty much identical, however, compiled rust code *should be* faster (it is also multithreaded). (See performance section below.)

The pimc code is NOT CORRECT as far as I know. Results seems to be off for some reason. But the main point is not the scientific correctness of program, but rather the learning process of program logic and python-pimc FFI.


## Main files
Main workflow is following:

* Create template for calculation with init_systems.py
    * Datastructures are used from 'data_structures.py/rs'
* Modify temperature and trotter number in main.py by cloning and dublicating the template
* Give new system for 
    * Python: pimc_python.start() 
    * Rust: pimc::start_calculation_set()
* Calculation loops metropolis algorithm, and calls methods in 'calculation_misc.py/rs'
* Return reasults to main.py, which does the figure plotting.

| Python | Rust counterpart | Description |
| ------ | ---------------- | ----------- |
| 'main.py' | *none* or 'tests/integration_test.rs' | Starts calculation|
| 'init_systems.py' | 'src/init_systems.rs' | Creates the systems with parametes |
| 'pimc_python.py' | 'src/pimc/mod.rs' | The metropolis algorithm itself |
| 'calculation_misc.py' | 'src/pimc/calculation_misc.rs' | Functions called from metropolis |
| 'data_structures.py' | 'src/pimc/data_structures.rs' | e.g. 'Particle', 'Parametes', 'Observables'... |


## Installation
For rust module installation, see 'pimc_rust_module/README.md'

## Run
Run program with python

```
cd prob4
python3 main.py
```

## Performance
On my machine the running times of this this program (main.py) is the following: 

* Rust (1 thread): 3min 55s. 
* Rust (4 threads): 10min 57s. 
* Python (1 thread): 6min 41s

So it seem that single threaded optimized rust is **slower** than pure python. However, [previously](https://gitlab.com/tolvanea/MetropolisMC) I have measured Rust to be about 20-100 times faster than Python.  

I don't know why this is, I should profile the code. Because learning and implementing this rust FFI was time consuming enough (about two weeks of daily programming), I'll leave this question unanswered for now.

*Edit:* It turns out that if rust code is ran directly without python interface, it is ~50-100 faster than python code. Somehow this ```pip3 install .``` compiles it very badly or something. Or I compare different systems somewhy.

