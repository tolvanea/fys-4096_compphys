"""
This file contains constructors for different systems.
"""

import numpy as np
from copy import deepcopy
import data_structures


def create_atomic_H2(T=300.0, trotter=1, num_iters=1000):
    """
    Atomic H2 system (H + H)
    """
    au_in_kelvin = 3.157746e5
    T_au = (T/au_in_kelvin)
    tau = 1 / (T_au * trotter)
    dim = 3

    H_spin0 = data_structures.ParticleSpecies(symbol="H",
                                              mass=1842+1,  # Mass of hydrogen: proton+electron
                                              spin=0, charge=0.0)
    H_spin1 = deepcopy(H_spin0)
    H_spin1.spin = 1
    species = [H_spin0, H_spin1]
    particles = np.array([0, 1])

    # For 2D quantum dot
    params = data_structures.Parameters(tau=tau,
                                        temperature=T_au,
                                        trotter=trotter,
                                        sys_dim=dim,
                                        num_blocks=100,
                                        num_iters=num_iters,
                                        burnout_ratio=0.4,  # 40% of first samples rejected
                                        )
    pair_potentials = {
        # (0,0): ("None"), # [H_spin0 - H_spin0] pair not present, can be whatever
        (0,1): ("Morse", 0.1745, 1.40, 1.0282), # (De,re,a)
        # (1,1): ("None"),
    }

    # On one time slice:
    walker = np.array(np.random.rand(len(particles), dim)*2 - 1)
    walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)
    state = data_structures.State(walkers=walkers,
                                  species=species,
                                  particles=particles,
                                  pair_potentials=pair_potentials)
    return params, state

def create_electronic_H2(T=300.0, trotter=1, num_iters=1000):
    """
    Electronic H2 system (p + p + e + e)
    """
    au_in_kelvin = 3.157746e5
    T_au = (T/au_in_kelvin)
    tau = 1 / (T_au * trotter)
    dim = 3

    p_s0 = data_structures.ParticleSpecies(symbol="p",  # proton
                                           mass=1842,
                                           spin=0,
                                           charge=1.0)
    e_s0 = data_structures.ParticleSpecies(symbol="e",  # electron
                                           mass=1,
                                           spin=0,
                                           charge=-1.0)

    p_s1 = deepcopy(p_s0)
    p_s1.spin = 1
    e_s1 = deepcopy(e_s0)
    e_s1.spin = 1
    species = [p_s0, p_s1, e_s0, e_s1]
    particles = np.array([0, 1, 2, 3])

    # For 2D quantum dot
    params = data_structures.Parameters(tau=tau,
                                        temperature=T_au,
                                        trotter=trotter,
                                        sys_dim=dim,
                                        num_blocks=100,
                                        num_iters=num_iters,
                                        burnout_ratio=0.4,  # 40% of first samples rejected
                                        )
    pair_potentials = {
        # (0,0): ("None", ),        # p_s0 - p_s0 (No two p_s0 particles present)
        (0,1): ("Coulombic", ),     # p_s0 - p_s1
        (0,2): ("Coulombic", ),     # ...
        (0,3): ("Coulombic", ),
        # (1,1): ("None", ),        # p_s1 - p_s1 pair not present as there is only one p_s1
        (1,2): ("Coulombic", ),
        (1,3): ("Coulombic", ),
        # (2,2): ("None", ),
        (2,3): ("Coulombic", ),
        # (3,3): ("None", ),
    }

    # On one time slice:
    walker = np.array(np.random.rand(len(particles), dim)*2 - 1)
    walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)
    state = data_structures.State(walkers=walkers,
                                  species=species,
                                  particles=particles,
                                  pair_potentials=pair_potentials)
    return params, state

def create_atomic_8H(T=300.0, trotter=1, num_iters=1000):
    """
    Atomic system with 8 H-atoms
    """
    au_in_kelvin = 3.157746e5
    T_au = (T/au_in_kelvin)
    tau = 1 / (T_au * trotter)
    dim = 3

    H_spin0 = data_structures.ParticleSpecies(symbol="H",
                                          mass=1842+1,  # Mass of hydrogen: proton+electron
                                          spin=0, charge=0.0)
    H_spin1 = deepcopy(H_spin0)
    H_spin1.spin = 1
    species = [H_spin0, H_spin1]
    particles = np.array([0, 0, 0, 0, 1, 1, 1, 1])

    # For 2D quantum dot
    params = data_structures.Parameters(tau=tau,
                                        temperature=T_au,
                                        trotter=trotter,
                                        sys_dim=dim,
                                        num_blocks=100,
                                        num_iters=num_iters,
                                        burnout_ratio=0.4,  # 40% of first samples rejected
                                        periodic_box=np.array((50.0, 50.0, 50.0))
                                        )
    pair_potentials = {(0,0): ("Morse", 0.1745, 1.40, 1.0282),  # (De,re,a)
                       (0,1): ("Morse", 0.1745, 1.40, 1.0282),  # (De,re,a)
                       (1,1): ("Morse", 0.1745, 1.40, 1.0282)}  # (De,re,a)

    # On one time slice:
    walker = np.array(np.random.rand(len(particles), dim)*2 - 1) * 0.1
    walkers = np.repeat(walker[np.newaxis,:], trotter, axis=0)
    state = data_structures.State(walkers=walkers,
                                  species=species,
                                  particles=particles,
                                  pair_potentials=pair_potentials)
    return params, state