import pimc_rust
import data_structures

print("Ok: 4==", pimc_rust.count_line("as df as fds as s as wer", "as"), "?")
print("1 + 1 = ", pimc_rust.addf(1.0, 1.0))

spec_rs = pimc_rust.ParticleSpecies(
    symbol="H",
    mass=1842.0 + 1.0,  # Mass of hydrogen: proton+electron
    spin=0,
    charge=0.0)
print("spec", spec_rs.symbol, spec_rs.mass)
print("spec.__dict__", spec_rs.__dict__,
      "(This is wrong, but it does not matter.)")
print("Two times mass, rust", pimc_rust.double_mass1(spec_rs))

spec_py = data_structures.ParticleSpecies(symbol="H",
                                      mass=1842+1,  # Mass of hydrogen: proton+electron
                                      spin=0, charge=0.0)
print("Two times mass, python", pimc_rust.double_mass1(spec_py))
