import numpy as np
import typing as tp
from scipy.special import erf

def ord(a,b):
    """
    Used in calling pair dict, as particle-particle key is defined so that smaller index
    is first.
    """
    if a < b:
        return a, b
    else:
        return b, a


def diff(r1, r2, box):
    """
    periodic difference of two points
    :return:
    """
    diff = r2 - r1
    half_box = box/2
    for dim in range(len(diff)):
        if diff[dim] < -half_box[dim]:
            diff[dim] += box[dim]
        elif diff[dim] >= half_box[dim]:
            diff[dim] -= box[dim]
    return diff


def distance(r1, r2, box):
    """
    Calculate distance of two points with possibility of periodic boundaries.
    """
    if box is None:
        return np.linalg.norm(r1 - r2)
    else:
        d = diff(r1, r2, box)
        return np.linalg.norm(d)


def keep_in_box(r1, box):
    """
    If system is periodic, then move point in it. Assuming point is not more than box length away.
    """
    if box is None:
        return r1
    else:
        half_box = box/2.0
        return np.mod(r1 + half_box, box) - half_box


def mid_point(r1, r2, box):
    """
    Middle point of two points, with the possibility of periodic boundaries
    """
    if box is None:
        return (r2 + r1) / 2.0
    else:
        d = diff(r1, r2, box)
        return keep_in_box(r1 + d/2, box)


def potential_action(state, time_slice1, time_slice2, params):
    return 0.5*params.tau*(potential(state, time_slice1, params)
                           + potential(state, time_slice2, params))

def kinetic_action(r1, r2, params, lambda1):
    return distance(r1, r2, params.periodic_box)**2 / (lambda1 * params.tau * 4)


def morse_potential(r, De, re, a):
    # Morse potential from ex11 slides, eq 14
    u = De * (1 - np.exp(-a * (r-re)))**2 - De
    return u


def energy(state, params):
    """
    Thermal energy. Equation 2.36 in thesis of Kylänpää
    """
    M = params.trotter
    d = params.sys_dim
    tau = params.tau
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(state, i, params)
        for j in range(len(state.particles)):
            m = state.species[state.particles[j]].mass
            lambda1 = 1 / (2*m)  # h^2 / (2m)
            K += d / (2 * tau) \
                 - distance(state.walkers[i,j], state.walkers[(i+1)%M,j],
                            params.periodic_box)**2 \
                 / (4 * lambda1 * tau ** 2)

    return K/M, U/M


def potential(state, time_slice_id, params):
    """
    Diagonal potential energy of particles.
    """
    V = 0.0
    walker = state.walkers[time_slice_id]
    for i in range(walker.shape[0] - 1):
        for j in range(i+1, walker.shape[0]):
            sp1_id = state.particles[i]  # species id, tells type of particle e.g. "H"
            sp2_id = state.particles[j]
            pot = state.pair_potentials[ord(sp1_id, sp2_id)]
            r = distance(walker[i], walker[j], params.periodic_box)
            if pot[0] == "Morse":
                V += morse_potential(r, pot[1], pot[2], pot[3])
            elif pot[0] == "Coulombic":
                charge1 = state.species[sp1_id].charge
                charge2 = state.species[sp2_id].charge
                σ = 0.1  # some random choice for smoothing parameter
                V += charge1 * charge2 * erf(r / np.sqrt(2 * σ)) / r
            else:
                # Other potentials can be added here, like coulombic 1/r
                assert(False)
    return V