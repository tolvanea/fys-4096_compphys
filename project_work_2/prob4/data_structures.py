import numpy as np
import typing as tp
from copy import deepcopy

from calculation_misc import energy, ord, distance


class ParticleSpecies:
    """ Defines distinguishable particle type
    """
    def __init__(self, symbol, mass, spin, charge):
        self.symbol = symbol
        self.mass = mass
        self.spin = spin
        self.charge = charge


class Parameters:
    """
    Parameters of a system. (Particle configuration is defined by 'State'.)
    """
    def __init__(self,
                 tau: float,  # Time step
                 temperature: float,  # Temperature
                 sys_dim: int,  # system dimension
                 trotter: int,  # Trotter number
                 num_blocks: int,  # Number of blocks, i.e. averaged observable count
                 num_iters: int,  # Number of random walker moves within block
                 burnout_ratio: float,  # How big portion of simulation is discarded (between 0.0-1.0)
                 obs_interval: int = 5,  # How many random moves will be taken between measurements
                 pair_correlation_gridsize: int = 128,  # Pair correlation vector resolution
                 pair_correlation_max_range: float = 9.0,  # Pair correlation range (min is 0.0)
                 # Perioidic box
                 periodic_box: tp.Union[None, tp.Tuple[float, float, float]] = None,
                 bisection: bool = True,  # Use bisection which speeds up process
                 step_size: float = 1.0  # Gaussian move scale IF 'bisection' is set to False'
                 ):
        self.tau = tau
        self.temperature = temperature
        self.sys_dim = sys_dim
        self.trotter = trotter
        self.num_blocks = num_blocks
        self.num_iters = num_iters
        # TODO burnout ratio is stupid idea, replace it with burnout_blocks
        self.burnout_ratio = burnout_ratio
        self.obs_interval = obs_interval
        self.pair_correlation_gridsize = pair_correlation_gridsize
        self.pair_correlation_max_range = pair_correlation_max_range
        self.periodic_box = periodic_box  # None means no box
        self.au_in_kelvin = 3.157746e5  # https://en.wikipedia.org/wiki/Atomic_units

        # Sometimes it may be so that acceptance with bisection is very low. Then it may be better
        # to calculate kinetic action with custom step that is smaller than fixed bisection step.
        self.bisection = bisection
        self.step_size = step_size  # Multiplies standard deviation of Gaussian step.
                                    # 'step_size' is used only when bisection is False.
                                    # Bisection uses step size 1.0.

    def convert_T_to_au(self, T):
        return T / self.au_in_kelvin

    def get_T_in_kelvin(self):
        return self.temperature * self.au_in_kelvin


class State:
    """
    Particle configuration: Locations, and species types of each particle.
    """
    def __init__(self, walkers: np.ndarray, species: np.ndarray,
                 particles: np.ndarray, pair_potentials:tp.Dict):
        """
        :param walkers:         3d array, same row - same time slice,
                                          same column - same particle
                                          same depth - same x/y/z coordinate
        :param species:         1d array, all different species in system
                                          e.g. [H_spin0, H_spin1, He_spin0]
        :param particles:       1d array, 'species'-indices of all the particles
                                          e.g. [1, 0, 2, 1]. In this case above particle
                                          'walkers[:,1,:]' would correspond species "H_spin0".
                                          Note integer must be (dtype=np.int64) which is default
        :param pair_potentials: Dict, Potential-types corresponding each species pair
                                          key: (a,b) where a and b integers and a<=b.
                                          value: (name, *params) where name is "Morse" or
                                          "Coulombic". With "Morse", 3 parameters are given.
                                          Note! All pair must be specified even though they are not
                                          present.
        """
        self.walkers = walkers
        self.species = species
        self.particles = particles
        self.pair_potentials = pair_potentials


class Observables:
    """
    Observables, which will be calculated during simulation.
    """
    def __init__(self, params, particles):
        """

        :param params:      System parameters
        :param particles:   See State.particles
        """

        # Following observables are calculated for each block
        self.E_tot = np.zeros(params.num_blocks)
        self.E_kin = np.zeros(params.num_blocks)
        self.E_pot = np.zeros(params.num_blocks)
        self.acceptance = np.zeros(params.num_blocks)
        # Distances and correlation
        R, pair_correlation = self.create_empty_pair_distance_structures(
                                        particles,
                                        params.pair_correlation_gridsize,
                                        params.num_blocks)
        # Mean distance per species pair, Dict of 1d-arrays
        self.R = deepcopy(R)           # mean distance R
        self.R_squared = deepcopy(R)   # mean of R^2
        self.R_inverse = deepcopy(R)   # mean of 1/R
        # Pair correlation per species. Dict of 2d-arrays (Not used nor tested!)
        self.pair_correlation = pair_correlation
        # Walker positions by block 4d array (= 1d × 3d)
        self.walkers_history = np.zeros((params.num_blocks,
                                         params.trotter,
                                         len(particles),
                                         params.sys_dim))

    def create_empty_pair_distance_structures(self, particles,
                                              pair_correlation_gridsize,
                                              num_blocks):
        """
        Creates empty 'pair_correlation' data structure, which is dict of
        2d arrays. Dict has particle species pair as a key, and 2d array
        dimensions is [blocks ×  paircorrelation_gridsize].

        :return: Dict[(species1,species2), 1d-array],
                 Dict[(species1,species2), 2d-array]
        """
        pair_correlation = {}
        R = {}
        for i in range(len(particles)-1):
            for j in range(i+1, len(particles)):
                prt1_id = particles[i]
                prt2_id = particles[j]

                R[ord(prt1_id, prt2_id)] = np.zeros(num_blocks)

                pair_correlation[ord(prt1_id, prt2_id)] = np.zeros(
                        (num_blocks,pair_correlation_gridsize))

        return R, pair_correlation

    def sum_new_measurement(self, block_id, state, params):
        """
        Calculates a new measurement within a block. Results must be normalized with
        'normalize_to_mean()' after the block is completed.
        :param block_id:    Current block
        :param state:       See 'State'
        :param params:      See 'Parameters'
        :return:
        """
        E_kin, E_pot = energy(state, params)
        self.E_kin[block_id] += E_kin
        self.E_pot[block_id] += E_pot
        self.E_tot[block_id] += E_kin + E_pot
        self.add_pair_distance_counts(block_id, state, params)
        self.walkers_history[block_id] = state.walkers

    def add_pair_distance_counts(self, block_id, state, params):
        """
        Part of 'sum_new_measurement()'. Adds particle distance information to observables
        R R^2 1/R and pair_correlation.
        :param block_id:    Current block
        :param state:       See 'State'
        :param params:      See 'Parameters'
        :return:
        """
        # Iterate over all timesteps
        for timestep in range(state.walkers.shape[0]):
            for prt1_id in range(state.walkers.shape[1]-1):
                for prt2_id in range(prt1_id+1, state.walkers.shape[1]):
                    prt1_pos = state.walkers[timestep, prt1_id, :]
                    prt2_pos = state.walkers[timestep, prt2_id, :]
                    # species_id
                    prt1_sp = state.particles[prt1_id]
                    prt2_sp = state.particles[prt2_id]

                    r = distance(prt1_pos, prt2_pos, params.periodic_box)

                    self.R[ord(prt1_sp, prt2_sp)][block_id] += r
                    self.R_squared[ord(prt1_sp, prt2_sp)][block_id] += r**2
                    self.R_inverse[ord(prt1_sp, prt2_sp)][block_id] += 1/r

                    # Paircorrelation
                    coeff = params.pair_correlation_max_range - r
                    if 0.0 <= coeff and coeff < 1.0:
                        r_id = int( coeff * params.pair_correlation_gridsize)
                        self.pair_correlation[ord(prt1_sp, prt2_sp)][block_id,r_id] += 1

    def normalize_to_mean(self, block_id, params):
        """
        Calculates the means of sums of observables. Divides with the number of measurements.
        :param block_id:    Current block
        :param params:      See 'Parameters'
        :return:
        """
        obs_count = params.num_iters // params.obs_interval
        self.E_tot[block_id] /= obs_count
        self.E_kin[block_id] /= obs_count
        self.E_pot[block_id] /= obs_count
        for pair in self.pair_correlation:
            self.R[pair][block_id] /= (obs_count * params.trotter)
            self.R_squared[pair][block_id] /= (obs_count * params.trotter)
            self.R_inverse[pair][block_id] /= (obs_count * params.trotter)
            self.pair_correlation[pair][block_id] /= (obs_count * params.trotter)
        self.acceptance[block_id] /= params.num_iters
        print('Block {0}/{1}'.format(block_id + 1, params.num_blocks))
        print('    E   = {0:.5f}'.format(self.E_tot[block_id]))
        print('    Acc = {0:.5f}'.format(self.acceptance[block_id]))


class Calculation:
    """
    Specifies an ended simulation of system: Parameters, particles and measured observables.
    """
    def __init__(self, params, state, obs):
        """
        :param params:          See Parameters
        :param state:           See 'State' (type, location, spin) ...
        :param obs:             See 'Observables'
        """
        self.params = params
        self.state = state
        self.obs = obs