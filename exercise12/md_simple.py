"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf
    - Done
    - See 'prob1_plot.png' for energy over time plot
    - See 'prob1&3_derivations.pdf' for analytic derivations
- What are the default settings: update/integration algorithm, boundary
  conditions, and dimensions of the system?
    - Velocity-Verlet
    - No boundary conditions
    - dimension 3
- Output:
    "
    T 10.0
    E_kin 0.000877447599544762
    E_pot -0.17361663274447342
    E_tot -0.17273918514492867
    "


Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf
    - Done
    - See 'prob2_plot.png' for plots of temperature and distance
- Output:
    "
    Problem 2.
        Heat capacity Cv 0.0
        Temperature and its deviation: 92.558±65.662
    "

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf
    - See 'prob3_potentials.png' for potential plots
    - See 'prob1&3_derivations.pdf' for analytic derivations of LJ-parameters
    - Note:
        - The Morse potential has smaller curvature, I don't know why
- Output
    "
    Problem 3. Lennard-Jones potential
        T 10.0
        E_kin 0.21077907123787276
        E_pot -0.12598214145333025
        E_tot 0.08479692978454252
    "

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf
    - Done
    - See image prob4_plot.png
    - It seems that 10K used in first problem is the only temperature where
      temperature and other values behave weirdly.

"""

from numpy import *
from matplotlib.pyplot import *
import copy


class Atom:
    """ Classical atom """
    def __init__(self, index, mass, dt, dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self, LJ_epsilon, LJ_sigma):
        """ Lennard Jones potential """
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self, coordinates):
        self.R = coordinates

    def set_velocity(self, velocity):
        self.v = velocity

    def set_force(self, force):
        self.force = force


class Observables:
    """ Observables over the time step. """
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.E_tot = None
        self.distance = []
        self.Temperature = []
        self.E_squared = []


def calculate_energetics(atoms):
    """ E_kin and E_pot observables """
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    # ADD calculation of kineti and potential energy
    for atom in atoms:
        E_kin += 0.5 * atom.mass * atom.v @ atom.v

    for i in range(0, N - 1):
        for j in range(i + 1, N):
            V += pair_potential(atoms[i], atoms[j])

    return E_kin, V


def calculate_force(atoms, potential):
    # ADD comments, e.g., what are the elements of the return function F
    """ Calculate forces between atoms at the time moment.
     F[i,j]:    Force between atom indices i and j
     Fs:        One long vector containing all pair forces
     ij_map:    Indices of Fs corresponding pair i, j
     """
    #
    # 
    N = len(atoms)
    ij_map = zeros((N, N), dtype=int)
    Fs = []
    ind = 0
    for i in range(0, N - 1):
        for j in range(i + 1, N):
            Fs.append(pair_force(atoms[i], atoms[j], potential))
            ij_map[i, j] = ind
            ij_map[j, i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i, j]
            if i < j:
                f += Fs[ind]
            elif i > j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F


def pair_force(atom1, atom2, potential="Morse"):
    if potential == "Morse":
        return Morse_force(atom1, atom2)
    elif potential == "LJ":
        return lennard_jones_force(atom1, atom2)
    else:
        assert(False)  # unknown potential


def pair_potential(atom1, atom2, potential="Morse"):
    if potential == "Morse":
        return Morse_potential(atom1, atom2)
    elif potential == "LJ":
        return lennard_jones_potential(atom1, atom2)
    else:
        assert(False)  # unknown potential


def Morse_potential(atom1, atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = linalg.norm(atom2.R - atom1.R)
    # ADD comments and calculation of Morse potential
    # Morse potential from ex11 slides, eq 14
    u = De * (1 - exp(-a * (r-re)))**2 - De
    return u


def Morse_force(atom1, atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    # ADD comments and calculation of Morse force
    d = atom1.R - atom2.R
    r = linalg.norm(d)
    r_hat = d / r
    F = -a * 2 * De * exp(-a * (r - re)) * (1 - exp(-a*(r-re))) * r_hat
    return F


def lennard_jones_potential(atom1, atom2):
    ϵ = sqrt(atom1.LJ_epsilon * atom2.LJ_epsilon)
    σ = (atom1.LJ_sigma + atom2.LJ_sigma) / 2
    # ADD --> in problem 3: comments and calculation of LJ potential
    r = linalg.norm(atom1.R - atom2.R)
    return 4 * ϵ * ((σ/r)**12 - (σ/r)**6)


def lennard_jones_force(atom1, atom2):
    ϵ = sqrt(atom1.LJ_epsilon * atom2.LJ_epsilon)
    σ = (atom1.LJ_sigma + atom2.LJ_sigma) / 2
    # ADD --> in problem 3: comments and calculation of LJ force
    r = linalg.norm(atom1.R - atom2.R)
    return 4 * ϵ * (σ**12 *(-12) * r**-13 - σ**6 * (-6) * r**-7)


def velocity_verlet_update(atoms, potential):
    """
    Velocity verlet in equations 10-11
    """
    # ADD comments and integration algorithm according to function name
    dt = atoms[0].dt
    dt2 = dt ** 2
    for i in range(len(atoms)):
        a = atoms[i]
        a.R += dt * a.v + dt**2 * (1/(2*a.mass) * a.force)  # ADD position update
    Fnew = calculate_force(atoms, potential)
    for i in range(len(atoms)):
        a = atoms[i]
        a.v += dt * 1/(2*a.mass) * (Fnew[i] + a.force)  # ADD velocity update
        atoms[i].force = Fnew[i]  # update force
    return atoms


def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8, 0.0, 0.0]))
    atoms[1].set_position(array([0.7, 0.0, 0.0]))


def initialize_velocities(atoms, T=10.0):
    # ADD --> in problem 4: comment on what is v_max
    # diatomic case
    dims = atoms[0].dims
    kB = 3.16e-6  # in hartree/Kelvin
    for i in range(len(atoms)):
        # v_max in mean velocity in thermal equilibrium
        # equation: 1/2*m*v^2 = 3/2*kB*T
        v_max = sqrt(3.0 / atoms[i].mass * kB * T)
        atoms[i].set_velocity(array([1.0, 0.0, 0.0]) * v_max)
    # However I don't know why only second atom is set to other direction
    atoms[1].v = -1.0 * atoms[0].v


def initialize_force(atoms, potential):
    F = calculate_force(atoms, potential)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])


def Temperature(E_kin, dims, N):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    # ADD calculation of temperature in problem 2
    T = E_kin * 2 / (dims * N * kB)
    return T


def distance(atoms):
    assert len(atoms) == 2
    return linalg.norm(atoms[0].R - atoms[1].R)


def calculate_observables(atoms, observables):
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    observables.distance.append(distance(atoms))
    observables.Temperature.append(Temperature(E_k, atoms[0].dims, len(atoms)))
    # ADD calculation of observables in problem 2

    return observables


def problem_1_plot(E_tot, dt):
    x_range = dt*arange(len(E_tot))
    fig, ax1 = subplots(1,1, figsize=(5,5))
    ax1.plot(x_range, E_tot)
    ax1.set_xlabel("time")
    ax1.set_ylabel("Energy $E_h$")
    ax1.set_title("Problem 1 plot")
    fig.tight_layout()


def problem_2_heat_capacity(E_tot, T):
    kB = 3.16e-6
    return (mean(E_tot**2) - mean(E_tot)**2) / (kB * mean(T))


def problem_2_plot(observables, dt):
    x_range = dt*arange(len(observables.E_kin))
    fig, (ax1, ax2) = subplots(1,2, figsize=(10,5))
    ax1.plot(x_range, observables.Temperature)
    ax2.plot(x_range, observables.distance)
    ax1.set_xlabel("time")
    ax2.set_xlabel("time")
    ax1.set_ylabel("T")
    ax1.set_ylabel("d ($a_0$)")
    ax1.set_title("Temperature (K)")
    ax2.set_title("distance")
    fig.tight_layout()


def problem_3_plot_potentials():
    mass = 1860.0
    dt = 0.1
    dims = 3
    # Morse potential parameters
    De = 0.1745
    re = 1.40
    # Lennard Jones parameters, derived from Morse parameters
    # (See 'prob1&3_derivations.pdf')
    ϵ = De    # distance in units of Bohr radius
    σ = re * 2**(-1/6)
    # Values from last week does not work: ϵ, σ = 8.6, 0.281
    grid_size = 200
    assert(grid_size%4 == 0)

    atom1 = Atom(0, mass, dt, dims)
    atom1.set_LJ_parameters(ϵ, σ)
    atom2 = copy.deepcopy(atom1)
    potentials = (("Lennard Jones", lennard_jones_potential, lennard_jones_force),
                  ("Morse potential", Morse_potential, Morse_force))

    # More fine grid near equilibrium:
    x_range = np.hstack((linspace(0.5, 1.3, grid_size//4, endpoint=False),
                         linspace(1.3, 1.6, grid_size//2, endpoint=False),
                         linspace(1.6, 4.0, grid_size//4)))
    # 1.3 - 1.6

    fig, (ax1, ax2) = subplots(1,2, figsize=(10,5))

    lss = ["--", ":"]

    for i, (label, pot_func, force_func) in enumerate(potentials):
        u = np.zeros(grid_size)
        f = np.zeros(grid_size)
        for j, r in enumerate(x_range):
            atom1.set_position(array((-r/2, 0.0, 0.0)))
            atom2.set_position(array((r/2, 0.0, 0.0)))
            u[j] = pot_func(atom1, atom2)
            f[j] = linalg.norm(force_func(atom1, atom2))
        ax1.plot(x_range, u, label=label, ls=lss[i])
        ax2.plot(x_range, f, label=label, ls=lss[i])
        ax1.set_xlabel("distance ($a_0$)")
        ax2.set_xlabel("distance ($a_0$)")
        ax1.set_ylabel("potential ($E_h$)")
        ax2.set_ylabel("force")
        ax1.set_title("Potential")
        ax2.set_title("Force")
        ax1.legend()
        ax2.legend()
    ax1.set_ylim((u.min(), u.max()))
    ax2.set_ylim((f.min(), f.max()))
    fig.suptitle("Problem 3 pair potential plot")
    fig.tight_layout()

    # TODO Why curveature differs so much near equilibrium? Doesn't this mean
    #  that heat capacity differs a lot between the models, as vibration energy
    #  differs?

def problem_1_calculate_with_Morse(atoms, dims, N_atoms):
    # Initialize observables
    observables = Observables()
    initialize_velocities(atoms, T=10.0)
    initialize_force(atoms, "Morse")

    E_k, E_p = calculate_energetics(atoms)
    print('    T', Temperature(E_k,dims,N_atoms))

    for i in range(100000):
        atoms = velocity_verlet_update(atoms, "Morse")
        if (i % 10 == 0):
            observables = calculate_observables(atoms, observables)

    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    observables.E_tot = E_kin + E_pot
    return observables


def problem_3_calculate_with_LJ(atoms, dims, N_atoms):
    # Initialize observables
    observables = Observables()
    initialize_velocities(atoms, T=10.0)
    initialize_force(atoms, "LJ")

    for i in range(100000):
        atoms = velocity_verlet_update(atoms, "LJ")
        if (i % 10 == 0):
            observables = calculate_observables(atoms, observables)

    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    observables.E_tot = E_kin + E_pot
    return observables


def problem_4_calculate_and_variate_T(atoms, dims, N_atoms):
    # Initialize observables

    steps = 100000

    Ts = np.geomspace(10, 4000, 40)
    obs_by_T = []
    for T in Ts:
        initialize_force(atoms, "LJ")
        observables = Observables()
        initialize_velocities(atoms, T=T)

        for i in range(steps):
            atoms = velocity_verlet_update(atoms, "LJ")
            if (i % 10 == 0):
                observables = calculate_observables(atoms, observables)

        E_kin = array(observables.E_kin)
        E_pot = array(observables.E_pot)
        observables.E_tot = E_kin + E_pot
        obs_by_T.append((T, observables))
    return obs_by_T

def plot_observables_by_T(obs_by_T):
    N = len(obs_by_T)
    # x-axis: start temperature
    T_start = []
    # Observables
    E_tot = []
    T_mean = []
    dist = []
    Cv = []  # Heat capacity
    # Errorbars
    E_tot_err = []
    T_mean_err = []
    dist_err = []
    Cv_err = [0.0 for i in range(N)]

    for T, obs in obs_by_T:
        T_start.append(T)
        T_mean.append(mean(obs.Temperature))
        E_tot.append(mean(obs.E_tot))
        dist.append(mean(obs.distance))
        Cv.append(problem_2_heat_capacity(obs.E_tot, obs.Temperature))

        T_mean_err.append(std(obs.Temperature))  # 2*sigma error range
        E_tot_err.append(std(obs.E_tot))
        dist_err.append(std(obs.distance))

    T_mean_err = array(T_mean_err)
    E_tot_err = array(E_tot_err)
    dist_err = array(dist_err)

    observs = [E_tot, T_mean, dist, Cv]
    errs = [E_tot_err, T_mean_err, dist_err, Cv_err]

    fig, ((ax1, ax2), (ax3, ax4)) = subplots(2,2, figsize=(10,10))
    axes = [ax1, ax2, ax3, ax4]
    labels = ["Total energy $E_{\\mathrm{tot}}$",
              "Mean temperature $T_{\\mathrm{mean}}$",
              "Separation $r_{\\mathrm{sep}}$",
              "Heat capavity $C_{\\mathrm{v}}$"]

    for i, (ax, label, data, err) in enumerate(zip(axes, labels, observs, errs)):
        ax.errorbar(T_start, data, err, capsize=5, c="C{}".format(i))
        ax.errorbar(T_start, data, err/sqrt(N), ls="", capsize=2.5,
                    c="k")
        ax.set_xlabel("$T_{\\mathrm{start}}$")
        ax.set_ylabel(label)
        ax.set_title(label)
        ax.set_xscale("log")
        ax.set_yscale("log")
    ax4.set_yscale("linear")

    fig.suptitle("Colored errorbars: variation, \nBlack errorbars:deviation of mean.")



def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    # Initialize atoms
    atoms = []
    for i in range(N_atoms):
        atoms.append(Atom(i, mass, dt, dims))
        atoms[i].set_LJ_parameters(0.1745, 1.25)

    initialize_positions(atoms)

    # Problem 1
    print("Problem 1. Morse potential")
    obs_M = problem_1_calculate_with_Morse(
        copy.deepcopy(atoms), dims, N_atoms)
    print('    E_kin', mean(obs_M.E_kin))
    print('    E_pot', mean(obs_M.E_pot))
    print('    E_tot', mean(obs_M.E_tot))
    problem_1_plot(obs_M.E_tot, dt)


    # Problem 2
    print("\nProblem 2.")
    problem_2_plot(obs_M, dt)
    Cv = problem_2_heat_capacity(obs_M.E_tot, obs_M.Temperature)
    print("    Heat capacity Cv", Cv)
    print("    Temperature and its deviation: {:.3f}±{:.3f}"
          .format( mean(obs_M.Temperature), std(obs_M.Temperature)))

    # Problem 3
    print("\nProblem 3. Lennard-Jones potential")
    problem_3_plot_potentials()
    obs_LJ = problem_3_calculate_with_LJ(
        copy.deepcopy(atoms), dims, N_atoms)
    print('    E_kin', mean(obs_LJ.E_kin))
    print('    E_pot', mean(obs_LJ.E_pot))
    print('    E_tot', mean(obs_LJ.E_tot))

    # Problem 4
    obs_by_T = problem_4_calculate_and_variate_T(
        copy.deepcopy(atoms), dims, N_atoms)
    plot_observables_by_T(obs_by_T)


    show()


if __name__ == "__main__":
    main()
