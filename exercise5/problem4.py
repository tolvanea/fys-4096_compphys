from problem3 import run_sor
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


def main():
    #Parameters
    nx, ny = 25, 25
    x_0, y_0 = -1.0, -1.0
    x_L, y_L = 1.0, 1.0
    h = x_L / nx
    x = np.linspace(x_0, x_L, nx, endpoint=True)
    y = np.linspace(y_0, y_L, ny, endpoint=True)
    X, Y = np.meshgrid(x, y)
    tol = 0.001  # Tolerance

    # initial state
    state_0 = np.zeros((nx,ny))

    # Boundary points: box outer edge
    state_0[[0,nx-1],:] = 0.0
    state_0[:, [0, ny-1]] = 0.0
    rho = np.zeros((nx,ny))

    # Create two rods with boundary points
    # Ok, rods are not ar x=0.3 rather they are at x=1/3
    state_0[6:19, 8] = -1.0
    state_0[6:19, 16] = 1.0
    boundary_points_x = np.hstack((np.arange(6, 19), np.arange(6, 19)))
    boundary_points_y = np.hstack((np.full(19-6, fill_value=8),
                                   np.full(19-6, fill_value=16)))
    points= np.vstack((boundary_points_x, boundary_points_y)).T

    # Run solver
    state_sor, iters = run_sor(tol, state_0, rho, h, w=1.8, boundary_points=points)

    # draw figures
    fig = plt.figure(figsize=(10,5))
    ax1 = fig.add_subplot(121, projection="3d")
    ax1.plot_wireframe(X, Y, state_sor, rstride=1, cstride=1)
    ax1.set_title("Potential")
    ax1.set_xlabel("x")
    ax1.set_ylabel("y")
    ax1.set_zlabel("z")
    ax2 = fig.add_subplot(122)
    grad_y, grad_x = np.gradient(state_sor)
    C = np.hypot(-grad_x, -grad_y) # colors of the arrows
    ax2.quiver(X, Y, -grad_x, -grad_y, C)
    ax2.plot([-0.333, -0.333], [-0.5, 0.5], c="k", lw=2)
    ax2.plot([0.333, 0.333], [-0.5, 0.5], c="k", lw=2)
    ax2.set_title("Electric field")

    fig.suptitle('Solve poisson equation')

    plt.show()

if __name__ == "__main__":
    main()