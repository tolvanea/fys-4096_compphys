import imageio
import numpy as np
import matplotlib.pyplot as plt
from sys import path as p1
from os import path as p2
p1.append(p2.join(p2.dirname(__file__), ".."))

from problem3 import run_sor
from mpl_toolkits.mplot3d import axes3d

def color_indices_of_logo():
    im = imageio.imread('tuni_logo_hand.png')
    gray_scale = im.sum(axis=2)
    #print("im.shape", im.shape, type(im))
    assert gray_scale.shape[0] == gray_scale.shape[1]
    #print("im", np.min(gray_scale,axis=(0,1)), np.max(gray_scale, axis=(0,1)))
    gray_scale = gray_scale[::2,::2] # downscale a bit

    # white:            grayscale = 716-765
    # darker  gray:     grayscale = 531
    # lighter gray:     grayscale = 660
    # violet:           grayscale = 236
    ids_white = gray_scale >= 700
    ids_d_gray = (gray_scale >= 600) == (gray_scale < 700)
    ids_l_gray = (gray_scale >= 400) == (gray_scale < 600)
    ids_violet = gray_scale < 400
    #print(gray_scale[62,:])
    return ids_white, ids_d_gray, ids_l_gray, ids_violet

def create_boundary_points(ids):

    N = ids.shape[0]
    X, Y = np.meshgrid(np.arange(N), np.arange(N))
    X = X.T
    Y = Y.T
    points = np.vstack(((X[ids]).ravel(), (Y[ids]).ravel())).T

    return points

def create_initial_state():
    # Tuni logo shaped points
    ids_white, ids_d_gray, ids_l_gray, ids_violet = color_indices_of_logo()
    N = ids_white.shape[0]

    #Parameters
    nx, ny = N, N
    x_0, y_0 = -0.0, -0.0
    x_L, y_L = N*1.0, N*1.0
    h = x_L / nx
    x = np.linspace(x_0, x_L, nx, endpoint=True)
    y = np.linspace(y_0, y_L, ny, endpoint=True)
    X, Y = np.meshgrid(x, y)
    X = X.T
    Y = Y.T


    # initial state
    state_0 = np.zeros((nx,ny))

    # Boundary points: box outer edge
    state_0[[0,nx-1],:] = 0.0
    state_0[:, [0, ny-1]] = 0.0
    rho = np.zeros((nx,ny))  # -((X-62)**2 + (Y-62)**2).astype(int)

    state_0[ids_d_gray] = 0.80
    state_0[ids_l_gray] = 1.0
    state_0[ids_white] = 1.0


    boundary_point_ids = np.logical_or(ids_d_gray, ids_l_gray)
    # show = (ids_d_gray*0 + ids_l_gray*0 + ids_white)
    # plt.imshow(boundary_point_ids)
    # plt.show()
    # exit(0)

    points = create_boundary_points(boundary_point_ids)

    return X, Y, state_0, rho, h, points

def main():

    X, Y, state_0, rho, h, points = create_initial_state()

    # Run solver
    tol = 0.001  # Tolerance
    state_sor, iters = run_sor(tol, state_0, rho, h, w=1.8, boundary_points=points)
    state_sor = np.rot90(state_sor, k=3)

    # draw figures
    fig = plt.figure(figsize=(10,5))
    ax1 = fig.add_subplot(121, projection="3d")
    ax1.plot_wireframe(X, Y, state_sor, rstride=4, cstride=4)
    ax1.set_title("Potentiaali")
    ax1.set_xlabel("x")
    ax1.set_ylabel("y")
    ax1.set_zlabel("z")
    ax2 = fig.add_subplot(122)
    grad_y, grad_x = np.gradient(state_sor[::2,::2])
    C = np.hypot(grad_x, grad_y) # colors of the arrows
    grad_x = grad_x / (C) * np.log(1+C)
    grad_y = grad_y / (C) * np.log(1+C)
    ax2.quiver(X[::2,::2], Y[::2,::2], -grad_x, -grad_y, C)
    ax2.set_title("Sähkökenttä")

    fig.suptitle("PO-   \nTEN- \nTIAA-\nLIT   ", size=16, weight="black")

    plt.show()

if __name__ == "__main__":
    main()