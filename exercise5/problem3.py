import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


def jacobi_update(phi: np.ndarray, rho: np.ndarray, h):
    """
    Propagate initial guess of a system that satisfies poisson equation in
    equilibrium. Use Jacobi method.
    :param phi:     time slice of system (2d array)
    :param rho:     time constant density (2d array)
    :param h:       spatial step
    :return:        system on next time slice
    """
    phi_new = np.full_like(phi, fill_value=float("nan"))
    nx = phi.shape[0]
    ny = phi.shape[0]
    phi_new[[0,nx-1],:] = phi[[0,nx-1],:]
    phi_new[:, 0] = phi[:, 0]
    phi_new[:, ny-1] = phi[:, ny-1]

    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi_new[i,j] = \
                1/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1] + phi[i,j-1] + h**2 * rho[i,j])

    return phi_new

def gauss_seidel_update(phi: np.ndarray, rho: np.ndarray, h):
    """
    Propagate initial guess of a system that satisfies poisson equation in
    equilibrium. Use Gauss Seidel method.
    :param phi:     time slice of system (2d array)
    :param rho:     time constant density (2d array)
    :param h:       spatial step
    :return:        system on next time slice
    """
    nx = phi.shape[0]
    ny = phi.shape[0]
    phi = phi.copy()

    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            phi[i,j] = \
                1/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1] + phi[i,j-1] + h**2 * rho[i,j])
    return phi

def sor_update(phi: np.ndarray, rho: np.ndarray, h, w, b_points=()):
    """
    Propagate initial guess of a system that satisfies poisson equation in
    equilibrium. Use simultaneous over relazation (SOR) -method. Box sides are
    considered as boundary points.
    :param phi:         time slice of system (2d array)
    :param rho:         time constant density (2d array)
    :param h:           spatial step
    :param b_points:    boundary points within the system, Nx2-matrix
                        Notice! They must be in iteration order, see function
                        'transform_to_iteration_order'.
    :return:            system on next time slice
    """
    phi = phi.copy()
    counter = 0

    for i in range(1, phi.shape[0] - 1):
        for j in range(1, phi.shape[1] - 1):
            # Skip boundary points
            if (counter < len(b_points)) \
                    and (np.array((i,j)) == b_points[counter]).all():
                # assert phi[i,j] > 0.5
                counter += 1
                continue
            phi[i,j] = (1 - w) * phi[i,j] \
                       + w/4 * (phi[i+1,j] + phi[i-1,j] + phi[i,j+1]
                                + phi[i,j-1] + h**2 * rho[i,j])
    if counter != len(b_points):
        print("counter", counter, "b_points", len(b_points))
        assert counter == len(b_points)  # Boundary points are in iteration order
    return phi

def transform_to_iteration_order(bp):
    """
    Transform array of 2D-indices in interation order. Iteration order means
    that they appear after each other in loop:
    for y in range(y_len):
        for x in range(x_len):
            (x,y)
    :param bp:  matrix of [N x 2], where N is number of points.
    """
    for i in range(bp.shape[0]):
        assert bp[i,0] != 0  # Box boundaries indices are calculated separately
        assert bp[i,1] != 0  # Box boundaries indices are calculated separately
    assert bp.shape[1] == 2
    order_x = np.argsort(bp[:,1], kind="mergesort")
    tmp = bp[order_x,:]
    order_y = np.argsort(tmp[:,0], kind="mergesort")
    return tmp[order_y,:]


def test_jacobi_method(tol, state_0, rho, h):
    state = state_0.copy()
    state_old = state_0.copy() - 1
    counter = 0
    while tol > (np.abs(state_old - state) < tol).all():
        state_old = state
        state = jacobi_update(state, rho, h)
        counter += 1
        if counter > 10000:
            print("Did not converge")
            break
    print("jacobi interations {}".format(counter))
    return state, counter

def test_gauss_seidel_method(tol, state_0, rho, h):
    state = state_0.copy()
    state_old = state_0.copy() - 1
    counter = 0
    while tol > (np.abs(state_old - state) < tol).all():
        state_old = state
        state = gauss_seidel_update(state, rho, h)
        counter += 1
        if counter > 10000:
            print("Did not converge")
            break
    print("Gauss-Seidel interations {}".format(counter))
    return state, counter


def run_sor(tol, state_0, rho, h, w = 1.8, boundary_points=None):
    """
    Run simultaneous over relazation to system satifying poisson equation.
    :param tol:             tolerance when to quit iteration. The largerst
                            difference between two consecutive steps.
    :param state_0: initial state
    :param rho:             space-depenent factor on right hand side of poisson
                            equation
    :param h:               spatial step size, notise: dx = dy
    :param boundary_points: list of boundary points withing region. [Nx2] matrix
    :return:                converged state, number of iterations required
    """
    state = state_0.copy()
    state_old = state_0.copy() - 1
    counter = 0

    if boundary_points is not None:
        bp = boundary_points
        bp = transform_to_iteration_order(bp)
        assert bp.shape[1] == 2
    else:
        bp = ()

    while tol > (np.abs(state_old - state) < tol).all():
        state_old = state
        state = sor_update(state, rho, h, w, bp)
        counter += 1
        if counter > 10000:
            print("Did not converge: ", end="")
            break
    print("SOR interations {}".format(counter))
    return state, counter

def main():
    #Parameters
    nx, ny = 20, 20
    x_0, y_0 = 0.0, 0.0
    x_L, y_L = 1.0, 1.0
    h = x_L / nx
    x = np.linspace(x_0, x_L, nx)
    y = np.linspace(y_0, y_L, ny)
    X, Y = np.meshgrid(x, y)
    tol = 0.001 # Tolerance

    # initial state
    state_0 = np.zeros((nx,ny))
    state_0[[0,nx-1],:] = 0.0
    state_0[:, 0] = 0.0
    state_0[:, ny-1] = 1.0
    rho = - np.ones((nx,ny))

    # Test different solvers
    state_jacobi, itr_jac= test_jacobi_method(tol, state_0, rho, h)
    state_gauss_seidel, itr_gs = test_gauss_seidel_method(tol, state_0, rho, h)
    state_sor, itr_sor = run_sor(tol, state_0, rho, h)

    # draw figures
    fig = plt.figure(figsize=(10,10))
    ax1 = fig.add_subplot(221, projection="3d")
    ax1.plot_wireframe(X, Y, state_0, rstride=1, cstride=1)
    ax1.set_title("Initial state")
    ax2 = fig.add_subplot(222, projection="3d")
    ax2.plot_wireframe(X, Y, state_jacobi, rstride=1, cstride=1)
    ax2.set_title("Jacobi ({} iterations)".format(itr_jac))
    ax3 = fig.add_subplot(223, projection="3d")
    ax3.plot_wireframe(X, Y, state_gauss_seidel, rstride=1, cstride=1)
    ax3.set_title("Gauss Seidel ({} iterations)".format(itr_gs))
    ax4 = fig.add_subplot(224, projection="3d")
    ax4.plot_wireframe(X, Y, state_sor, rstride=1, cstride=1)
    ax4.set_title("SOR ({} iterations)".format(itr_sor))
    fig.suptitle('Solve poisson equation')

    plt.show()

if __name__ == "__main__":
    main()