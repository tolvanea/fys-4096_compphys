import numpy as np
from exercise5_help_files import runge_kutta
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


def lorenz_derivative(u: np.ndarray, t: float, args):
    """ Caluculate loretz force for particle."""
    r = u[:3]
    v = u[3:]
    q, m, E, B = args

    out = np.empty(len(u))

    out[:3] = v
    out[3:] = q/m * E + q/m * np.cross(v, B)

    return out


def main():

    # Set system electric field and magnetic field
    # q*E / m == 0.05  and q*B / m == 4.0
    unknown_constant = 1.0
    m = unknown_constant  # unknown, but it cancels out eventually
    q = unknown_constant  # unknown, but it cancels out eventually
    E_magn = 0.05 * m / q   # electric field magnitude
    B_magn = 4.0 * m / q    # electric field magnitude
    E = E_magn * np.array([1.0, 0.0, 0.0])
    B = B_magn * np.array([0.0, 1.0, 0.0])

    args = q, m, E, B

    # Set initial values
    t0 = 0.0
    t = np.linspace(t0, 5, 1000)
    v0 = np.array([0.1, 0.1, 0.1])
    r0 = np.array([0.0, 0.0, 0.0])
    u = np.hstack((r0, v0))

    # solve ODE with our own solver
    sol = np.empty((len(u),len(t)))
    for i in range(len(t)-1):
        sol[:,i] = u
        dt = t[i+1] - t[i]
        u, tp = runge_kutta.runge_kutta4(u, t[i] ,dt, lorenz_derivative,
                                         args=(args,))
    sol[:, len(t)-1] = u

    # Draw figures
    fig = plt.figure(figsize=(10,5))
    ax1 = fig.add_subplot(121, projection="3d")
    ax1.plot(sol[0,:], sol[1,:], sol[2,:], 'b', label='position')
    ax1.legend(loc='best')
    ax2 = fig.add_subplot(122, projection="3d")
    ax2.plot(sol[3,:], sol[4,:], sol[5,:], 'b', label='velocity')
    ax2.legend(loc='best')
    fig.suptitle('solve lorenz')
    plt.show()

main()