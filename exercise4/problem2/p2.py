import numpy as np

# import help files from  directory "../exercise4_help_files"
from sys import path as p1
from os import path as p2
p1.append(p2.realpath(p2.dirname(p2.dirname(__file__))))
from exercise4_help_files import read_xsf_example as ex


def p2():
    print("")
    filenames = ['../exercise4_help_files/dft_chargedensity1.xsf',
                 '../exercise4_help_files/dft_chargedensity2.xsf']
    for name in filenames:
        rho, lattice, grid, shift = ex.read_example_xsf_density(name)

        elem = np.zeros((3,3))
        for i in range(3):
            elem[i,:] = lattice[i,:] / (grid[i]-1)

        V = np.linalg.det(elem)
        s = np.sum(rho[:-1,:-1,:-1], axis=(0,1,2))

        N = s * V
        print("{}\n-----".format(name))
        print("number of electrons: {}".format(N))

        A = lattice
        B = np.transpose(2 * np.pi * np.linalg.inv(A))

        print("reciprocal lattice vectors: \n{}".format(B))
        print("")


def main():
    p2()


main() 
