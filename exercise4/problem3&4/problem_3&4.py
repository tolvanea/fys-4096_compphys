import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt

# import help files from  directory "../exercise4_help_files"
from sys import path as p1
from os import path as p2
p1.append(p2.realpath(p2.dirname(p2.dirname(__file__))))
from exercise4_help_files import read_xsf_example as ex
from exercise4_help_files import spline_class as sp


def p3():

    print("")
    filenames = ['../exercise4_help_files/dft_chargedensity1.xsf',
                 '../exercise4_help_files/dft_chargedensity2.xsf',
                 '../exercise4_help_files/dft_chargedensity2.xsf']  # <- I'm lazy
    fig, axs = plt.subplots(1,2, figsize=(10,5))

    for itr, name in enumerate(filenames):
        # load data
        rho, lattice, grid, shift = ex.read_example_xsf_density(name)

        # construct all points in coordinates [0,1), [0,1), [0,1)
        α1 = np.linspace(0,1,grid[0]-1)
        α2 = np.linspace(0,1,grid[1]-1)
        α3 = np.linspace(0,1,grid[2]-1)

        u_X, u_Y, u_Z = np.meshgrid(α1, α2, α3)
        α_points = np.vstack((np.ravel(u_X), np.ravel(u_Y), np.ravel(u_Z)))

        # Stretch the grid to real coordinates
        A = lattice.T
        r_points = (A @ α_points)

        # Generate line coordinates
        if itr == 0:
            r0 = np.array([0.1, 0.1, 2.8528])
            r1 = np.array([4.45, 4.45, 2.8528])
        elif itr == 1:
            r0 = np.array([-1.4466, 1.3073, 3.2115])
            r1 = np.array([1.4361, 3.1883, 1.3542])
        elif itr == 2:
            r0 = np.array([2.9996, 2.1733, 2.1462])
            r1 = np.array([8.7516, 2.1733, 2.1462])
        N_line = 500
        u_line = np.linspace(0,1,N_line)
        r = r0[:, np.newaxis] + np.outer((r1-r0), u_line)

        spline_func = "ilkka"

        # Scipy version: Slow, poor implementation and unaccurate algorithm
        if spline_func == "scipy":
            skip = 1  # parameter to take only fraction of data in account
            points, values = r_points[:,::skip], rho[:-1,:-1,:-1].ravel()[::skip]
            print("points", points.shape)
            print("values", values.shape)
            print("r[0,:]", r[0,::20].shape)
            line_interp = griddata(points.T, values, (r[0,:], r[1,:], r[2,:]), method='linear')
        # Ilkka's version: superior speed and accuracy
        elif spline_func == "ilkka":

            # My ugly lazy hack to speed up:
            # Do not calculate spline again with dens2's second line
            if itr != 2:
                spl3d = sp.spline(x=α1, y=α2, z=α3, f=rho[:-1,:-1,:-1], dims=3)
            line_interp = np.zeros(r.shape[1])
            for i in range(r.shape[1]):
                u = np.linalg.inv(A) @ r[:,i]  # negative values permitted
                u = np.mod(u, 1.0)
                #print("u", u, "itr", itr)

                line_interp[i] = spl3d.eval3d(*u)

        ax_idx = min(itr, 1)
        axs[ax_idx].plot(u_line*np.linalg.norm(r1-r0), line_interp, label="line {}".format(itr))
        axs[ax_idx].set_xlabel("r")
        axs[ax_idx].set_ylabel("$\\rho$")
        if ax_idx != 0:
            axs[ax_idx].legend()

    axs[0].set_title("dens1")
    axs[0].text(0.5, 0.9, "Notice that only valence electrons \n" +
                "are counted (at nuclei density goes to 0).",
                horizontalalignment='center',
                verticalalignment='center',
                transform=axs[0].transAxes)
    axs[1].set_title("dens2")
    axs[1].text(0.5, 0.9, "",
                horizontalalignment='center',
                verticalalignment='center',
                transform=axs[0].transAxes)
    plt.show()


def main():
    p3()


main()
