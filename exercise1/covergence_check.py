 
import numpy as np
from num_calculus.differentiation import eval_derivative, eval_derivative_v2, eval_2nd_derivative
from num_calculus.integration import monte_carlo_integration, simple_simpson, riemann_sum
import matplotlib.pyplot as plt


def get_integrals(gridsizes):
    """Generates the integrals and grids for plots. Reduces some repetition in 
    code."""
    f = lambda x: np.sin(x)
    int_f = lambda x: -np.cos(x)
    
    g = lambda x: np.exp(-x)
    int_g = lambda x: -np.exp(-x)
    
    start, end = 0, np.pi/2

    # exact values for integrals
    f_exact = int_f(end) - int_f(start)
    g_exact = int_g(end) - int_g(start)
    
    return f, g, f_exact, g_exact, start, end


def plot_normal_integrals(ax, gridsizes):
    """Plots cnovergence of sin(x) and exp(x) with rieman sum and simpson compares it to exact value."""
    
    f, g, f_exact, g_exact, start, end = get_integrals(gridsizes)
    
    # Results are stored in these and then plotted
    f_conv = np.empty((len(gridsizes), 3), dtype=np.float_)
    g_conv = np.empty((len(gridsizes), 3), dtype=np.float_)
    
    for i, gs in enumerate(gridsizes):
        points = np.linspace(start, end, int(gs), endpoint=True)
    
        f_r_int = riemann_sum(points, f)
        f_s_int = simple_simpson(points, f)
        
        g_r_int = riemann_sum(points, g)
        g_s_int = simple_simpson(points, g)
        
        f_conv[i,:] = (f_r_int, f_s_int, f_exact)
        g_conv[i,:] = (g_r_int, g_s_int, g_exact)
        
    ax.plot(gridsizes, f_conv[:,0], label="$sin(x)$, Riemann sum")
    ax.plot(gridsizes, f_conv[:,1], label="$sin(x)$, Simpson's integration")
    ax.plot(gridsizes, f_conv[:,2], label="$sin(x)$, Exact", color="c")
    
    ax.plot(gridsizes, g_conv[:,0], ls="--", label="$e^{-x}$, Riemann sum")
    ax.plot(gridsizes, g_conv[:,1], ls="--", label="$e^{-x}$, Simpson's integration")
    ax.plot(gridsizes, g_conv[:,2], ls="--", label="$e^{-x}$, Exact", color="c")
    ax.set_xscale("log", nonposx="clip")
    
    ax.set_xlabel("gridsize")
    ax.set_ylabel("integral")
    ax.set_title("Riemann and Simpson")
    
    ax.set_ylim(0.75, 1.02)
    
    ax.legend()

    
def plot_monte_carlo(ax, gridsizes):
    """Plots cnovergence of sin(x) and exp(x) with Monte Carlo compares it to exact value."""
    
    f, g, f_exact, g_exact, start, end = get_integrals(gridsizes)
    
    # Results are stored in these and then plotted
    f_conv = np.empty((len(gridsizes), 2), dtype=np.float_)
    g_conv = np.empty((len(gridsizes), 2), dtype=np.float_)

    for i, gs in enumerate(gridsizes):
        iter = int(gs // 100)
        f_int, f_err = monte_carlo_integration(f, start, end, 100, iter)
        g_int, g_err = monte_carlo_integration(g, start, end, 100, iter)
        
        f_conv[i,:] = (f_int, f_err)
        g_conv[i,:] = (g_int, g_err)
        
    g_int
            
    ax.errorbar(gridsizes, f_conv[:,0], f_conv[:,1], label="$sin(x)$, Monte Carlo")
    ax.errorbar(gridsizes, g_conv[:,0], g_conv[:,1], label="$e^{-x}$, Monte Carlo")
    
    ef = np.full_like(gridsizes, fill_value=f_exact) # exact
    eg = np.full_like(gridsizes, fill_value=g_exact) # exact
    ax.plot(gridsizes, ef, label="$sin(x)$, Exact")
    ax.plot(gridsizes, eg, label="$e^{-x}$, Exact")
    ax.set_xscale("log", nonposx="clip")
    
    ax.set_xlabel("N")
    ax.set_ylabel("integral")
    ax.set_title("Monte Carlo")
    
    ax.set_ylim(0.75, 1.02)
    
    ax.legend()

        
    

def main():
    """Main function: plot the figures."""
    
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,5))
    
    gridsizes = np.logspace(2,5,7)  # 100 - 100 000
    
    plot_normal_integrals(ax1, gridsizes)
    plot_monte_carlo(ax2, gridsizes)
    
    fig.savefig("convergence.pdf")
    plt.show()




# Run tests if called directly.
if __name__=="__main__":
    main()
