# FYS-4096 2018-01 Computational Physics

# This file is build around example by Ilkka Kylänpää.

# Exercise 1, Problem 3: Programming exercise

import numpy as np



# Problem 3 a)



def eval_derivative(f, x, dx):
    """Evalueates first derivative with simple approach. Error is order O(h)."""
    return (f(x+dx) - f(x)) / dx


def eval_derivative_v2(f, x, dx):
    """Evalueates first derivative with a bit better approach. Error is order 
    O(h^2)."""
    return (f(x+dx) - f(x-dx)) / (2 * dx)

    
    
# Problem 3 b)
    
    
    
def eval_2nd_derivative(f, x, dx):
    """Second derivative form from slides."""
    return (f(x+dx) + f(x-dx) -2*f(x)) / (dx**2)
    
    

    
