# FYS-4096 2018-01 Computational Physics

# Exercise 1, Problem 3: Programming exercise
# This file is build around example by Ilkka Kylänpää.

import numpy as np



# Problem 3 c)



def riemann_sum(points, f):
    """Riemannn sum. Inverval 'points' includes end point."""
    
    sum = 0.0
    for i in range(len(points)-1):
        h = (points[i+1] - points[i])
        sum += f(points[i]) * h
    return sum



def simpson_integral(points, f):
    """Simpson integral. Inverval 'points' includes the end point."""
    
    # If there's odd number of points, calculate last item alone
    if len(points)%2 == 0:
        N = len(points)
    else:
        N = len(points)-1
    
    sum = 0.0
    h = np.diff(points)
    print("len(h), len(points)", len(h), len(points))
    
    # Main part of integral (last point may not be included in this)
    for i in range(0, N//2-1):
        h1 = h[2*i+1]
        h0 = h[2*i]        
        α = (2*h1**3 - h0**3 + 3*h0*h1**2) / (6*h1 * (h1+h0))
        β = (h1**3 + h0**3 + 3*h1*h0*(h1+h0)) / (6*h1*h0)
        η = (2*h0**3 - h1**3 + 3*h1*h0**2 ) / (6*h0 * (h1+h0))
        
        sum += α*f(points[2*i+2]) + β*f(points[2*i+1]) + η*f(points[2*i])
    
    # If there's odd number of points, calculate last item alone
    if len(points)%2 == 1:
        h1 = h[N-1]
        h2 = h[N-2] 
        α = (2*h1**2 + 3*h1*h2) / (6 * (h2+h1))
        β = (h1**2 + 3*h1*h2) / (6 * h2)
        η = - h1**3 / (h2 + h1)
        
        sum += α*f(h0) + β*f(h1) + η*f(h2)
        
    return sum


def simple_simpson(points, f):
    """"This is uniformly spaced version of simpson integral. (Points include the last index of range)."""
    
    p = points
        
    # Grid points must be evenly spaced
    assert(np.isclose(p[1]-p[0], (p[-1]-p[0])/(len(p)-1),
                      rtol=0.001))
    
    h = (p[-1]-p[0])/len(p-1)
    sum = 0.0
    N = len(p)
    # If there's odd number of points, calculate last item alone
    if len(p)%2 == 1: 
        N += -1
    
    for i in range(N//2-1):
        sum += f(p[2*i]) + 4*f(p[2*i+1]) + f(p[2*i+2])
            
    sum *= (h/3)

    # If there's odd number of points, calculate last item alone
    if len(p)%2 == 1: 
        sum += (h/12) * (-f(p[-3]) + 8*f(p[-2]) + 5*f(p[-1]))
        
    return sum



# Problem 3 c)
    
    
# This funtion is copied from example made by Ilkka Kylänpää.
def monte_carlo_integration(fun, xmin, xmax, blocks, iters):
    """Monte Carlo inegration with uniform distribution."""
    
    # Blocks are just temporary results that used for calculating standard deviation.
    block_values=np.zeros((blocks,))
    
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            #generate random point on given interval
            x = xmin+np.random.rand() * L 
            block_values[block] += fun(x)
        block_values[block] /= iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI




    




# Run this if called directly.
if __name__=="__main__":
    print("Run 'print_test_values.py' outside the package for prints")
