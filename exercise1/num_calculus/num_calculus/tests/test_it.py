import unittest
import numpy as np
#import importlib
#pkg = importlib.import_module(__package__)
from num_calculus.differentiation import eval_derivative, eval_derivative_v2, \
        eval_2nd_derivative
from num_calculus.integration import riemann_sum, simple_simpson, \
        simpson_integral, monte_carlo_integration

class TestStringMethods(unittest.TestCase):
    def test_first_derivative_cos(self):
    
        f = lambda x: np.sin(x)
        der_f = lambda x: np.cos(x)
        
        a = eval_derivative(f, 2.0, 0.01)
        b = eval_derivative_v2(f, 2.0, 0.01)
        exact = der_f(2.0)  # exact
        
        self.assertTrue(np.isclose(a, exact, rtol=0.1))
        self.assertTrue(np.isclose(b, exact, rtol=0.1))
        
        
    def test_first_derivative_polynomial(self):
        
        g = lambda x: (3*x**3 + 2*x*2 + x - 3)
        a = eval_derivative(g, 2.0, 0.01)
        b = eval_derivative_v2(g, 2.0, 0.01)
        der_g = lambda x: 9*x**2 + 4*x + 1
        exact = der_g(2.0)  # exact
        
        self.assertTrue(np.isclose(a, exact, rtol=0.1))
        self.assertTrue(np.isclose(b, exact, rtol=0.1))
        
        
    def test_second_derivative(self):
    
        g = lambda x: (3*x**3 + 2*x*2 + x - 3)
        der2_g = lambda x: 18*x + 4
        
        a = eval_2nd_derivative(g, 2.0, 0.01)
        exact = der2_g(2.0)  # exact
        
        self.assertTrue(np.isclose(a, exact, rtol=0.1))  
        
        
    def test_integrals(self):
        f = lambda x: np.sin(5*x)
        int_f = lambda x: -(1/5) * np.cos(5*x)
        
        start = 1
        end = 3
        N = 100
        points = np.linspace(start, end, N, endpoint=True)
        
        r_int = riemann_sum(points, f)
        s1_int = simple_simpson(points, f)
        s2_int = simpson_integral(points, f)
        m_int, err = monte_carlo_integration(f, start, end, 100, 100)
        e_int = int_f(end) - int_f(start)  # exact
        
        self.assertTrue(np.isclose(r_int, e_int, rtol=0.1))
        self.assertTrue(np.isclose(s1_int, e_int, rtol=0.1))
        # TODO fix general case of simpson integration
        #self.assertTrue(np.isclose(s2_int, e_int, rtol=0.1))
        self.assertTrue(np.isclose(m_int, e_int, rtol=0.1))
        
        
    
    
if __name__ == '__main__':
    unittest.main()
