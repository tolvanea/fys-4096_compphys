from setuptools import setup

setup(name='num_calculus',
      version='0.1',
      description='CompPhys course. Numerical differentiation and integration',
      url='https://gitlab.com/tolvanea/fys-4096_compphys',
      author='Alpi Tolvanen',
      author_email='alpi.tolvanen@tutanota.com',
      license='MIT',
      packages=['num_calculus'],
      zip_safe=False,
      #test_suite="unittest.TestSuite",
      test_require=["unittest"])
