import numpy as np 
from num_calculus.differentiation import eval_derivative, eval_derivative_v2, eval_2nd_derivative
from num_calculus.integration import monte_carlo_integration, simple_simpson,simpson_integral, riemann_sum



def print_first_derivative():
    """Check if first derivative approximation works."""
    
    f = lambda x: np.sin(x)
    der_f = lambda x: np.cos(x)
    
    a = eval_derivative(f, 2.0, 0.01)
    b= eval_derivative_v2(f, 2.0, 0.01)
    c = der_f(2.0)
    
    print("Problem 3 a)")
    print(" d/dx sin(x) at x=2 with dx=0.01")
    print(" Approximation O(h):  {}".format(a))
    print("(Approximation O(h^2) {})".format(b))
    print(" Exact:               {}".format(c))


def print_second_derivative():
    """Riemannn sum. Inverval 'points' includes end point."""
    
    f = lambda x: (3*x**3 + 2*x*2 + x - 3)
    der2_f = lambda x: 19*x + 4
    
    a = eval_2nd_derivative(f, 2.0, 0.01)
    b = der2_f(2.0)
    
    print("Running numerical differentiations with few values and printing them. Tests are located in tests directory.")
    print("Problem 3 b)")
    print("    d^2/dx^2 (3*x**3 + 2*x*2 + x - 3) at x=2 with dx=0.01")
    print("    Approximation O(h):  {}".format(a))
    print("    Exact:               {}".format(b))


def print_integrals():
    """Prints integrals in problems 3 c) and d)."""
    
    f = lambda x: np.sin(5*x)
    int_f = lambda x: -(1/5) * np.cos(5*x)
    
    start = 1
    end = 3
    N = 100
    points = np.linspace(start, end, N, endpoint=True)
    
    r_int = riemann_sum(points, f)
    s1_int = simple_simpson(points, f)
    s2_int = simpson_integral(points, f)
    m_int, err = monte_carlo_integration(f, start, end, 100, 100)
    e_int = int_f(end) - int_f(start)  # exact
    
    print("Problem 3 c) and d)")
    print("    Riemann integral:           {}".format(r_int))
    print("    Simpson integral, (simple)  {}".format(s1_int))
    print("    Simpson integral: (general) {}".format(s2_int))
    print("    Monte Carlo integration     {:.3f}({:.3f})".format(m_int, err))
    print("    Exact integral:             {}".format(e_int))


def main():
    """ Main function: print some values."""
    print("Running numerical methdos with few values and printing some data.")
    print("This are quite same as test, but now user can see actual results.")
    print("To run actual tests, run 'python3 setup.py test'\n")
    print_first_derivative()
    print("")
    print_second_derivative()
    print("")
    print_integrals()
    print("")
    
    
# Run tests prints if called directly.
if __name__=="__main__":
    main()
