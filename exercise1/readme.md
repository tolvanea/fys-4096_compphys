# num_calculus package

The python package num_calculus (or num-calculus on pip3) contains functions for numerical differentiation and integration. The package also contains built in tests. Convergence plot script for problem 4 is located outside the package, and it requires installation.
 
##Usage:

### Run tests:
    Go to same directory with setup.py
    ```cd num_calculus
    Run tests
    ```python3 setup.py test```
    
### Installing and uninstalling.
    Installation is needed to plot convergence. Also installation is needed for printing calculated results data on tests (extra part).

#### Using pip3 (easiest)
    Go to same directory with setup.py
    ```cd num_calculus```
    install
    ```pip3 install .```
    For uninstalling, you may need to remove generated files by hand first. This includes all egg-files, build-files, dist-files in directories 'exercise1' and 'num_calculus'. Then run command.
    ```pip3 uninstall num-calculus```

#### Using setup.py (alternative way)
    Go to same directory with setup.py
    ```cd num_calculus```
    install
    ```python3 setup.py install --record files.txt```
    The argument "--record files.txt" makes it possible to remove package by following uninstall command:
    ```cat files.txt | xargs rm -rf```
    
### Plot convergence
    ```python3 covergence_check.py```
    
    
### Print random values on calculated results
    This is like tests, but now users sees the actual results. This requires installation.
    ```python3 print_test_values.py```
    
