"""
Problem 2 of exercises 6: Solve poisson by finite elements method.
"""

import numpy as np
import matplotlib.pyplot as plt


def A_and_b_by_given_matrix_elements(xvec: np.ndarray):
    """
    Calculate matrix element's of A and vector b by formula given in problem.
    """
    h = (xvec[-1]-xvec[0])/len(xvec)
    diag0 = np.full(len(xvec), fill_value=2.0 / h)
    diag1 = np.full(len(xvec) - 1, fill_value=-1.0 / h)
    A = np.diag(diag0) + np.diag(diag1, k=+1) + np.diag(diag1, k=-1)
    A[[0,-1],:] = 0.0
    A[:,[0,-1]] = 0.0
    A[0,0] = 1.0
    A[-1,-1] = 1.0

    b = np.zeros_like(xvec)
    for i in range(1, len(xvec)-1):
        b[i] = np.pi/h * (xvec[i-1] + xvec[i+1] - 2*xvec[i]) \
               * np.cos(np.pi * xvec[i]) \
               + 1/h * (2 * np.sin(np.pi * xvec[i]) - np.sin(np.pi * xvec[i-1])
                        - np.sin(np.pi * xvec[i+1]))
    return A, b

def A_and_b_by_integration(xvec: np.ndarray):
    """
    Calculate matrix element's of A and vector b by numerical integration.
    """
    from scipy.misc import derivative

    # Instead of creating uhat-lambda inside for loop, I create them beforehand.
    # Vectorize function: u_hat(float) --> u_hat(np.ndarray)
    uhat_vec = np.vectorize(u_hat, excluded=[1,2])
    # Create list of basis functions and its derivative.
    u_base = [None for i in range(len(xvec))]
    u_derivs = [None for i in range(len(xvec))]
    for i in range(len(u_base)):
        # Here I use method called currying, which is better known in functional
        # programming languages. Below is commented out what does not work.
        u_i = (lambda i_fix: (lambda x: uhat_vec(x, i_fix, xvec)))(i)
        u_base[i] = u_i
        u_derivs[i] = (lambda ufix: lambda x: derivative(ufix, x, dx=1e-5))(u_i)

    # Following does not work due to the lazy evaluation of list lambdas!
    #   u_base = [ uhat_vec(x, i, xvec) for i in range(len(xvec))]
    #   u_derivs = [lambda x: derivative(u, x, dx=1e-5) for u in u_base]
    # Below is a good example:
    # https://stackoverflow.com/questions/53086592/lazy-evaluation-when-use-lambda-and-list-comprehension


    def A(j,i):
        if not (i <= j+1 and i >= j-1):
            return 0
        # index is i-3 (or 0)  /  i+3 (or end-1)
        lo, hi = max(i-3,0), min(i+3,len(xvec)-1)
        x = np.linspace(xvec[lo], xvec[hi], 1000)
        kernel = u_derivs[j](x) * u_derivs[i](x)
        return np.trapz(kernel, x=x)

    def rho(x):
        return np.pi*np.sin(np.pi*x)

    def b(i):
        lo, hi = max(i-3,0), min(i+3,len(xvec)-1)  # i-3 or 0 / i+3 or end-1
        x = np.linspace(xvec[lo], xvec[hi], 1000)  # we know that ubase_i is
                                                   # zero elsewhere
        kernel = rho(x)*u_base[i](x)
        return np.trapz(kernel, x=x)

    A_out = np.zeros((len(xvec),len(xvec)))
    A_out[0,0] = 1.0
    A_out[-1,-1] = 1.0
    b_out = np.zeros((len(xvec)))
    b_out[0] = b(0)
    b_out[-1] = b(len(b_out)-1)
    for j in range(1,len(xvec)-1):
        for i in range(1,len(xvec)-1):
            a = A(j,i)
            #print("a", a)
            A_out[j,i] = a
        b_out[j] = b(j)

    return A_out, b_out

def u_hat(x: float, i: int, xvec: np.ndarray) -> float:
    """
    Hat basis function.
    """

    h = xvec[i] - xvec[i-1]
    assert i >= 0 and i <= len(xvec)-1
    if i == 0 or i == len(xvec)-1:
        return 0.0
    elif (i>0 and (x >= xvec[i - 1])) and (x < xvec[i]):
        return (x - xvec[i - 1]) / h
    elif (x >= xvec[i]) and (i<len(xvec)-1 and (x < xvec[i + 1])):
        return (xvec[i + 1] - x) / h
    else:
        return 0.0

def sum_of_u_bases(x: float, a: np.ndarray, xvec: np.ndarray) -> None:
    """
    Naive loop to add all functions in u basis. This can be greatly optimized by
    cropping of zero-calculations, but meh, this works too.
    """
    sum = 0.0
    for i in range(0,len(a)):
        sum += a[i] * u_hat(x, i, xvec)
    return sum

def compare_matrices(A1, b1, A2, b2):
    np.set_printoptions(formatter={'float': lambda x: "{0: 8.3f}".format(x)})
    print("A1[:6,:6]")
    print(np.matrix(A1[:6,:6]))
    print("A2[:6,:6]")
    print(np.matrix(A2[:6,:6]))

    print("")
    print("b1[6:]")
    print(b1[:6])
    print("b2[6:]")
    print(b2[:6])
    print("")


def main():
    N = 50
    h = 1 / N
    xvec = np.linspace(0-h,1+h,N+2, endpoint=True)
    A1, b1 = A_and_b_by_given_matrix_elements(xvec)
    A2, b2 = A_and_b_by_integration(xvec)

    Hervanta_constant = 1.0
    b2 *= Hervanta_constant

    fig, ax = plt.subplots(1,1)
    grid_vec = np.linspace(0,1,2*N)
    ax.plot(grid_vec,np.sin(np.pi*grid_vec),label="$\\phi$, analytic")

    for A, b, label in ((A1, b1, "$\\phi$, precalculated, FEM"),
                        (A2, b2, "$\\phi$, integrated, FEM\nwe get curve closer by\nincreasing accuracy")):
        print(label)
        a = np.zeros_like(b)
        a[1:-1] = np.linalg.solve(A[1:-1,1:-1], b[1:-1])

        phi = np.zeros_like(grid_vec)
        phi[0] = 0.0  # TODO Mitä ovat u[0] tai u[N-1] ?
        phi[-1] = 0.0
        for i in range(1,len(grid_vec)-1):
            x = grid_vec[i]
            phi[i] = sum_of_u_bases(x, a, xvec)

        ax.plot(grid_vec,phi,label=label)

    ax.set_xlabel("$x$")
    ax.set_ylabel("$\\phi$")
    ax.legend()
    ax.set_title("Poisson eq. solved by given$A_{ij},b_i$\n or with integrated $A_{ij},b_i$.")

    compare_matrices(A1, b1, A2, b2)

    plt.show()


if __name__ == "__main__":
    main()