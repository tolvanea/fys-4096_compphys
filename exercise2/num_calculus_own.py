import numpy as np
# Alpi Tolvanen 2019-01-16



def monte_carlo_integration_nd(fun, lims, blocks, iters, args=None):
    """ 
    N-D Monte Carlo integration with uniform random numbers
    in range [xmin,xmax]. 
    
    Args
        fun:    callable(r), where r=np.array[x1,x2...xN]
        lims:   limits, np.array([[x1_min, x2_max], [x2_min, x2_max]...])
        blocks: number means of samples that is used to calculate standard deviation
        iters:  number of samples in a block
        args:   arguments
        
    Returns
        interal result and error estimate with ~68% reliability.
    """
    if not isinstance(lims, np.ndarray):
        lims = np.array(lims)
    assert(lims.shape[1] == 2)
    
    for pair in lims:
        assert(pair[0] < pair[1])  # min is smaller than max

    dim = len(lims)
    args = args or ()
    
    block_values = np.zeros((blocks,))
    box = lims[:,1] - lims[:,0]
    for block in range(blocks):
        for i in range(iters):
            x = lims[:,0] + np.random.rand(dim) * box
            block_values[block] += fun(x, *args)
        block_values[block]/=iters
    I = box.prod()*np.mean(block_values)
    dI = box.prod()*np.std(block_values)/np.sqrt(blocks)
    return I, dI 



# This orginates from excercise 1
def simpson_integral(points, f):
    """Simpson integral. Inverval 'points' includes the end point. This is non-uniform grid implementation."""
    
    # If there's odd number of points, calculate last item alone
    if len(points)%2 == 0:
        N = len(points)
    else:
        N = len(points)-1
    
    sum = 0.0
    h = np.diff(points)
    print("len(h), len(points)", len(h), len(points))
    
    # Main part of integral (last point may not be included in this)
    for i in range(0, N//2-1):
        h1 = h[2*i+1]
        h0 = h[2*i]        
        α = (2*h1**3 - h0**3 + 3*h0*h1**2) / (6*h1 * (h1+h0))
        β = (h1**3 + h0**3 + 3*h1*h0*(h1+h0)) / (6*h1*h0)
        η = (2*h0**3 - h1**3 + 3*h1*h0**2 ) / (6*h0 * (h1+h0))
        
        sum += α*f(points[2*i+2]) + β*f(points[2*i+1]) + η*f(points[2*i])
    
    # If there's odd number of points, calculate last item alone
    if len(points)%2 == 1:
        h1 = h[N-1]
        h2 = h[N-2] 
        α = (2*h1**2 + 3*h1*h2) / (6 * (h2+h1))
        β = (h1**2 + 3*h1*h2) / (6 * h2)
        η = - h1**3 / (h2 + h1)
        
        sum += α*f(h0) + β*f(h1) + η*f(h2)
        
    return sum


def derivative_Oh2(f, x, dx):
    """Evalueates first derivative with a bit better approach. Error is order 
    O(h^2)."""
    return (f(x+dx) - f(x-dx)) / np.linalg.norm(2 * dx)


def gradient(f, x, h):
    """
    Calculates vector gradient for callable.
    
    Args:
        f:      callable
        x:      vector position x
        h:      scalar difference
    Return:
        gradient ndarray with length of dimensions
    """
    gradient = np.empty(len(x))
    for dim in range(len(x)):
        hvec = x*0.0
        hvec[dim] = h
        der = derivative_Oh2(f, x, hvec)
        gradient[dim] = der
        
    return gradient


def gradient_decent(f, start, iter, h):
    """ Gradient decent algorithm with steps to steepest direction.
    
    Args:
        start:  start position
        iter:   iter count
        h:      difference used in gradient calculation. Also step size is
                proportional to this.
    Return:
        end point
    """
    
    x = start
    for i in range(iter):
        g = gradient(f, x, h)
        γ = 10*h / np.linalg.norm(gradient(f, x, h))
        x = x - γ * g
        
    return x
    


# Oh my bad. In problem 3, the gradien was probaly asked for function, instead 
# of matrix. That's easier to implement

#def gradient(m):
#    """
#    Calculates gradient of matrix, with error O(h^2)
#    
#    Args:
#        m:   matrix
#        
#    Returns
#        [g_0, g_1 ... g_N-1], 
#            where N = dim(m), and g_X gradient in that dimension. Gradient 
#             has shape m.shape.
#    """
#    gradients = [None for s in m.shape]
#    for dim, l in enumerate(m.shape):
#        pass
    
    
