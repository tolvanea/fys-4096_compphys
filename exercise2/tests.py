import unittest
from exercise2_help_files.linear_interp import *
from num_calculus_own import monte_carlo_integration_nd, gradient, gradient_decent
from scipy.integrate import nquad

# Tests for problem 1
class TestLinearInterpModule(unittest.TestCase):
    
    def test_1d(self):
        x1=np.linspace(0.,2.*np.pi,10)
        y1=np.sin(x1)
        x2=np.linspace(0.,2.*np.pi,20)
        y2=np.sin(x2)
        lin1d=linear_interp(x=x1,f=y1,dims=1)
        
        self.assertTrue(self.are_close(y2, lin1d.eval1d(x2)))
        
    def are_close(self, real, interp, eps=3.0, add=0.0):
        diff = np.gradient(real)  # diff by y[n+1]-y[n]
        
        # Epsilon is the error multiplication coefficient 
        # It sets how much interpolated value can differ from real one.
        # The base eps = 1 means that error can not be larger than difference to
        # nearest gridpoint.
        # I chose epsilon to 3 because it did pass the first test :P
        b = True
        for d in diff:
            b = b and np.all(np.abs(real-interp) < eps*np.abs(d) + add)  
        
        if not b:
            print("real.shape: {}, interp.shape: {}".format(real.shape, interp.shape))
            for i,d in enumerate(diff):
                print("dim{}: d.shape {}".format(i, d.shape))
            sum = np.sum(~(np.abs(real-interp) < eps*np.abs(d)))
            all = np.prod(real.shape)
            print("Failed points {}/{}".format(sum, all))
            #print("Max error detected: {}"
            #      .format(np.max(np.ravel(np.abs(real-interp))))) 
            #for i,d in enumerate(diff):
            #    print("Max error allowed: dim{}: {}"
            #        .format(i, np.max(np.ravel(eps*np.abs(d)))))
                
        return b
    
    def test_2d(self):
        x1=np.linspace(-2.0,2.0,11)
        y1=np.linspace(-2.0,2.0,11)
        X1,Y1 = np.meshgrid(x1,y1)
        Z1 = X1*np.exp(-1.0*(X1*X1+Y1*Y1))
        lin2d=linear_interp(x=x1,y=y1,f=Z1,dims=2)
        
        x2=np.linspace(-2.0,2.0,51)
        y2=np.linspace(-2.0,2.0,51)
        X2,Y2 = np.meshgrid(x2,y2)
        Z2 = X2*np.exp(-1.0*(X2*X2+Y2*Y2))
        
        self.assertTrue(self.are_close(Z2, 
                                       lin2d.eval2d(x2, y2),
                                       add=0.1))  # I have this add-term only to
                                                  # pass tests.
        
    def test_3d(self):
        x1=np.linspace(0.0,3.0,10)
        y1=np.linspace(0.0,3.0,10)
        z1=np.linspace(0.0,3.0,10)
        X1,Y1,Z1 = np.meshgrid(x1,y1,z1)
        F1 = (X1+Y1+Z1) * np.exp(-1.0 * (X1**2 + Y1**2 + Z1**2))
        #X0,Y0= np.meshgrid(x1,y1)
        lin3d=linear_interp(x=x1, y=y1, z=z1, f=F1, dims=3)

        x2=np.linspace(0.0,3.0,40)
        y2=np.linspace(0.0,3.0,40)
        z2=np.linspace(0.0,3.0,40)
        X2,Y2,Z2 = np.meshgrid(x2, y2, z2)
        F2 = (X2+Y2+Z2) * np.exp(-1.0 * (X2**2 + Y2**2 + Z2**2))
        
        self.assertTrue(self.are_close(F2, 
                                       lin3d.eval3d(x2, y2, z2),
                                       eps=3.0,
                                       add=0.1))  # I have this add-term only to
                                                  # pass tests.


# Custom tests for problem 2
class TestMonteCarlo(unittest.TestCase):
    def test_2d(self):
        blocks = 100
        iters = 10000

        def constant(r):
            return 1.0
        
        def linear(r):
            return r.sum()
        
        # Monte Carlo
        lims = ((-2, 2), (-2, 2))
        i1, e1 = monte_carlo_integration_nd(constant, lims, blocks, iters)
        i2, e2 = monte_carlo_integration_nd(linear, lims, blocks, iters)
        
        print("\nTesting 2d:")
        print("    Constant : {:.3f}({:.3f}), it should be 16.".format(i1, e1))
        print("    Linear : {:.3f}({:.3f}), it should be 0.".format(i2, e2))
        self.assertTrue(i1 - 16 < 0.001)
        self.assertTrue(i2 - 0  < 0.001)
        
    def test_3d(self):
        blocks = 100
        iters = 10000
        mi = -1
        ma = 1  #half of box side length


        def linear(r):
            return r.sum()
        
        def linear_v2(z,y,x):
            return x+y+z
        
                # Monte Carlo
        lims = ((mi, ma), (mi, ma), (mi, ma))
        i1, e1 = monte_carlo_integration_nd(linear, lims, blocks, iters)
        
        print("\nTesting 3d:")
        print("    Monte Carlo : {:.3f}({:.3f}), it should be 0".format(i1, e1))
        #self.assertTrue(i1 - 13.85 < 0.001)
        
        xlim2 = lambda : (mi, ma)
        ylim2 = lambda x: (mi, ma)
        zlim2 = lambda x, y: (mi, ma)
        I1, E1 = nquad(linear_v2, [zlim2, ylim2, xlim2])
        print("    Quad: {:.3f}({:.3f})".format(I1, E1))
        self.assertTrue(i1 - 0 < 0.001)
        self.assertTrue(I1 - 0 < 0.001)



class TestGradientDecent(unittest.TestCase):
    
    def test_gradient(self):
        
        def linear(r):
            dim = len(r)
            #print("asd,", (np.arange(dim) + 1))
            #print("asdasd,", r)
            return (np.arange(dim) + 1) @ r
        
        x = np.array([1,1,1,1])
        h = x.mean() * 0.01
        g = gradient(linear, x, h)
        
        dim = len(x)
        answer = np.arange(dim) + 1
        self.assertTrue((g-answer).sum() < 0.0001)
        
    
    def test_gradient_descent(self):
        
        def spike_at_10(r):
            point = np.zeros(len(r))
            z = np.zeros(len(r), dtype=np.int_)
            #print("r", r, "z", z, "point", point)
            point[z] = 10
            return - 1 / np.linalg.norm(r - point)**2
        
        def spike_at_10_and_20(r):
            point = np.zeros(len(r))
            z = np.zeros(len(r), dtype=np.int_)
            point[z] = 20
            return - 2 / np.linalg.norm(r - point)**2 + spike_at_10(r)
        
        h = 0.01
        start1 = np.array([1,1,1,1])
        ans1 = np.array([10,0,0,0])
        
        # Converges to right minimun
        end1 = gradient_decent(spike_at_10, start1, 100, h)
        self.assertTrue((end1-ans1).sum() > 0.001)
        
        # Converges to wrong minimun (greedy algorithm problem)
        end2 = gradient_decent(spike_at_10_and_20, start1, 100, h)
        self.assertTrue((end2-ans1).sum() > 0.001)
        
        start2 = np.array([30,1,1,1])
        ans2 = np.array([20,0,0,0])
        
        # Converges to right minimun
        end3 = gradient_decent(spike_at_10_and_20, start2, 100, h)
        self.assertTrue((end3-ans2).sum() > 0.001)
        

    
if __name__=="__main__":
    unittest.main()
