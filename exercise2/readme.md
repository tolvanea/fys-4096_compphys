# Exercise 2 Computational Physics

Alpi Tolvanen

## Done problems
    See ```ex2_solved_problems.pdf``` what parts of problems I think I have solved for this exercise.

## Running problems
    It is assumed current directory is ```exercise2```.

    Problem 1 can be run with 
        ```python3 exercise2_help_files_copy/linear_interp.py```
        ```python3 exercise2_help_files_copy/spline_class.py```
        ```python3 tests.py TestLinearInterpModule```

    Problem2 can be run with
        ```python3 problem2.py```             (may take 20 sec)
        ```python3 tests.py TestMonteCarlo``` (optional)
        
    Problem 3 can be run with
        ```python3 tests.py TestGradientDecent```
        
