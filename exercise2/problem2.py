import numpy as np
from exercise2_help_files_copy.num_calculus import monte_carlo_integration
from num_calculus_own import monte_carlo_integration_nd
from scipy.integrate import quad, dblquad, tplquad, nquad

# Alpi Tolvanen 2019-01-15

# 2a 1/3
def f_2a1(r):
    return r**2 * np.exp(-2*r)

# Draw samples from np.exp(-2*r) distribution in monte carlo integration. 
def f_2a1_sample_dst(y):
    inv = (-0.5 * np.log(1 - 2*y))
    return inv**2

# 2a 2/3
def f_2a2(x):
    if x**2 > 1e-8:
        return np.sin(x) / x
    else:
        return 1 - x**2/3

# 2a 3/3
def f_2a3(x):
    return np.exp(np.sin(x**3))

# 2b
def f_2b(r):
    return r[0] * np.exp(-(r**2).sum())

# 2b for quadrature
def f_2b_v2(y, x):
    return f_2b(np.array((x, y)))

# 2c
def f_3c(r, A, B):
    p = psi(r-A)
    norm = np.linalg.norm(r-B)
    if norm > 0.0001:  # Cut off sigualrity, if present. It is still slow.
        return (p**2).sum() / norm
    else:
        return (p**2).sum() / 0.0001

# 2c for quad
def f_3c_v2(x, y, z, A, B):
    return f_3c(np.array((x, y, z)), A, B)
    
def psi(r):
    return np.exp(-np.linalg.norm(r))/np.sqrt(np.pi)

def j(A, B):
    R = np.linalg.norm(A-B)
    return (1 - (1+R) * np.exp(-2*R)) / R
    

def p2():
    blocks = 100
    iters = 10000
    def p2_a():
        # Monte Carlo
        i1, e1 = monte_carlo_integration(f_2a1, 0, 10, blocks, iters)
        i2, e2 = monte_carlo_integration(f_2a2, 0, 1,  blocks, iters)
        i3, e3 = monte_carlo_integration(f_2a3, 0, 5,  blocks, iters)
        
        # Extra: 
        # Draw samples from exp(-2*x) distribution to get better convergence
        i4, e4 = monte_carlo_integration(f_2a1_sample_dst, 0, 0.5, blocks, iters)
        
        # General quadrature
        I1, E1 = quad(f_2a1, 0, 10)
        I2, E2 = quad(f_2a2, 0, 1)
        I3, E3 = quad(f_2a3, 0, 5)
        
        print("\nProb 2a")
        print("    I1: Monte Carlo: {:.3f}({:.3f}),   quad: {:.3f}({:.3f})"
              .format(i1, e1, I1, E1))
        print("    I1: Monte Carlo: {:.3f}({:.3f})    (draw samples from e^-2x)"
              .format(i4, e4))
        print("    I2: Monte Carlo: {:.3f}({:.3f}),   quad: {:.3f}({:.3f})"
              .format(i2, e2, I2, E2))
        print("    I3: Monte Carlo: {:.3f}({:.3f}),   quad: {:.3f}({:.3f})"
              .format(i3, e3, I3, E3))        
        
    
    def p2_b():
        
        # Monte Carlo
        lims = ((-2, 2), (0,2))
        i1, e1 = monte_carlo_integration_nd(f_2b, lims, blocks, iters)
        
        # General quadrature
        xlim = (-2, 2)
        ylim = (lambda x: 0, lambda x: 2)
        I1, E1 = dblquad(f_2b_v2, *xlim, *ylim)
        
        print("\nProb 2b")
        print("    Monte Carlo : {:.3f}({:.3f})".format(i1, e1))
        print("    Quadrature : {:.3f}({:.3f})".format(I1, E1))
        # 1.140
    
    
    def p2_c():
        # B is outside the box (no singularity), which saves time.
        # Integration is wrong anyways.
        A, B = np.array((-0.7,0,0)), np.array((0.7,0,0))
        
        l = 7  #half of box side length. This is too small, but integrated value
        # is anyways way too large to 
        
        print("\nProb 2c")
        
        # Analytical
        i_analytical = j(A, B)
        print("    Analytical: {:.3f}".format(i_analytical))
        
        # Monte Carlo
        lims = ((-l, l), (-l, l), (-l, l))
        i1, e1 = monte_carlo_integration_nd(f_3c, lims, blocks, iters, (A, B))
        print("    Monte Carlo: {:.3f}({:.3f})".format(i1, e1))
        
        # General quadrature
        xlim1 = (-l, l)
        ylim1 = (lambda x: -l, lambda x: l)
        zlim1 = (lambda x, y: -l, lambda x, y: l)
        I1, E1 = tplquad(f_3c_v2, *xlim1, *ylim1, *zlim1, args=(A,B))
        print("    Quadrature v1: {:.3f}({:.3f})".format(I1, E1))
        
        xlim2 = lambda A, B: (-l, l)
        ylim2 = lambda x, A, B: (-l, l)
        zlim2 = lambda x, y, A, B: (-l, l)
        I1_v2, E1_v2 = nquad(f_3c_v2, [zlim2, ylim2, xlim2], args=(A,B))
        print("    Quadrature v2: {:.3f}({:.3f}) (wrong)\n".format(I1_v2, E1_v2))
        print("    Accuracy should be seen from Monte Carlo error estimates and quad error estimates.")
                
    p2_a()
    p2_b()
    p2_c()
    
if __name__ == "__main__":
    p2()
    
