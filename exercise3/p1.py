# Computational Physics Exercises 3 Problem 1
# Alpi Tolvanen 2019

import numpy as np
from scipy.integrate import simps

def f(x, y):
    return (x+y) * np.exp(-(x**2 + y**2))

def p1():
    ylim = (-2, 2)
    xlim = (0, 2)
    N = 1000
    ygrid = np.linspace(ylim[0], ylim[1], N)
    xgrid = np.linspace(xlim[0], xlim[1], N)
    f_tmp = np.full(N, fill_value=float("nan"))
    y_values = np.full(N, fill_value=float("nan"))
    for i, y in enumerate(ygrid):
        f_fixed = lambda x: f(x, y)
        for j in range(N):
            f_tmp[j] = f_fixed(xgrid[j])
            if (np.isnan(f_tmp[i])):
                print("?? {}".format(i))
        assert(not np.isnan(f_tmp).any())
        y_values[i] = simps(f_tmp, x=xgrid)
    
    assert(not np.isnan(not y_values[i]).any())
    sum = np.sum(y_values)
    I = simps(y_values, x=ygrid)
    
    print("P1 I: {:.3f}, sum: {}".format(I, sum))
    
p1()
