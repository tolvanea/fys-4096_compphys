# Exercise 3 Computational Physics

Alpi Tolvanen

## Done problems
I solved all problems, but not one extra-question ( see ```ex3_solved_problems.pdf``` for more).

## Running problems
It is assumed current directory is ```exercise3```.

Problems can be run with:

    * ```python3 p1.py```
    * ```python3 p2.py```
    * ```python3 p3.py```
    * ```python3 p4.py```
