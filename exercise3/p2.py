# Computational Physics Exercises 3 Problem 2
# Alpi Tolvanen 2019

import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt


def f(x, y):
    return (x+y) * np.exp(-(x**2 + y**2))

def gen_exp_data():
    x = np.linspace(-2, 2, 40)
    y = np.linspace(-2, 2, 40)
    X, Y = np.meshgrid(x, y)
    points = np.vstack((np.ravel(X), np.ravel(Y)))
    Z = f(X, Y)
    return np.ravel(Z), points.T

def p2():
    values, points = gen_exp_data()
    line_x = np.linspace(0, np.sqrt(2), 100)
    line_y = line_x*np.sqrt(2)
    line_interp = griddata(points, values, (line_x, line_y), method='cubic')
    line_real = f(line_x, line_y)
    
    fig, ax = plt.subplots(1,1)
    ax.plot(line_y, line_interp, label="interp", marker="*")
    ax.plot(line_y, line_real, label="real")
    ax.legend()
    ax.set_title("function interpolation on line $y=\\sqrt{(2)} x$")
    plt.show()
p2()
