# Computational Physics Exercises 3 Problem 4
# Alpi Tolvanen 2019

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import quad, simps, trapz


eps0 = 8.85419e-12

def d_E_1d(r, lam):
    """ Differential electric field in 1d."""
    assert(isinstance(r, float))  # Python's type system is way too permissive..
    assert(isinstance(lam, float))
    
    r2 = max(r**2, 0.000001)
    E = 1/(4*np.pi*eps0) * (lam / r2) * np.sign(r)
    assert(isinstance(E, float))
    # Let's add minus as a Hervanta constant
    return -E 

def d_E_Nd(r, lam):
    """ Differential electric field in Nd."""
    assert(isinstance(r, np.ndarray))
    assert(len(r.shape) == 1)
    assert(r.shape[0] != 1)
    assert(isinstance(lam, float))
    
    r2 = r@r
    if r2 == 0:
        return np.zeros_like(r)
    else:
        # Let's add minus as a Hervanta constant
        return -1/(4*np.pi*eps0) * (lam / (r@r)) * (r / np.linalg.norm(r))

def E_analytical(lam, L, d):
    """ Analytical electric field in 1d, that is caused by a rod."""
    return (lam/(4*np.pi*eps0)) * (1/d - 1/(d+L))

def p4():
    # The integrals are evaluated only on rod-line (not over the full space)
    
    L = 1.0
    Q = 1.0
    lam = Q/L
    # form 1d rod charge density
    x_grid = np.linspace(-2.5*L, 2.5*L, 101, endpoint=True)
    y_grid = np.linspace(-2.5*L, 2.5*L, 101, endpoint=True)
    Y, X = np.meshgrid(x_grid, y_grid)
    charge_dens = np.zeros_like(X)
    charge_dens[40:61, 50] = lam
    
    fig = plt.figure(figsize=(9,4), num="Cool bar")

    # 1d-plot
    def part1():
        charge_dens_on_x_axis = charge_dens[:, 50]
        field_on_x_axis_intg = np.zeros_like(x_grid)
        field_on_x_axis_analyt = np.full_like(x_grid, fill_value=float("nan"))
        
        for i, x in enumerate(x_grid):
            dE = np.empty(61-40)
            # Calculate differential for different values of x. 
            # Integral is every where else zero
            for j in range(len(dE)):
                dE[j] = d_E_1d(x_grid[40+j]-x, charge_dens_on_x_axis[40+j])

            field_on_x_axis_intg[i] = simps(dE, x=(x_grid[40:61]-x))
            if x > L/2:
                field_on_x_axis_analyt[i] = E_analytical(lam, L, x-L/2)
        
        ax1 = plt.subplot2grid((1, 2), (0, 0), colspan=1, rowspan=1)
            
        ax1.plot(x_grid, field_on_x_axis_intg, label="field")
        ax1.plot(x_grid, field_on_x_axis_analyt, label="analyt", marker="*")
        ax1.plot([-L/2, L/2], [0, 0], label="rod", c="r", lw=2)
        ax1.legend()
    
    
    # 2d-plot
    def part2():
        # Coordinates of a rod
        line = np.vstack((x_grid[40:61], np.zeros(61-40))).T 
        charge_dens_on_line = charge_dens[40:61, 50]
        field = np.zeros((*(charge_dens.shape), 2))
        for i, x in enumerate(x_grid):
            for j, y in enumerate(y_grid):
                r = (x,y)
                dE = np.zeros_like(line)
                for k in range(len(dE)):
                    dE[k,:] = d_E_Nd(line[k,:]-r, charge_dens_on_line[k])

                # Integrate over x
                s = simps(dE, x=(line-r), axis=0)
                E_x = simps(dE[:,0], x=(line-r)[:,0])  # Integrate Ex over x
                E_y = simps(dE[:,1], x=(line-r)[:,0])  # Integrate Ey over x
                field[i,j,:] = (E_x, E_y)
                assert(not np.isnan(field[i,j,:]).any())
                
        assert(not np.isnan(field).any())
        ax2 = plt.subplot2grid((1, 2), (0, 1), colspan=1, rowspan=1)
        C = np.hypot(field[::4,::4,0], field[::4,::4,1]) # colors of the arrows
        ax2.quiver(X[::4,::4], Y[::4,::4], field[::4,::4,0], field[::4,::4,1], 
                   C)
        ax2.plot([-L/2, L/2], [0, 0], label="rod", color="r", lw=2)

    part1()
    part2()
    
    plt.show()
    
p4()
