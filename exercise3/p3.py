# Computational Physics Exercises 3 Problem 3
# Alpi Tolvanen 2019

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import matplotlib.pyplot as plt

 
def largest_eig(A, N_max):
    """Calulate largest eigenvalue and eigenvector of matrix with power method.
    """
    assert(isinstance(A, np.ndarray))
    assert(A.shape[0] == A.shape[1])
    assert(not np.isnan(A).all())
    N_mean_over = 10
    v1 = np.zeros((A.shape[0], N_mean_over))
    lam1 = np.zeros(N_mean_over)
    for sample in range(N_mean_over):
        x = np.random.rand(A.shape[0])
        A_pow = A @ A
        A_pow = A_pow / np.linalg.norm(A_pow)
        A_pow_old = A_pow.copy()
        b = (A_pow @ x) / np.linalg.norm(A_pow @ x)
        b_old = b.copy()
            
        tol = 0.00001
        for i in range(N_max):
            
            A_pow = A_pow @ A_pow
            A_pow = A_pow / np.linalg.norm(A_pow)
            
            b = A_pow @ x
            b = b / np.linalg.norm(b)
            
            if np.linalg.norm(b - b_old) < np.linalg.norm(b_old) * tol:
                break
            
            b_old = b.copy()
            A_pow_old = A_pow.copy()
            
        else:
            raise Exception("Convergence not achieved")
        #print("Iterations: {}".format(i))
        
        v1[:,sample] = b
        #lam1[sample] = np.sum(A @ b) / np.sum(b) # Bad accuracy
        lam1[sample] = np.mean(A @ b / b)
    
    ids = np.argsort(lam1)
    lam1 = lam1[ids]
    v1 = v1[:,ids]
    
    return v1[:,N_mean_over//2], lam1[N_mean_over//2]
            

def gen_matrix():
    grid = np.linspace(-5,5,100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    # make test matrix
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0.toarray()


        
def p3():
    H0 = gen_matrix()
    print("H0.shape", H0.shape)
    v, l = largest_eig(H0, 100000)
    
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    
    n = np.linalg.norm
    
    #print("n(evecs), n(v)", n(evecs), n(v))
    print("Problem 3")
    print("    largest_eig:  lambda: {}".format(l))
    print("    Scipy:        lambda: {}".format(eigs))
    print("        Relative norm of diffence of eigenvectors {} "
          .format(n(evecs-v)/n(v)))
    print("        --> It seems that eigenvectors differ from each other.")
    
    plt.plot(evecs**2,label='scipy eig. vector squared')
    plt.plot(v**2,'r--',label='largest_eig vector squared')
    plt.show()

    
p3()
