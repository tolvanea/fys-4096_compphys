"""
This file contains answers to problem 1 in exercises 10. Answers below.

a) Yes I got energy near 3 Ha. This solved quantum dot system with two
   electrons.
       M = 100,
       tau=0.25,
       k=3.116681e-6
       M*tau = 1/(kT)
       --> Temperature:
            T = (1/(M*tau*k))
              = 12834 K

b) Comments added to code.

c) Constants are already m = ω = 1.
   For hydrogen converence and tau-etrapolation, see figure prob1_c.png.
   As seen from figure, smallest tau is not really converged block-wise, so
   therefore energy estimate is higher than it should be.

d)
Energies below are calculated with tau=0.0625. Gaussian distribution means bisection move.
   Distrib.   Action    | Q-dot                | H2                   | Should be correct
--------------------------------------------------------------------------------
1. Gaussian ; ΔK & ΔU   |    4.689 (±   0.303) |    4.783 (±   0.479) | True
2. Uniform  ; ΔK & ΔU   | -124.648 (±   5.488) | -187.478 (±   8.287) | True
3. Gaussian ; ΔU        |    4.586 (±   0.304) |    5.505 (±   0.456) | True
4. Uniform  ; ΔU        | -111.287 (±   5.052) | -201.369 (±   8.931) | False
- Row 1. is the original calulation setup.
- Now rows 1. and 2. should have same value, as sampling distribution should not matter.
- Also rows 1. and 3. should have same value, as bisection move samples
  kinetic distribution exactly.
- Row 4. should have wrong value, because kinetic term can not be left out with uniform distribution.

--> We see that kinetic term can be dropped out, but uniform distribution does
    not seem to work well somewhy.
"""

from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import scipy.linalg
import copy


class Walker:
    """ Walker in closed loop. In finnish term "helminauhan helmi" may be also used.
    """

    def __init__(self, *args, **kwargs):
        self.Ne = kwargs['Ne']
        self.Re = kwargs['Re']
        self.spins = kwargs['spins']
        self.Nn = kwargs['Nn']
        self.Rn = kwargs['Rn']
        self.Zn = kwargs['Zn']
        self.tau = kwargs['tau']
        self.sys_dim = kwargs['dim']
        self.type = kwargs['type']
        self.name = kwargs['name']

    def w_copy(self):
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      tau=self.tau,
                      dim=self.sys_dim,
                      type=self.type,
                      name=self.name, )


def kinetic_action(r1, r2, tau, lambda1):
    return sum((r1 - r2) ** 2) / lambda1 / tau / 4


def potential_action(Walkers, time_slice1, time_slice2, tau):
    return 0.5 * tau * (potential(Walkers[time_slice1]) \
                        + potential(Walkers[time_slice2]))


def pimc(Nblocks, Niters, Walkers, movetype=(False, False)):
    """
    Simple path integral monte carlo calulation.

    :param Nblocks: Number of blocks (measurements are avergagen over block)
    :param Niters:  Number of iterations per block
    :param Walkers: Walkers as a list
    :return:
    """
    M = len(Walkers)  # Trotter number
    Ne = Walkers[0].Ne * 1  # Number electrons
    Eb = zeros((Nblocks,))
    Accept = zeros((Nblocks,))
    AccCount = zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            time_slice1 = int(random.rand() * M)
            ptcl_index = int(random.rand() * Ne)
            r1_old = 1.0 * Walkers[time_slice1].Re[ptcl_index]

            A_RtoRp = bisection_move(Walkers, time_slice1, ptcl_index, movetype)

            # If action degreases action, allow move, else throw dice
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[time_slice1].Re[ptcl_index] = r1_old
            AccCount[i] += 1
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                # print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            # exit()

        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i + 1, Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))

    return Walkers, Eb, Accept


def bisection_move(Walkers, time_slice1, ptcl_index, movetype):
    M = len(Walkers)  # Trotter number
    sys_dim = 1 * Walkers[0].sys_dim  # dimension
    tau = 1.0 * Walkers[0].tau  # time step
    lambda1 = 0.5  # h^2 / (2m)
    sigma2 = lambda1 * tau  # diffusion variance
    sigma = sqrt(sigma2)
    discard_kinetic_action, uniform_distribution = movetype

    # Three consecutive time slices
    time_slice0 = (time_slice1 + M - 1) % M
    time_slice2 = (time_slice1 + 1) % M

    r0 = Walkers[time_slice0].Re[ptcl_index]
    r1 = 1.0 * Walkers[time_slice1].Re[ptcl_index]
    r2 = Walkers[time_slice2].Re[ptcl_index]

    KineticActionOld = kinetic_action(r0, r1, tau, lambda1) + \
                       kinetic_action(r1, r2, tau, lambda1)
    PotentialActionOld = potential_action(Walkers, time_slice0, time_slice1,
                                          tau) + \
                         potential_action(Walkers, time_slice1, time_slice2,
                                          tau)

    # bisection sampling
    r02_ave = (r0 + r2) / 2
    # Sampling probability of old point
    log_S_Rp_R = -sum((r1 - r02_ave) ** 2) / (2 * sigma2)
    if uniform_distribution:
        Rp = r1 + (random.rand(sys_dim)-0.5) * 2 * sigma
    else:
        Rp = r02_ave + random.randn(sys_dim) * sigma
    # Sample distance from midpoint
    log_S_R_Rp = -sum((Rp - r02_ave) ** 2) / (2 * sigma2)

    Walkers[time_slice1].Re[ptcl_index] = 1.0 * Rp
    KineticActionNew = kinetic_action(r0, Rp, tau, lambda1) + \
                       kinetic_action(Rp, r2, tau, lambda1)
    PotentialActionNew = potential_action(Walkers, time_slice0, time_slice1,
                                          tau) + \
                         potential_action(Walkers, time_slice1, time_slice2,
                                          tau)

    # Action difference
    deltaK = KineticActionNew - KineticActionOld
    deltaU = PotentialActionNew - PotentialActionOld

    if discard_kinetic_action:
        q_R_Rp = exp(-deltaU)
    else:
        q_R_Rp = exp(log_S_Rp_R - log_S_R_Rp - deltaK - deltaU)
    # q sρ/(s'ρ')
    A_RtoRp = min(1.0, q_R_Rp)

    return A_RtoRp


def Energy(Walkers):
    """
    Calculate thermal total energy by equation 22 in slides 10.

    :return:
    """
    M = len(Walkers)
    d = 1.0 * Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            if (i < M - 1):
                K += d / 2 / tau - sum((Walkers[i].Re[j] - Walkers[i + 1].Re[
                    j]) ** 2) / 4 / lambda1 / tau ** 2
            else:
                K += d / 2 / tau - sum((Walkers[i].Re[j] - Walkers[0].Re[
                    j]) ** 2) / 4 / lambda1 / tau ** 2
    return K / M, U / M


def potential(Walker):
    """
    Calculate potential energy of electrons (diagonal operator)
    :return:
    """
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(Walker.Ne - 1):
        for j in range(i + 1, Walker.Ne):
            r = sqrt(sum((Walker.Re[i] - Walker.Re[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    if Walker.type == "quantum_dot":
        Vext = external_potential_qdot(Walker)
    elif Walker.type == "nucleus":
        Vext = potential_nucleus(Walker)

    return V + Vext


def external_potential_qdot(Walker):
    """
    Caclulate quantum dot external potential.
    :return:
    """
    V = 0.0
    for i in range(Walker.Ne):
        V += 0.5 * sum(Walker.Re[i] ** 2)
    return V


def potential_nucleus(Walker):
    """
    Caclulate quantum dot external potential.
    :return:
    """
    V = 0.0
    r_cut = 1.0e-12

    # Ion - E
    for i in range(Walker.Nn):
        for j in range(Walker.Ne):
            r = sum((Walker.Re[j] - Walker.Nn) ** 2)
            V -= Walker.Zn[i] * erf(r / sqrt(2 * 0.1)) / r

    for i in range(Walker.Nn - 1):
        for j in range(i + 1, Walker.Nn):
            r = np.sqrt(sum((Walker.Rn[i] - Walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)
    return V


def calculate_timestep_extrapolation(Nblocks, Niters, taus, Ms, systems):
    Ebs = [[None for j in range(len(systems))] for i in range(len(taus))]

    # Calculate
    for i, (tau, M) in enumerate(zip(taus, Ms)):
        for j, walk in enumerate(systems):
            walkers = []
            walk.tau = tau
            for i_ in range(M):
                walkers.append(copy.deepcopy(walk))
            walkers, Ebs[i][j], Acc = pimc(Nblocks, Niters, walkers)
    return Ebs


def plot_block_convergence(ax1, Ebs, taus, burnout, pick_idx, systems):
    """
    Plot energy by block count for some single tau value.
    """
    for system, label in enumerate(
            (systems[0].name, systems[1].name)):  # "Q-dot", "H2"
        Eb = Ebs[pick_idx][system][burnout:]
        ax1.plot(np.arange(burnout), Ebs[pick_idx][system][:burnout], ls=":",
                 c="C{}".format(system))
        ax1.plot(np.arange(burnout, burnout + len(Eb)), Eb, label=label,
                 c="C{}".format(system))
        ax1.set_xlabel("blocks")
        ax1.set_ylabel("energy")
        txt = label + " at $\\tau = {}$ : $E_{{tot}}$ : {:.3f} ± {:0.3f}".format(
            taus[pick_idx], mean(Eb), std(Eb) / sqrt(len(Eb)))
        print(txt)
        ax1.text(0.05, 0.25 - 0.1 * system, txt, transform=ax1.transAxes)
        print(
            'Variance to energy ratio: {0:.5f}'.format(abs(std(Eb) / mean(Eb))))
    ax1.set_title(
        "Energy convergence by block, $\\tau = {}$".format(taus[pick_idx]))
    ax1.legend()


def plot_timestep_extrapolation(ax2, Ebs, taus, systems):
    """
    Plot energy by time step (tau).
    """
    for system, label in enumerate(
            (systems[0].name, systems[1].name)):  # "Q-dot", "H2"
        # Energies (and errorbars) by tau
        energies_by_tau = np.array(
            [mean(Ebs[i][system]) for i in range(len(taus))])
        errors_by_tau = np.array(
            [std(Ebs[i][system]) / sqrt(len(Ebs[i][system])) for i in
             range(len(taus))])

        ax2.errorbar(taus, energies_by_tau, errors_by_tau, label=label)

        # Polynomial fit
        mat = np.zeros((len(taus), 3))
        mat[:, 0] = taus ** 2
        mat[:, 1] = taus
        mat[:, 2] = 1.0
        coeff = linalg.lstsq(mat, energies_by_tau)[0]

        fit = lambda x, c: c[0] * x ** 2 + c[1] * x + c[2]
        x_range = np.linspace(0, taus[-1])
        ax2.plot(x_range, fit(x_range, coeff), ls=":", c="k")

        extrapolated_energy = fit(0, coeff)
        ax2.text(0.05, 0.25 - system * 0.1,
                 label + " at $\\tau = 0$ : $E_tot = {:.3f}\\;$Ha"
                 .format(extrapolated_energy), transform=ax2.transAxes)

    ax2.set_title("Energy by timestep $\\tau$")
    ax2.legend()
    ax2.set_xlim(0, taus[-1])
    ax2.set_ylabel("energy")
    ax2.set_xlabel("$\\tau$")


def calculate_with_bisection_testing(systems, M, tau, Nblocks, Niters, burnout):
    """
    Calculate with uniform and wtih bisection moves, enable and disable kinetic
    term.
    """

    # Enable/disable kinetic action, or use uniform/bisection moves.
    params = ((False, False),
              (False, True),
              (True, False),
              (True, True),)
    results = np.zeros((2, 4))
    errors = np.zeros((2, 4))

    for i, walk in enumerate(systems):
        for j, (kin, uni) in enumerate(params):
            discard_kinetic_action = kin
            uniform_distribution = uni
            movetype = (discard_kinetic_action, uniform_distribution)
            walkers = []
            walk.tau = tau
            for i_ in range(M):
                walkers.append(copy.deepcopy(walk))
            walkers, Eb, Acc = pimc(Nblocks, Niters, walkers, movetype)
            results[i, j] = Eb[burnout:].mean()
            errors[i, j] = std(Eb) / sqrt(len(Eb))

    labels = ("Gaussian ; ΔK & ΔU",
              "Uniform  ; ΔK & ΔU",
              "Gaussian ; ΔU     ",
              "Uniform  ; ΔU     ",)
    should_be_correct = (True, True, True, False)
    print("Problem 1.d")
    print("Energies below are calculated with tau={}. Gaussian distribution \n"
          "means bisection move.".format(tau))
    print("   {:20} | {:18} | {:18} | Should be correct".format(
        "Distrib.  Action", systems[0].name, systems[1].name))
    print("-" * 80)
    for i in range(len(params)):
        print("{:1}. {:20} | {: 8.3f} (±{: 8.3f}) | {: 8.3f} (±{: 8.3f}) | {}"
              .format(i + 1, labels[i], results[0, i], errors[0, i],
                      results[1, i], errors[1, i], should_be_correct[i], ))
    print("- Row 1. is the original calulation setup.")
    print("- Now rows 1. and 2. should have same value, as sampling distribution\n"
          "  should not matter.")
    print(
        "- Also rows 1. and 3. should have same value, as bisection move samples \n"
        "  kinetic distribution exactly.")
    print("- Row 4. should have wrong value, because kinetic term can not be left \n"
          "  out with uniform distribution.")


def main():
    # For H2
    H2 = Walker(Ne=2,
                Re=[array([0.5, 0, 0]), array([-0.5, 0, 0])],
                spins=[0, 1],
                Nn=2,
                Rn=[array([-0.7, 0, 0]), array([0.7, 0, 0])],
                Zn=[1.0, 1.0],
                tau=0.1,
                dim=3,
                type="nucleus",
                name="H2", )

    # For 2D quantum dot
    QD = Walker(Ne=2,
                Re=[array([0.5, 0]), array([-0.5, 0])],
                spins=[0, 1],
                Nn=2,  # not used
                Rn=[array([-0.7, 0]), array([0.7, 0])],  # not used
                Zn=[1.0, 1.0],  # not used
                tau=0.25,
                dim=2,
                type="quantum_dot",
                name="Q-dot")

    Nblocks = 200
    Niters = 100
    burnout = 30

    # Part 1.c

    taus = np.array([0.0625, 0.125, 0.25, 0.5, ])
    Ms = np.array([400, 200, 100, 50, ])
    pick_idx = 0  # choose one simulation for block-convergence plotting
    systems = (QD, H2)
    Ebs = calculate_timestep_extrapolation(Nblocks, Niters, taus, Ms, systems)

    fig, (ax1, ax2) = subplots(1, 2, figsize=(10, 5))
    # Plot energy convergences by block (axis 1)
    plot_block_convergence(ax1, Ebs, taus, burnout, pick_idx, systems)
    # Tau-convergence near 0, (axis 2)
    plot_timestep_extrapolation(ax2, Ebs, taus, systems)

    # Part 1.d, print data
    calculate_with_bisection_testing(systems, Ms[pick_idx], taus[pick_idx],
                                     Nblocks, Niters, burnout)

    show()


if __name__ == "__main__":
    main()
